package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableZipcode;
import myskysfa.com.sfa.database.TableZipcodeValsist;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/17/2016.
 */
public class TableZipcodeValsistAdapter {

    static private TableZipcodeValsistAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableZipcodeValsistAdapter(ctx);
        }
    }
    static public TableZipcodeValsistAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableZipcodeValsistAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableZipcodeValsist> getAllData() {
        List<TableZipcodeValsist> tblsatu = null;
        try {
            tblsatu = getHelper().getTableZipcodeValsistDAO().queryBuilder().orderBy("POSTAL_CODE", false).query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> getDatabyCondition(String condition, Object param) {
        List<TableZipcodeValsist> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            Where<TableZipcodeValsist, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("POSTAL_CODE", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> fetchFindByandSearch(String condition, Object param) {
        List<TableZipcodeValsist> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            Where<TableZipcodeValsist, Integer> where = queryBuilder.where();
            where.like(condition, param);
            queryBuilder.orderBy("POSTAL_CODE", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> listProvinsi() {
        List<TableZipcodeValsist> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcodeValsist.fPROVINCE).selectColumns(TableZipcodeValsist.fPROVINCE);
            queryBuilder.groupBy(TableZipcodeValsist.fPROVINCE);
            queryBuilder.groupBy(TableZipcodeValsist.fPROVINCE);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> listCity(String param) {
        List<TableZipcodeValsist> tblsatu = null;
        int count = 0;
        String result = "";
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcodeValsist.fBIG_CITY).selectColumns(TableZipcodeValsist.fBIG_CITY);
            Where<TableZipcodeValsist, Integer> where = queryBuilder.where();
            where.eq(TableZipcodeValsist.fPROVINCE, param);
            queryBuilder.groupBy(TableZipcodeValsist.fBIG_CITY);
            queryBuilder.groupBy(TableZipcodeValsist.fBIG_CITY);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> listKecamatan(String param) {
        List<TableZipcodeValsist> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcodeValsist.fSMALL_CITY).selectColumns(TableZipcodeValsist.fSMALL_CITY);
            Where<TableZipcodeValsist, Integer> where = queryBuilder.where();
            where.eq(TableZipcodeValsist.fBIG_CITY, param);
            queryBuilder.groupBy(TableZipcodeValsist.fSMALL_CITY);
            queryBuilder.groupBy(TableZipcodeValsist.fSMALL_CITY);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> listKelurahan(String param) {
        List<TableZipcodeValsist> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcodeValsist.fPOSTAL_CODE).selectColumns(TableZipcodeValsist.fPOSTAL_CODE);
            queryBuilder.selectColumns(TableZipcodeValsist.fVILLAGE).selectColumns(TableZipcodeValsist.fVILLAGE);
            Where<TableZipcodeValsist, Integer> where = queryBuilder.where();
            where.eq(TableZipcodeValsist.fSMALL_CITY, param);
            queryBuilder.groupBy(TableZipcodeValsist.fSMALL_CITY);
            queryBuilder.groupBy(TableZipcodeValsist.fSMALL_CITY);
            queryBuilder.groupBy(TableZipcodeValsist.fVILLAGE);
            queryBuilder.groupBy(TableZipcodeValsist.fVILLAGE);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcodeValsist> listZipcode(String param) {
        List<TableZipcodeValsist> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcodeValsist.class);
            QueryBuilder<TableZipcodeValsist, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcodeValsist.fPOSTAL_CODE);
            Where<TableZipcodeValsist, Integer> where = queryBuilder.where();
            where.eq(TableZipcodeValsist.fVILLAGE, param);
            queryBuilder.groupBy(TableZipcodeValsist.fPOSTAL_CODE);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }
}
