package myskysfa.com.sfa.adapter.master;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableMasterPackage;

/**
 * Created by Eno on 6/20/2016.
 */
public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.ViewHolder> {
    View itemLayoutView;
    ViewGroup mParent;
    private List<TableMasterPackage> itemsData;
    private Context context;

    public PackageAdapter(Context context, List<TableMasterPackage> itemsData) {
        this.context = context;
        this.itemsData = itemsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.package_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterPackage itemPackage = itemsData.get(position);
        if (itemPackage != null) {
            holder.txtTitle.setText(itemPackage.getProduct_name());
            holder.txtPrice.setText(itemPackage.getPrice());
            holder.txtIcon.setText(itemPackage.getProduct_name().substring(0, 1));
            if (itemPackage.getAlacarte().equals("1")){
                holder.type.setText("alacarte");
                holder.type.setTextColor(Color.BLUE);
            }else{
                holder.type.setText("basic");
                holder.type.setTextColor(Color.GREEN);
            }
        }

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public TextView txtPrice;
        public TextView txtIcon;
        private TextView type;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemLayoutView.findViewById(R.id.master_pack_item_title);
            txtPrice = (TextView) itemLayoutView.findViewById(R.id.master_pack_item_price);
            txtIcon = (TextView) itemLayoutView.findViewById(R.id.master_pack_item_icon);
            type = (TextView) itemLayoutView.findViewById(R.id.master_pack_item_type);

        }
    }
}