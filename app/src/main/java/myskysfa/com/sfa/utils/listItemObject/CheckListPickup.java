package myskysfa.com.sfa.utils.listItemObject;

import android.content.Context;

/**
 * Created by Asus on 11/7/2016.
 */

public class CheckListPickup {

    private  Context mContext;
    public  String ID_CHECKLIST ;
    public  String CHECKLIST_LABEL_EDIT  ;
    public  String CHECKLIST_LABEL ;
    public  String CHECKLIST_STATUS ;
    public String CHECKLIST_FLAG;



    // Empty constructor
    public CheckListPickup(){

    }
    // constructor
    public CheckListPickup(Context mContext, String ID_CHECKLIST , String CHECKLIST_LABEL,
                           String CHECKLIST_LABEL_EDIT , String CHECKLIST_STATUS, String CHECKLIST_FLAG){
        super();
        this.ID_CHECKLIST = ID_CHECKLIST;
        this.CHECKLIST_LABEL = CHECKLIST_LABEL;
        this.CHECKLIST_LABEL_EDIT = CHECKLIST_LABEL_EDIT;
        this.CHECKLIST_STATUS =CHECKLIST_STATUS;
        this.CHECKLIST_FLAG = CHECKLIST_FLAG;

    }

    public CheckListPickup(Context mContext) {
        this.mContext = mContext;
    }


    public void setID_CHECKLIST(String ID_CHECKLIST) {
        this.ID_CHECKLIST = ID_CHECKLIST;
    }

    public String getID_CHECKLIST() {
        return ID_CHECKLIST;
    }

    public void setCHECKLIST_LABEL(String CHECKLIST_LABEL) {
        this.CHECKLIST_LABEL = CHECKLIST_LABEL;
    }

    public String getCHECKLIST_LABEL() {
        return CHECKLIST_LABEL;
    }

    public void setCHECKLIST_STATUS(String CHECKLIST_STATUS) {
        this.CHECKLIST_STATUS = CHECKLIST_STATUS;
    }

    public String getCHECKLIST_STATUS() {
        return CHECKLIST_STATUS;
    }

    public void setCHECKLIST_LABEL_EDIT(String CHECKLIST_LABEL_EDIT) {
        this.CHECKLIST_LABEL_EDIT = CHECKLIST_LABEL_EDIT;
    }

    public String getCHECKLIST_LABEL_EDIT() {
        return CHECKLIST_LABEL_EDIT;
    }

    public String getCHECKLIST_FLAG() {
        return CHECKLIST_FLAG;
    }

    public void setCHECKLIST_FLAG(String CHECKLIST_FLAG) {
        this.CHECKLIST_FLAG = CHECKLIST_FLAG;
    }
}