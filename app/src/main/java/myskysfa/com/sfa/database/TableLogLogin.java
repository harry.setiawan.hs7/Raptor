package myskysfa.com.sfa.database;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "log_login")
public class TableLogLogin {

    public static final String TABLE_NAME = "log_login";
    public static final String C_ROW_ID = "id";
    public static final String C_UID = "uid";
    public static final String C_USER_NAME = "user_name";
    public static final String C_EMPLOYEE_ID = "employee_id";
    public static final String C_FULL_NAME = "full_name";
    public static final String C_ROLE_CODE = "role_code";
    public static final String C_ROLE_NAME = "role_name";
    public static final String C_IS_SUPERVISOR = "is_supervisor";
    public static final String C_TOKEN = "token";
    public static final String C_SFL_CODE = "sfl_code";
    public static final String C_SFL_NAME = "sfl_name";
    public static final String C_LOG_DATE = "log_date";
    public static final String C_IS_LOGIN = "is_login";
    public static final String C_PASSWORD = "password";
    public static final String C_BRAND = "brand";
    public static final String C_BRANCH = "branch";
    public static final String C_BRANCH_ID = "branch_id";
    public static final String C_USERTYPE = "user_type";
    public static final String C_REGIONCODE = "region_code";
    public static final String C_REGIONNAME = "region_name";
    public static final String C_NIK = "nik";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String uid;
    @DatabaseField
    private String user_name;
    @DatabaseField
    private String employee_id;
    @DatabaseField
    private String full_name;
    @DatabaseField
    private String role_code;
    @DatabaseField
    private String role_name;
    @DatabaseField
    private String is_supervisor;
    @DatabaseField
    private String token;
    @DatabaseField
    private String sfl_code;
    @DatabaseField
    private String sfl_name;
    @DatabaseField
    private String log_date;
    @DatabaseField
    private String is_login;
    @DatabaseField
    private String password;
    @DatabaseField
    private String brand;
    @DatabaseField
    private String branch;
    @DatabaseField
    private String branch_id;
    @DatabaseField
    private String user_type;
    @DatabaseField
    private String region_code;
    @DatabaseField
    private String region_name;
    @DatabaseField
    private String nik;

    public TableLogLogin() {
    }

    public void setcRowId(int row_id) {
        this.id = row_id;
    }

    public int getcRowId() {
        return id;
    }

    public void setcUid(String uid) {
        this.uid = uid;
    }

    public String getcUid() {
        return uid;
    }

    public void setcUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getcUserName() {
        return user_name;
    }

    public void setcEmployeeId(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getcEmployeeId() {
        return employee_id;
    }

    public void setcFullName(String full_name) {
        this.full_name = full_name;
    }

    public String getcFullName() {
        return full_name;
    }

    public void setcRoleCode(String role_code) {
        this.role_code = role_code;
    }

    public String getcRoleCode() {
        return role_code;
    }

    public void setcRoleName(String role_name) {
        this.role_name = role_name;
    }

    public String getcRoleName() {
        return role_name;
    }

    public void setcIsSupervisor(String is_supervisor) {
        this.is_supervisor = is_supervisor;
    }

    public String getcIsSupervisor() {
        return is_supervisor;
    }

    public void setcToken(String token) {
        this.token = token;
    }

    public String getcToken() {
        return token;
    }

    public void setcSflCode(String sfl_code) {
        this.sfl_code = sfl_code;
    }

    public String getcSflCode() {
        return sfl_code;
    }

    public void setcSflName(String sfl_name) {
        this.sfl_name = sfl_name;
    }

    public String getcSflName() {
        return sfl_name;
    }

    public void setcLogDate(String log_date) {
        this.log_date = log_date;
    }

    public String getcLogDate() {
        return log_date;
    }

    public void setcIsLogin(String is_login) {
        this.is_login = is_login;
    }

    public String getcIsLogin() {
        return is_login;
    }

    public void setcPassword(String password) {
        this.password = password;
    }

    public String getcPassword() {
        return password;
    }

    public void setcBrand(String brand) {
        this.brand = brand;
    }

    public String getcBrand() {
        return brand;
    }

    public void setcBranch(String branch) {
        this.branch = branch;
    }

    public String getcBranch() {
        return branch;
    }

    public void setcBranchId(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getcBranchId() {
        return branch_id;
    }

    public void setcUsertype(String user_type) {
        this.user_type = user_type;
    }

    public String getcUsertype() {
        return user_type;
    }

    public void setcRegioncode(String region_code) {
        this.region_code = region_code;
    }

    public String getcRegioncode() {
        return region_code;
    }

    public void setcRegionname(String region_name) {
        this.region_name = region_name;
    }

    public String getcRegionname() {
        return region_name;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }
}