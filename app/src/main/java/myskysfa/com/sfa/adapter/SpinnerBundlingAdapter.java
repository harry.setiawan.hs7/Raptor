package myskysfa.com.sfa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.database.TableFreeTrialCoh;

/**
 * Created by Hari h on 8/1/2016.
 */
public class SpinnerBundlingAdapter extends ArrayAdapter<TableFreeTrialCoh> {

    private List<TableFreeTrialCoh> itemsStatus;
    private Context context;

    public SpinnerBundlingAdapter(Context context, int textViewResourceId,
                                  List<TableFreeTrialCoh> itemsStatus) {
        super(context, textViewResourceId, itemsStatus);
        this.context = context;
        this.itemsStatus = itemsStatus;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(getContext());
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10, 10, 10, 10);
        label.setTextSize(16);
        label.setText(itemsStatus.get(position).getSn());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.DKGRAY);
        label.setPadding(10,10,10,10);
        label.setTextSize(16);
        label.setText(itemsStatus.get(position).getSn());

        return label;
    }

}
