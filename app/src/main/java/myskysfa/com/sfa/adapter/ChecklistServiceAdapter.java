package myskysfa.com.sfa.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.vision.text.Line;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TablePromotionServices;


/**
 * Created by Asus on 11/3/2016.
 */

public class ChecklistServiceAdapter extends RecyclerView.Adapter<ChecklistServiceAdapter.ViewHolder> {
    private List<TablePromotionServices> list;
    private View itemLayoutView;
    //private Context mContext;




    public interface OnItemCheckListener {
        void onItemCheck(TablePromotionServices item);
        void onItemUncheck(TablePromotionServices item);
    }

    @NonNull
    private OnItemCheckListener onItemCheckListener;

    public ChecklistServiceAdapter(List<TablePromotionServices> list, @NonNull OnItemCheckListener onItemCheckListener) {
        this.list = list;
        this.onItemCheckListener = onItemCheckListener;
        //this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dtd_checklist_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TablePromotionServices itemChecklist;
        itemChecklist = list.get(position);
        String labelS = itemChecklist.getService();
        holder.checklistCB.setText(labelS);
        holder.liear.setVisibility(View.GONE);

        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //holder.checklistCB.setChecked(!holder.checklistCB.isChecked());

                if (holder.checklistCB.isChecked()) {
                    onItemCheckListener.onItemCheck(itemChecklist);
                } else {
                    onItemCheckListener.onItemUncheck(itemChecklist);
                }
            }
        });

        holder.checklistCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    onItemCheckListener.onItemCheck(itemChecklist);
                }else {
                    onItemCheckListener.onItemUncheck(itemChecklist);
                    //itemChecklist.setCHECKLIST_STATUS("0");
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextWatcher {
        private CheckBox checklistCB;
        private LinearLayout liear;

        public ViewHolder(View itemView) {
            super(itemView);
            this.checklistCB = (CheckBox) itemLayoutView.findViewById(R.id.checklistCB);
            this.liear = (LinearLayout) itemLayoutView.findViewById(R.id.linearEdit);
        }

        public void setOnClickListener(View.OnClickListener onClickListener) {
            itemView.setOnClickListener(onClickListener);
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


}

