package myskysfa.com.sfa.adapter.master;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableMasterMS;

/**
 * Created by Eno on 6/20/2016.
 */
public class ModernStoreAdapter extends RecyclerView.Adapter<ModernStoreAdapter.ViewHolder> {
    View itemLayoutView;
    ViewGroup mParent;
    private List<TableMasterMS> itemsData;
    private Context context;

    public ModernStoreAdapter(Context context, List<TableMasterMS> itemsData) {
        this.context = context;
        this.itemsData = itemsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ms_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterMS itemMaterial = itemsData.get(position);
        if (itemMaterial != null) {
            holder.txtTitle.setText(itemMaterial.getName());
            holder.txtId.setText(itemMaterial.getAddress() + " - " + itemMaterial.getPost_code());
            holder.txtIcon.setText(itemMaterial.getName().substring(0, 1));
        }

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public TextView txtId;
        public TextView txtIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemLayoutView.findViewById(R.id.master_ms_item_title);
            txtId = (TextView) itemLayoutView.findViewById(R.id.master_ms_address);
            txtIcon = (TextView) itemLayoutView.findViewById(R.id.master_ms_item_icon);

        }
    }
}
