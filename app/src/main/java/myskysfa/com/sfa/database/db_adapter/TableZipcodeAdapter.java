package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableZipcode;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/17/2016.
 */
public class TableZipcodeAdapter {

    static private TableZipcodeAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableZipcodeAdapter(ctx);
        }
    }
    static public TableZipcodeAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableZipcodeAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableZipcode> getAllData() {
        List<TableZipcode> tblsatu = null;
        try {
            tblsatu = getHelper().getTableZipcodeDAO().queryBuilder().orderBy("ZIPCODE", false).query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> getDatabyCondition(String condition, Object param) {
        List<TableZipcode> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            Where<TableZipcode, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("ZIPCODE", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> fetchFindByandSearch(String condition, Object param) {
        List<TableZipcode> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            Where<TableZipcode, Integer> where = queryBuilder.where();
            where.like(condition, param);
            queryBuilder.orderBy("ZIPCODE", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> listProvinsi() {
        List<TableZipcode> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcode.fDISTRICT_CODE).selectColumns(TableZipcode.fDISTRICT_NAME);
            queryBuilder.groupBy(TableZipcode.fDISTRICT_CODE);
            queryBuilder.groupBy(TableZipcode.fDISTRICT_NAME);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> listCity(String param) {
        List<TableZipcode> tblsatu = null;
        int count = 0;
        String result = "";
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcode.fCITY_CODE).selectColumns(TableZipcode.fCITY_NAME);
            Where<TableZipcode, Integer> where = queryBuilder.where();
            where.eq(TableZipcode.fDISTRICT_CODE, param);
            queryBuilder.groupBy(TableZipcode.fCITY_CODE);
            queryBuilder.groupBy(TableZipcode.fCITY_NAME);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> listKecamatan(String param) {
        List<TableZipcode> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcode.fAREA_CODE).selectColumns(TableZipcode.fAREA_NAME);
            Where<TableZipcode, Integer> where = queryBuilder.where();
            where.eq(TableZipcode.fCITY_CODE, param);
            queryBuilder.groupBy(TableZipcode.fAREA_CODE);
            queryBuilder.groupBy(TableZipcode.fAREA_NAME);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> listKelurahan(String param) {
        List<TableZipcode> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcode.fSTREET_CODE).selectColumns(TableZipcode.fSTREET_NAME);
            Where<TableZipcode, Integer> where = queryBuilder.where();
            where.eq(TableZipcode.fAREA_CODE, param);
            queryBuilder.groupBy(TableZipcode.fSTREET_CODE);
            queryBuilder.groupBy(TableZipcode.fSTREET_NAME);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableZipcode> listZipcode(String param) {
        List<TableZipcode> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableZipcode.class);
            QueryBuilder<TableZipcode, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.selectColumns(TableZipcode.fZIPCODE);
            Where<TableZipcode, Integer> where = queryBuilder.where();
            where.eq(TableZipcode.fSTREET_CODE, param);
            queryBuilder.groupBy(TableZipcode.fZIPCODE);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }
}
