package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.TablePlanMS;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/13/2016.
 */
public class TablePlanMSAdapter {

    static private TablePlanMSAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TablePlanMSAdapter(ctx);
        }
    }
    static public TablePlanMSAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TablePlanMSAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TablePlanMS> getAllData() {
        List<TablePlanMS> tblsatu = null;
        try {
            dao = helper.getDao(TablePlanMS.class);
            QueryBuilder<TablePlanMS, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(TablePlanMS.KEY_ASSIGNMENT_ID,false);
            tblsatu = queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }


    /**
     * Get Data By ID
     * @return
     */
    public  List<TablePlanMS> getDatabySfl(Context context, String condition, Object param) {
        List<TablePlanMS> tblsatu = null;
        int count = 0;
        SharedPreferences sessionPref = context.getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        String logSflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
        try {
            dao = helper.getDao(TablePlanMS.class);
            QueryBuilder<TablePlanMS, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePlanMS, Integer> where = queryBuilder.where();
            where.eq(TablePlanMS.KEY_SFL_CODE, logSflCode).and().eq(condition, param);
            queryBuilder.orderBy(TablePlanMS.KEY_ASSIGNMENT_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TablePlanMS> getDatabyCondition(String condition, Object param) {
        List<TablePlanMS> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TablePlanMS.class);
            QueryBuilder<TablePlanMS, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePlanMS, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            //queryBuilder.orderBy(TablePlanMS.KEY_ASSIGNMENT_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }


    /**
     * Insert Data
     */
    public void insertData(TablePlanMS tbl, String assignment_id, String assignment_date ,String ts_name,
                           String sfl_code,String value, String status, String entity_id) {
        try {
            tbl.setAssignment_id(assignment_id);
            tbl.setAssignment_date(assignment_date);
            tbl.setTs_name(ts_name);
            tbl.setSfl_code(sfl_code);
            tbl.setValue(value);
            tbl.setStatus(status);
            tbl.setEntity_id(entity_id);
            getHelper().getTablePlanMSDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePlan.class);
            UpdateBuilder<TablePlan, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteAll() {
        List<TablePlanMS> tblsatu = null;
        try {
            tblsatu = getHelper().getTablePlanMSDAO().queryForAll();
            getHelper().getTablePlanMSDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePlanMS.class);
            DeleteBuilder<TablePlanMS, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
}
