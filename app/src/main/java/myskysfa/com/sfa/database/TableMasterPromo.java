package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_promo")
public class TableMasterPromo {

    public static final String TABLE_NAME = "m_promo";
    public static final String FLD_ROWID = "promotion_id";
    public static final String FLD_CODE = "promotion_code";
    public static final String FLD_DESC = "promotion_desc";
    public static final String FLD_DATE_START = "start_date";
    public static final String FLD_DATE_END = "end_date";
    public static final String FLD_STATUS = "status";
    public static final String FLD_FLAG = "flag_promo";

    @DatabaseField(id = true)
    private String promotion_id;

    @DatabaseField
    private String promotion_code, promotion_desc, start_date, end_date, status, flag_promo;

    public String getPromotion_id() {
        return promotion_id;
    }

    public void setPromotion_id(String promotion_id) {
        this.promotion_id = promotion_id;
    }

    public String getPromotion_code() {
        return promotion_code;
    }

    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }

    public String getPromotion_desc() {
        return promotion_desc;
    }

    public void setPromotion_desc(String promotion_desc) {
        this.promotion_desc = promotion_desc;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag_promo() {
        return flag_promo;
    }

    public void setFlag_promo(String flag_promo) {
        this.flag_promo = flag_promo;
    }




}
