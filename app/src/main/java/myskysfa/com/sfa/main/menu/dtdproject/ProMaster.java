package myskysfa.com.sfa.main.menu.dtdproject;


import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.FreeTrialSohAdapter;
import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProMaster extends Fragment {

    public String mFilter;
    public String mFilterInt, imei;
    private List<TableFreeTrialCoh> mListMaster;
    private FreeTrialSohAdapter mAdapterCoh;
    private RecyclerView mRecyclerView;
    protected ViewGroup mView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean isTaskRunning = false;
    private ProgressDialog _progressDialog;
    private String url, response, status, data;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Utils utils;
    private Handler handler = new Handler();
    private int signal;
    private ArrayList<TableLogLogin> listLogin = new ArrayList<>();
    private TableLogLoginAdapter loginAdapter;
    private TableFTSOHAdapter db;

    public ProMaster() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = (ViewGroup) inflater.inflate(R.layout.ft_master, container, false);
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.list_ft_master);
        mSwipeRefreshLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipe_ft_master);
        try {
            mFilter = this.getArguments().getString("filter");
            utils = new Utils(getActivity());
            utils.setIMEI();
            mSwipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Log.d("mFileter", mFilter);
                    if (mFilter.equalsIgnoreCase("Archive")) {
                        reloadRecycle();
                    } else {
                        refreshItems();
                    }
                }
            });
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (mFilter.equals("Archive")) {
                        mFilterInt = "DEL";
                    } else {
                        mFilterInt = "ADD";
                        refreshItems();
                        //reloadRecycle();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


        return mView;
    }

    private void refreshItems() {
        cd = new ConnectionDetector(getContext());
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new AttemptRequest().execute();
        } else {
            utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1), getResources().getString(R.string.no_internet2), false);
        }
    }

    private void reloadRecycle() {
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                TableFTSOHAdapter db = new TableFTSOHAdapter(getContext());
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                mRecyclerView.setLayoutManager(layoutManager);
                mListMaster = db.getDatabyCondition(
                        TableFreeTrialCoh.KEY_HW_STATUS, "1");
                mAdapterCoh = new FreeTrialSohAdapter(mListMaster);
                mRecyclerView.setAdapter(mAdapterCoh);
            }
        });
    }

    /**
     * Class Asynctask for request SOH
     */
    private class AttemptRequest extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSwipeRefreshLayout.setRefreshing(true);
            db = new TableFTSOHAdapter(getContext());
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                imei = utils.getIMEI();
                loginAdapter = new TableLogLoginAdapter(getContext());
                List<TableLogLogin> listLogin = loginAdapter.getAllData();
                String sflCode = listLogin.get(listLogin.size() - 1).getcSflCode();
                String token = listLogin.get(listLogin.size() - 1).getcToken();
                url = ConnectionManager.CM_URL_FT_SOH + sflCode;
                //url = ConnectionManager.CM_URL_FT_SOH+"01011204";
                System.out.println(url);
                response = ConnectionManager.requestFtSoh(url, token, imei, Config.version, String.valueOf(signal), getContext());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS).toString();
                    data = jsonObject.getString(Config.KEY_DATA).toString();
                    JSONObject obj = new JSONObject(data);
                    if (status.equals("true")) {
                        JSONArray jsonData = new JSONArray(obj.getString("serialize"));

                        if (jsonData != null) {
                            db.delete(getActivity()); // delete all master
                            for (int i = 0; i < jsonData.length(); i++) {
                                String sn = jsonData.getJSONObject(i).getString(Config.KEY_M_SN);
                                String type = jsonData.getJSONObject(i).getString(Config.KEY_M_TYPE);
                                String hw_id = jsonData.getJSONObject(i).getString(Config.KEY_M_ID);
                                String name = jsonData.getJSONObject(i).getString(Config.KEY_M_NAME);
                                String out_date = jsonData.getJSONObject(i).getString(Config.KEY_M_OUT_DATE);
                                String status = jsonData.getJSONObject(i).getString(Config.KEY_M_STATUS);
                                String isSerial = jsonData.getJSONObject(i).getString(Config.KEY_M_ISSERIAL);
                                String price = jsonData.getJSONObject(i).getString(Config.KEY_M_PRICE);

                                //String package_id = jsonData.getJSONObject(i).getString(Config.KEY_M_PACKAGE_ID);
                                String package_id = "";
                                db.insertData(new TableFreeTrialCoh(), sn, type, name, out_date,
                                        package_id, status, isSerial, hw_id, price);
                            }
                        }
                        JSONArray jsonUnSerialize = new JSONArray(obj.getString("unserialize"));

                        if (jsonUnSerialize != null) {
                            for (int i = 0; i < jsonUnSerialize.length(); i++) {
                                String sn = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_SN);
                                String type = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_TYPE);
                                String hw_id = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_ID);
                                String name = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_NAME);
                                String out_date = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_OUT_DATE);
                                String status = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_STATUS);
                                String isSerial = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_ISSERIAL);
                                String price = jsonUnSerialize.getJSONObject(i).getString(Config.KEY_M_PRICE);

                                //String package_id = jsonData.getJSONObject(i).getString(Config.KEY_M_PACKAGE_ID);
                                String package_id = "";
                                db.insertData(new TableFreeTrialCoh(), sn, type, name, out_date,
                                        package_id, status, isSerial, hw_id, price);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mSwipeRefreshLayout.setRefreshing(false);
            if (status != null && status.equals("true")) {
                reloadRecycle();
            } else {
                if (mSwipeRefreshLayout.isRefreshing()) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                try {
                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
                            getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
