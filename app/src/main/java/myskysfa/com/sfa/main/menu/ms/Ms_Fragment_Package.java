package myskysfa.com.sfa.main.menu.ms;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.MSDTDPackage;
import myskysfa.com.sfa.main.menu.dtd.DTDStatus;
import myskysfa.com.sfa.utils.Utils;


/**
 * Created by Eno on 6/13/2016.
 */
public class Ms_Fragment_Package extends Fragment {
    private ViewGroup root;
    //private Utils utils;
    //private int totPakacge;
    private String numberForm;
    private static Context _context;
    public MSDTDPackage msdtdPackage;
    private LinearLayout linier_add, liner_package, layout_price_charge, layout_price_hardware, layout_price_bundling, layout_price_transport;
    private Spinner spinner_type, spinner_brand, spinnner_bundling, billFrek;
    private SearchableSpinner spinner_promo;
    private TextView priceInstalasi, pricePacket, pricePromo,promoDiscDescryption, promoAmountDescryption, priceAlacarte, promoDisc, promoAmount, priceOffer, price_charge, price_hardware, price_bundling, priceTransport;
    private RecyclerView promoService;
    private Button save;
    private Utils utils;

    public static Fragment newInstance(Context context) {
        _context = context;
        Ms_Fragment_Package dtdPaket = new Ms_Fragment_Package();
        return dtdPaket;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.ms_fragment_package, container, false);
        utils = new Utils(getActivity());
        //handler = new Handler();
        //totPakacge = 0;

        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms), Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
        initView(root);
        return root;
    }

    private void initView(ViewGroup mView) {
        linier_add = (LinearLayout) mView.findViewById(R.id.linier_package);
        liner_package = (LinearLayout) mView.findViewById(R.id.package_list);
        spinner_type = (Spinner) mView.findViewById(R.id.spinner_type);
        spinner_brand = (Spinner) mView.findViewById(R.id.spinner_brand);
        spinner_promo = (SearchableSpinner) mView.findViewById(R.id.spinner_promo);
        pricePacket = (TextView) mView.findViewById(R.id.pricePaket);
        pricePromo = (TextView) mView.findViewById(R.id.pricePromo);
        priceInstalasi = (TextView) mView.findViewById(R.id.priceInst);
        priceAlacarte = (TextView) mView.findViewById(R.id.priceAlacarte);
        spinnner_bundling = (Spinner) mView.findViewById(R.id.spinner_bundling);
        promoDisc = (TextView) mView.findViewById(R.id.promoDisc);
        promoAmount = (TextView) mView.findViewById(R.id.promoAmount);
        promoService = (RecyclerView) mView.findViewById(R.id.checlist_service);
        priceOffer = (TextView) mView.findViewById(R.id.priceOffer);
        billFrek = (Spinner) mView.findViewById(R.id.list_payment_period);
        layout_price_charge = (LinearLayout) mView.findViewById(R.id.layout_price_charge);
        layout_price_hardware = (LinearLayout) mView.findViewById(R.id.layout_price_hardware);
        layout_price_bundling = (LinearLayout) mView.findViewById(R.id.layout_price_bundling);
        layout_price_transport = (LinearLayout) mView.findViewById(R.id.layout_price_transport);
        priceTransport = (TextView) mView.findViewById(R.id.priceTransport);
        price_charge = (TextView) mView.findViewById(R.id.price_charge);
        price_hardware = (TextView) mView.findViewById(R.id.price_hardware);
        price_bundling = (TextView) mView.findViewById(R.id.price_bundling);
        save = (Button) mView.findViewById(R.id.save);
        promoDiscDescryption = (TextView) mView.findViewById(R.id.promoDiscDescryption);
        promoAmountDescryption = (TextView) mView.findViewById(R.id.promoAmountDescryption);

        msdtdPackage = new MSDTDPackage(getActivity(), mView, linier_add, liner_package, spinner_type,
                spinner_brand, spinner_promo, priceInstalasi, pricePacket, pricePromo, promoDiscDescryption, promoAmountDescryption, numberForm,
                "m",spinnner_bundling, priceAlacarte, promoDisc,promoAmount, promoService, priceOffer, billFrek,
                layout_price_charge, layout_price_hardware, layout_price_bundling, layout_price_transport, price_charge, price_hardware, price_bundling, priceTransport, save);
        msdtdPackage.InitView();
    }

    public void setFormNumber() {
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms),
                Context.MODE_PRIVATE);
        numberForm = sm.getString("fn", null);
    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), root);
    }

}
