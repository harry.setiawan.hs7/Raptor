/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.zxing.client.androidlegacy;

public final class R {
    public static final class array {
        public static final int zxinglegacy_country_codes = 0x7f100023;
        public static final int zxinglegacy_preferences_front_light_values = 0x7f100024;
    }
    public static final class color {
        public static final int zxinglegacy_contents_text = 0x7f0f0204;
        public static final int zxinglegacy_encode_view = 0x7f0f0205;
        public static final int zxinglegacy_possible_result_points = 0x7f0f0206;
        public static final int zxinglegacy_result_minor_text = 0x7f0f0207;
        public static final int zxinglegacy_result_points = 0x7f0f0208;
        public static final int zxinglegacy_result_text = 0x7f0f0209;
        public static final int zxinglegacy_result_view = 0x7f0f020a;
        public static final int zxinglegacy_status_text = 0x7f0f020b;
        public static final int zxinglegacy_transparent = 0x7f0f020c;
        public static final int zxinglegacy_viewfinder_laser = 0x7f0f020d;
        public static final int zxinglegacy_viewfinder_mask = 0x7f0f020e;
    }
    public static final class dimen {
        public static final int zxinglegacy_half_padding = 0x7f0b00dc;
        public static final int zxinglegacy_standard_padding = 0x7f0b00dd;
    }
    public static final class id {
        public static final int format_text_view = 0x7f1102e9;
        public static final int menu_encode = 0x7f110301;
        public static final int menu_help = 0x7f1102ff;
        public static final int menu_share = 0x7f110300;
        public static final int meta_text_view_label = 0x7f1102ec;
        public static final int zxinglegacy_back_button = 0x7f110026;
        public static final int zxinglegacy_barcode_image_view = 0x7f1102e8;
        public static final int zxinglegacy_contents_supplement_text_view = 0x7f1102ef;
        public static final int zxinglegacy_contents_text_view = 0x7f1102ee;
        public static final int zxinglegacy_decode = 0x7f110027;
        public static final int zxinglegacy_decode_failed = 0x7f110028;
        public static final int zxinglegacy_decode_succeeded = 0x7f110029;
        public static final int zxinglegacy_done_button = 0x7f1102f4;
        public static final int zxinglegacy_help_contents = 0x7f1102f3;
        public static final int zxinglegacy_image_view = 0x7f1102f2;
        public static final int zxinglegacy_launch_product_query = 0x7f11002a;
        public static final int zxinglegacy_meta_text_view = 0x7f1102ed;
        public static final int zxinglegacy_preview_view = 0x7f1102e5;
        public static final int zxinglegacy_quit = 0x7f11002b;
        public static final int zxinglegacy_restart_preview = 0x7f11002c;
        public static final int zxinglegacy_result_button_view = 0x7f1102f0;
        public static final int zxinglegacy_result_view = 0x7f1102e7;
        public static final int zxinglegacy_return_scan_result = 0x7f11002d;
        public static final int zxinglegacy_status_view = 0x7f1102f1;
        public static final int zxinglegacy_time_text_view = 0x7f1102eb;
        public static final int zxinglegacy_type_text_view = 0x7f1102ea;
        public static final int zxinglegacy_viewfinder_view = 0x7f1102e6;
    }
    public static final class layout {
        public static final int zxinglegacy_capture = 0x7f0400a5;
        public static final int zxinglegacy_encode = 0x7f0400a6;
        public static final int zxinglegacy_help = 0x7f0400a7;
    }
    public static final class menu {
        public static final int zxinglegacy_capture = 0x7f120007;
        public static final int zxinglegacy_encode = 0x7f120008;
    }
    public static final class raw {
        public static final int zxinglegacy_beep = 0x7f090002;
    }
    public static final class string {
        public static final int zxinglegacy_app_name = 0x7f0a007f;
        public static final int zxinglegacy_button_back = 0x7f0a0080;
        public static final int zxinglegacy_button_cancel = 0x7f0a0081;
        public static final int zxinglegacy_button_done = 0x7f0a0082;
        public static final int zxinglegacy_button_ok = 0x7f0a0083;
        public static final int zxinglegacy_contents_contact = 0x7f0a0084;
        public static final int zxinglegacy_contents_email = 0x7f0a0085;
        public static final int zxinglegacy_contents_location = 0x7f0a0086;
        public static final int zxinglegacy_contents_phone = 0x7f0a0087;
        public static final int zxinglegacy_contents_sms = 0x7f0a0088;
        public static final int zxinglegacy_contents_text = 0x7f0a0089;
        public static final int zxinglegacy_menu_encode_mecard = 0x7f0a008a;
        public static final int zxinglegacy_menu_encode_vcard = 0x7f0a008b;
        public static final int zxinglegacy_menu_help = 0x7f0a008c;
        public static final int zxinglegacy_menu_share = 0x7f0a008d;
        public static final int zxinglegacy_msg_camera_framework_bug = 0x7f0a008e;
        public static final int zxinglegacy_msg_default_format = 0x7f0a008f;
        public static final int zxinglegacy_msg_default_meta = 0x7f0a0090;
        public static final int zxinglegacy_msg_default_status = 0x7f0a0091;
        public static final int zxinglegacy_msg_default_time = 0x7f0a0092;
        public static final int zxinglegacy_msg_default_type = 0x7f0a0093;
        public static final int zxinglegacy_msg_encode_contents_failed = 0x7f0a0094;
        public static final int zxinglegacy_msg_unmount_usb = 0x7f0a0095;
    }
    public static final class xml {
        public static final int zxinglegacy_preferences = 0x7f080002;
    }
}
