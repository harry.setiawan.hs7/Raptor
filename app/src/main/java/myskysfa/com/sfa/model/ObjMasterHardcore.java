package myskysfa.com.sfa.model;

/**
 * Created by harry_setiawan on 16/10/17.
 */

public class ObjMasterHardcore {
    private String id;
    private String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
