package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_modernstore")
public class TableMasterMS {

    public static final String FLD_ROWID = "id";
    public static final String FLD_NAME = "name";
    public static final String FLD_ADDRESS = "address";
    public static final String FLD_ZIPCODE = "post_code";
    public static final String FLD_LATITUDE = "latitude";
    public static final String FLD_LONGITUDE = "longitude";
    public static final String FLD_DATE_START = "date_start";
    public static final String FLD_DATE_END = "date_end";
    public static final String FLD_POS = "id_pos";
    public static final String FLD_CODE = "ms_code";
    public static final String FLD_ENTITY = "entity";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String name;
    @DatabaseField
    private String address;
    @DatabaseField
    private String post_code;
    @DatabaseField
    private String latitude;
    @DatabaseField
    private String longitude;
    @DatabaseField
    private String date_start;
    @DatabaseField
    private String date_end;
    @DatabaseField
    private String id_pos;
    @DatabaseField
    private String ms_code;
    @DatabaseField
    private String entity;

    public String getMs_code() {
        return ms_code;
    }

    public void setMs_code(String ms_code) {
        this.ms_code = ms_code;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getId_pos() {
        return id_pos;
    }

    public void setId_pos(String id_pos) {
        this.id_pos = id_pos;
    }


}
