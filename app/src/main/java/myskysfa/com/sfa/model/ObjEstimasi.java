package myskysfa.com.sfa.model;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by harry_setiawan on 29/09/17.
 */

public class ObjEstimasi {
    BigInteger BF  = new BigInteger("0");
    ArrayList<BigInteger> harga_basic;
    ArrayList<BigInteger> harga_alacarte;
    BigInteger harga_installation = new BigInteger("200000");
    BigInteger total_discount_installation = new BigInteger("0");
    BigInteger total_harga_installation_with_discount  = new BigInteger("0");
    BigInteger total_harga_basic  = new BigInteger("0");
    BigInteger total_harga_alacarte  = new BigInteger("0");
    ArrayList<BigInteger> harga_hardware;
    BigInteger total_harga_hardware  = new BigInteger("0");
    BigInteger harga_additional  = new BigInteger("0");
    BigInteger total_additional  = new BigInteger("0");
    BigInteger harga_bundling  = new BigInteger("0");
    ArrayList<BigInteger> harga_charge;
    BigInteger discount_harga_charge  = new BigInteger("0");
    BigInteger total_harga_charge  = new BigInteger("0");
    BigInteger total_discount__bundling  = new BigInteger("0");
    BigInteger total_harga_bundling  = new BigInteger("0");
    BigInteger total_harga_bundling_with_discount  = new BigInteger("0");
    BigInteger total_discount_basic_alacarte  = new BigInteger("0");
    BigInteger total_discount_basic  = new BigInteger("0");
    BigInteger total_discount_alacarte  = new BigInteger("0");
    BigInteger total_harga_basic_with_discount  = new BigInteger("0");
    BigInteger total_harga_alacarte_with_discount  = new BigInteger("0");
    BigInteger total_offer  = new BigInteger("0");
    BigInteger total_bayar  = new BigInteger("0");
    BigInteger total_transport  = new BigInteger("0");

    public BigInteger getBF() {
        return BF;
    }

    public BigInteger getDiscount_harga_charge() {
        return discount_harga_charge;
    }

    public void setDiscount_harga_charge(BigInteger discount_harga_charge) {
        this.discount_harga_charge = discount_harga_charge;
    }

    public void setBF(BigInteger BF) {
        this.BF = BF;
    }

    public ArrayList<BigInteger> getHarga_basic() {
        return harga_basic;
    }

    public void setHarga_basic(ArrayList<BigInteger> harga_basic) {
        this.harga_basic = harga_basic;
    }

    public ArrayList<BigInteger> getHarga_alacarte() {
        return harga_alacarte;
    }

    public void setHarga_alacarte(ArrayList<BigInteger> harga_alacarte) {
        this.harga_alacarte = harga_alacarte;
    }

    public BigInteger getHarga_installation() {
        return harga_installation;
    }

    public void setHarga_installation(BigInteger harga_installation) {
        this.harga_installation = harga_installation;
    }

    public BigInteger getTotal_discount_installation() {
        return total_discount_installation;
    }

    public void setTotal_discount_installation(BigInteger total_discount_installation) {
        this.total_discount_installation = total_discount_installation;
    }

    public BigInteger getTotal_harga_installation_with_discount() {
        return total_harga_installation_with_discount;
    }

    public void setTotal_harga_installation_with_discount(BigInteger total_harga_installation_with_discount) {
        this.total_harga_installation_with_discount = total_harga_installation_with_discount;
    }

    public BigInteger getTotal_harga_basic() {
        return total_harga_basic;
    }

    public void setTotal_harga_basic(BigInteger total_harga_basic) {
        this.total_harga_basic = total_harga_basic;
    }

    public BigInteger getTotal_harga_alacarte() {
        return total_harga_alacarte;
    }

    public void setTotal_harga_alacarte(BigInteger total_harga_alacarte) {
        this.total_harga_alacarte = total_harga_alacarte;
    }

    public ArrayList<BigInteger> getHarga_hardware() {
        return harga_hardware;
    }

    public void setHarga_hardware(ArrayList<BigInteger> harga_hardware) {
        this.harga_hardware = harga_hardware;
    }

    public BigInteger getTotal_harga_hardware() {
        return total_harga_hardware;
    }

    public void setTotal_harga_hardware(BigInteger total_harga_hardware) {
        this.total_harga_hardware = total_harga_hardware;
    }

    public BigInteger getHarga_additional() {
        return harga_additional;
    }

    public void setHarga_additional(BigInteger harga_additional) {
        this.harga_additional = harga_additional;
    }

    public BigInteger getTotal_additional() {
        return total_additional;
    }

    public void setTotal_additional(BigInteger total_additional) {
        this.total_additional = total_additional;
    }

    public BigInteger getHarga_bundling() {
        return harga_bundling;
    }

    public void setHarga_bundling(BigInteger harga_bundling) {
        this.harga_bundling = harga_bundling;
    }

    public ArrayList<BigInteger> getHarga_charge() {
        return harga_charge;
    }

    public void setHarga_charge(ArrayList<BigInteger> harga_charge) {
        this.harga_charge = harga_charge;
    }

    public BigInteger getTotal_harga_charge() {
        return total_harga_charge;
    }

    public void setTotal_harga_charge(BigInteger total_harga_charge) {
        this.total_harga_charge = total_harga_charge;
    }

    public BigInteger getTotal_discount__bundling() {
        return total_discount__bundling;
    }

    public void setTotal_discount__bundling(BigInteger total_discount__bundling) {
        this.total_discount__bundling = total_discount__bundling;
    }

    public BigInteger getTotal_harga_bundling() {
        return total_harga_bundling;
    }

    public void setTotal_harga_bundling(BigInteger total_harga_bundling) {
        this.total_harga_bundling = total_harga_bundling;
    }

    public BigInteger getTotal_harga_bundling_with_discount() {
        return total_harga_bundling_with_discount;
    }

    public void setTotal_harga_bundling_with_discount(BigInteger total_harga_bundling_with_discount) {
        this.total_harga_bundling_with_discount = total_harga_bundling_with_discount;
    }

    public BigInteger getTotal_discount_basic_alacarte() {
        return total_discount_basic_alacarte;
    }

    public void setTotal_discount_basic_alacarte(BigInteger total_discount_basic_alacarte) {
        this.total_discount_basic_alacarte = total_discount_basic_alacarte;
    }

    public BigInteger getTotal_discount_basic() {
        return total_discount_basic;
    }

    public void setTotal_discount_basic(BigInteger total_discount_basic) {
        this.total_discount_basic = total_discount_basic;
    }

    public BigInteger getTotal_discount_alacarte() {
        return total_discount_alacarte;
    }

    public void setTotal_discount_alacarte(BigInteger total_discount_alacarte) {
        this.total_discount_alacarte = total_discount_alacarte;
    }

    public BigInteger getTotal_harga_basic_with_discount() {
        return total_harga_basic_with_discount;
    }

    public void setTotal_harga_basic_with_discount(BigInteger total_harga_basic_with_discount) {
        this.total_harga_basic_with_discount = total_harga_basic_with_discount;
    }

    public BigInteger getTotal_harga_alacarte_with_discount() {
        return total_harga_alacarte_with_discount;
    }

    public void setTotal_harga_alacarte_with_discount(BigInteger total_harga_alacarte_with_discount) {
        this.total_harga_alacarte_with_discount = total_harga_alacarte_with_discount;
    }

    public BigInteger getTotal_offer() {
        return total_offer;
    }

    public void setTotal_offer(BigInteger total_offer) {
        this.total_offer = total_offer;
    }

    public BigInteger getTotal_bayar() {
        return total_bayar;
    }

    public void setTotal_bayar(BigInteger total_bayar) {
        this.total_bayar = total_bayar;
    }

    public BigInteger getTotal_transport() {
        return total_transport;
    }

    public void setTotal_transport(BigInteger total_transport) {
        this.total_transport = total_transport;
    }
}