package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.util.List;

import myskysfa.com.sfa.database.TableMasterMaterial;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 6/20/2016.
 */
public class TableMasterMaterialAdapter {
    private static TableMasterMaterialAdapter instance;
    private DatabaseManager helper;

    public static void init(Context ctx) {
        if (null == instance) {
            instance = new TableMasterMaterialAdapter(ctx);
        }
    }

    public TableMasterMaterialAdapter getInstance() {
        return instance;
    }

    public TableMasterMaterialAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMasterMaterial> getAllData() {
        List<TableMasterMaterial> tblData = null;
        try {
            tblData = getHelper().getTableMasterMaterialsDAO()
                    .queryBuilder()
                    .orderBy(TableMasterMaterial.KEY_DESC, true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblData;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMasterMaterial tbl, String id, String code, String description,
                           String rate, String class_code, String is_package) {
        try {
            tbl.setId(id);
            tbl.setCode(code);
            tbl.setDescription(description);
            tbl.setRate(rate);
            tbl.setClass_code(class_code);
            tbl.setIs_package(is_package);
            getHelper().getTableMasterMaterialsDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            Dao dao = helper.getDao(TableMasterMaterial.class);
            DeleteBuilder<TableMasterMaterial, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
