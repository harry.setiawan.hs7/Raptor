package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/20/2016.
 */
public class TableFormAppAdapter {

    static private TableFormAppAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFormAppAdapter(ctx);
        }
    }

    static public TableFormAppAdapter getInstance() {
        return instance;
    }

    public DatabaseManager helper;

    public TableFormAppAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFormApp> getAllData() {
        List<TableFormApp> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFormAppsDAODAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableFormApp> getDatabyCondition(String condition, Object param) {
        List<TableFormApp> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableFormApp.class);
            QueryBuilder<TableFormApp, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFormApp, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableFormApp> getDatalikeCondition(String condition, Object param) {
        List<TableFormApp> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableFormApp.class);
            QueryBuilder<TableFormApp, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFormApp, Integer> where = queryBuilder.where();
            where.like(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Method for count data by condition
     */
    public Long getDataCount(String condition,String param) {
        Long usuarios = 0L;
        try {
            dao = helper.getDao(TableFormApp.class);
            QueryBuilder queryBuilder = dao.queryBuilder();
            queryBuilder.setCountOf(true);
            queryBuilder.setWhere(queryBuilder.where().eq(condition, param));
            usuarios = dao.countOf(queryBuilder.prepare());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarios;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFormApp tbl, String form_no, String form_type, String user_id,
                           String trans_no, String date_reg, String identity_type, String identity_no,
                           String first_name, String last_name, String birth_date,
                           String values_profile, String values_emergency, String values_payment,
                           String values_package, String values_signature,String values_checklist, String form_status,
                           String cust_id, String value_account) {
        try {
            tbl.setFORM_NO(form_no);
            tbl.setFORM_TYPE(form_type);
            tbl.setUSER_ID(user_id);
            tbl.setTRANS_NO(trans_no);
            tbl.setDATE_REG(date_reg);
            tbl.setIDENTITY_TYPE(identity_type);
            tbl.setIDENTITY_NO(identity_no);
            tbl.setFIRST_NAME(first_name);
            tbl.setLAST_NAME(last_name);
            tbl.setBIRTH_DATE(birth_date);
            tbl.setVALUES_PROFILE(values_profile);
            tbl.setVALUES_EMERGENCY(values_emergency);
            tbl.setVALUES_PAYMENT(values_payment);
            tbl.setVALUES_PACKAGE(values_package);
            tbl.setVALUES_SIGNATURE(values_signature);
            tbl.setVALUES_CHECKLIST(values_checklist);
            tbl.setFORM_STATUS(form_status);
            tbl.setCUST_ID(cust_id);
            tbl.setVALUES_SIGNACCOUNT(value_account);
            getHelper().getTableFormAppsDAODAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Update by Condition
    *  column = nama coloumn
    *  object value =
    *  string condition =
    *  object param =
    */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFormApp.class);
            UpdateBuilder<TableFormApp, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
            Log.d("updateBy", "update suskes");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String form_no, String form_type, String user_id,
                          String trans_no, String date_reg, String identity_type, String identity_no,
                          String first_name, String last_name, String birth_date,
                          String values_profile, String values_emergency, String values_payment,
                          String values_package, String values_signature,String values_checklist,String form_status,
                          String cust_id,String values_account, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFormApp.class);
            UpdateBuilder<TableFormApp, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFormApp.fFORM_NO, form_no);
            updateBuilder.updateColumnValue(TableFormApp.fFORM_TYPE, form_type);
            updateBuilder.updateColumnValue(TableFormApp.fUSER_ID, user_id);
            updateBuilder.updateColumnValue(TableFormApp.fTRANS_NO, trans_no);
            updateBuilder.updateColumnValue(TableFormApp.fDATE_REG, date_reg);
            updateBuilder.updateColumnValue(TableFormApp.fIDENTITY_TYPE, identity_type);
            updateBuilder.updateColumnValue(TableFormApp.fIDENTITY_NO, identity_no);
            updateBuilder.updateColumnValue(TableFormApp.fFIRST_NAME, first_name);
            updateBuilder.updateColumnValue(TableFormApp.fLAST_NAME, last_name);
            updateBuilder.updateColumnValue(TableFormApp.fBIRTH_DATE, birth_date);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_PROFILE, values_profile);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_EMERGENCY, values_emergency);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_PAYMENT, values_payment);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_PACKAGE, values_package);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_SIGNACCOUNT, values_account);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_SIGNATURE, values_signature);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_CHECKLIST, values_checklist);
            updateBuilder.updateColumnValue(TableFormApp.fFORM_STATUS, form_status);
            updateBuilder.updateColumnValue(TableFormApp.fCUST_ID, cust_id);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFormApp.class);
            DeleteBuilder<TableFormApp, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
            Log.d("deleteBy", "delete sukes");
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            Log.d("deleteBy", "delete gagal");
        }
    }

    public void deleteAll() {
        List<TableFormApp> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFormAppsDAODAO().queryForAll();
            getHelper().getTableFormAppsDAODAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
