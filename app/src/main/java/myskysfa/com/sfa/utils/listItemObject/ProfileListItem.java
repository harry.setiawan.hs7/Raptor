package myskysfa.com.sfa.utils.listItemObject;

import android.content.Context;

/**
 * Created by Hari Hendryan on 11/5/2015.
 */
public class ProfileListItem {
    private Context context;
    private String formNo;
    private String regDate;
    private String storeType;
    private String storeDetail;
    private String identityType;
    private String identityNo;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private String religion;
    private String birthPlace;
    private String birthDate;
    private String address;
    private String zipCode;
    private String kelurahan;
    private String kecamatan;
    private String city;
    private String province;
    private String validityPeriod;
    private String installDate;
    private String installTime;
    private String confirmTimeStart;
    private String confirmTimeEnd;
    private String installAddress;
    private String installZipCode;
    private String installKelurahan;
    private String installKecamatan;
    private String installCity;
    private String installProvince;
    private String billingAddress;
    private String billingZipCode;
    private String billingKelurahan;
    private String billingKecamatan;
    private String billingCity;
    private String billingProvince;
    private String telephone;
    private String handPhone;
    private String emailAddress;
    private String brand;
    private boolean print= false;
    private String hardwareStatus;
    private String homeType;
    private String homeStatus;
    private String occupation;
    private String income;
    private String imagePath;
    private String form_type;
    private String user_id;
    private String homeNo;
    private String rt;
    private String rw;
    private String direction;

    public ProfileListItem(Context context) {
        this.context = context;
    }

    public void setFormType(String form_type) {
        this.form_type = form_type;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public void setBillingKecamatan(String billingKecamatan) {
        this.billingKecamatan = billingKecamatan;
    }

    public void setBillingKelurahan(String billingKelurahan) {
        this.billingKelurahan = billingKelurahan;
    }

    public void setBillingProvince(String billingProvince) {
        this.billingProvince = billingProvince;
    }

    public void setBillingZipCode(String billingZipCode) {
        this.billingZipCode = billingZipCode;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setConfirmTimeEnd(String confirmTimeEnd) {
        this.confirmTimeEnd = confirmTimeEnd;
    }

    public void setConfirmTimeStart(String confirmTimeStart) {
        this.confirmTimeStart = confirmTimeStart;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setFormNo(String formNo) {
        this.formNo = formNo;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setHandPhone(String handPhone) {
        this.handPhone = handPhone;
    }

    public void setHardwareStatus(String hardwareStatus) {
        this.hardwareStatus = hardwareStatus;
    }

    public void setHomeStatus(String homeStatus) {
        this.homeStatus = homeStatus;
    }

    public void setHomeType(String homeType) {
        this.homeType = homeType;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public void setInstallAddress(String installAddress) {
        this.installAddress = installAddress;
    }

    public void setInstallCity(String installCity) {
        this.installCity = installCity;
    }

    public void setInstallDate(String installDate) {
        this.installDate = installDate;
    }

    public void setInstallKecamatan(String installKecamatan) {
        this.installKecamatan = installKecamatan;
    }

    public void setInstallKelurahan(String installKelurahan) {
        this.installKelurahan = installKelurahan;
    }

    public void setInstallProvince(String installProvince) {
        this.installProvince = installProvince;
    }

    public void setInstallTime(String installTime) {
        this.installTime = installTime;
    }

    public void setInstallZipCode(String installZipCode) {
        this.installZipCode = installZipCode;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setPrint(boolean print) {
        this.print = print;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public void setStoreDetail(String storeDetail) {
        this.storeDetail = storeDetail;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setHomeNo(String homeNo) {
        this.homeNo = homeNo;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public void setDirection(String direction){
        this.direction = direction;
    }

    public String getDirection(){
        return direction;
    }

    public String getAddress() {
        return address;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public String getBillingKecamatan() {
        return billingKecamatan;
    }

    public String getBillingKelurahan() {
        return billingKelurahan;
    }

    public String getBillingProvince() {
        return billingProvince;
    }

    public String getBillingZipCode() {
        return billingZipCode;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public String getBrand() {
        return brand;
    }

    public String getCity() {
        return city;
    }

    public String getConfirmTimeEnd() {
        return confirmTimeEnd;
    }

    public String getConfirmTimeStart() {
        return confirmTimeStart;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFormNo() {
        return formNo;
    }

    public String getGender() {
        return gender;
    }

    public String getHandPhone() {
        return handPhone;
    }

    public String getHardwareStatus() {
        return hardwareStatus;
    }

    public String getHomeStatus() {
        return homeStatus;
    }

    public String getHomeType() {
        return homeType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public String getIncome() {
        return income;
    }

    public String getIdentityType() {
        return identityType;
    }

    public String getInstallAddress() {
        return installAddress;
    }

    public String getInstallCity() {
        return installCity;
    }

    public String getInstallDate() {
        return installDate;
    }

    public String getInstallKecamatan() {
        return installKecamatan;
    }

    public String getInstallKelurahan() {
        return installKelurahan;
    }

    public String getInstallProvince() {
        return installProvince;
    }

    public String getInstallTime() {
        return installTime;
    }

    public String getInstallZipCode() {
        return installZipCode;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getOccupation() {
        return occupation;
    }

    public String getProvince() {
        return province;
    }

    public String getRegDate() {
        return regDate;
    }

    public String getReligion() {
        return religion;
    }

    public String getStoreDetail() {
        return storeDetail;
    }

    public String getStoreType() {
        return storeType;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public String getZipCode() {
        return zipCode;
    }

    public boolean isPrint() {
        return print;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getFormType() {
        return form_type;
    }

    public String getUserId() {
        return user_id;
    }

    public String getHomeNo() {
        return homeNo;
    }

    public String getRt() {
        return rt;
    }

    public String getRw() {
        return rw;
    }
}
