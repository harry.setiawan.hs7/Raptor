package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/13/2016.
 */
public class TablePlanAdapter {

    static private TablePlanAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TablePlanAdapter(ctx);
        }
    }
    static public TablePlanAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TablePlanAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TablePlan> getAllData() {
        List<TablePlan> tblsatu = null;
        try {
            QueryBuilder<TablePlan, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.orderBy(TablePlan.KEY_PLAN_ID,false);
            tblsatu = queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TablePlan> getDatabyCondition(String condition, Object param) {
        List<TablePlan> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TablePlan.class);
            QueryBuilder<TablePlan, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePlan, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TablePlan.KEY_PLAN_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TablePlan> getDatabySfl(Context context, String condition, Object param) {
        List<TablePlan> tblsatu = null;
        int count = 0;
        SharedPreferences sessionPref = context.getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        String logSflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
        try {
            dao = helper.getDao(TablePlan.class);
            QueryBuilder<TablePlan, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePlan, Integer> where = queryBuilder.where();
            where.eq(TablePlan.KEY_SFL_CODE, logSflCode).and().eq(condition, param);
            queryBuilder.orderBy(TablePlan.KEY_PLAN_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TablePlan tbl, String planId, String plan_date, String zip_code,
                           String area, String ts_code, String ts_name, String target, String result,
                           String sfl_code,String value, String status) {
        try {
            tbl.setPlan_id(planId);
            tbl.setPlan_date(plan_date);
            tbl.setZip_code(zip_code);
            tbl.setArea(area);
            tbl.setTs_code(ts_code);
            tbl.setTs_name(ts_name);
            tbl.setTarget(target);
            tbl.setResult(result);
            tbl.setSfl_code(sfl_code);
            tbl.setValue(value);
            tbl.setStatus(status);
            getHelper().getTablePlanDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePlan.class);
            UpdateBuilder<TablePlan, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String plan_date, String zip_code, String area,
                          String ts_code, String ts_name, String target, String result,
                          String sfl_code,String value, String status, String condition,
                          Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePlan.class);
            UpdateBuilder<TablePlan, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TablePlan.KEY_PLAN_DATE, plan_date);
            updateBuilder.updateColumnValue(TablePlan.KEY_ZIPCODE, zip_code);
            updateBuilder.updateColumnValue(TablePlan.KEY_SFL_CODE, sfl_code);
            updateBuilder.updateColumnValue(TablePlan.KEY_AREA, area);
            updateBuilder.updateColumnValue(TablePlan.KEY_STATUS, status);
            updateBuilder.updateColumnValue(TablePlan.KEY_VALUE, value);
            updateBuilder.updateColumnValue(TablePlan.KEY_TS_CODE, ts_code);
            updateBuilder.updateColumnValue(TablePlan.KEY_TS_NAME, ts_name);
            updateBuilder.updateColumnValue(TablePlan.KEY_TARGET, target);
            updateBuilder.updateColumnValue(TablePlan.KEY_RESULT, result);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePlan.class);
            DeleteBuilder<TablePlan, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(Context context) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePlan.class);
            DeleteBuilder<TablePlan, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /*public void deleteAll() {
        List<TablePlan> tblsatu = null;
        try {
            tblsatu = getHelper().getTablePlanDAO().queryForAll();
            getHelper().getTablePlanDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
