package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Eno on 11/1/2016.
 */

@DatabaseTable(tableName = "auto_sender")
public class TableDataMSDTDFailed {

    public static final String ID = "id";
    public static final String FORM_NO = "form_no";
    public static final String DATA = "data";
    public static final String APP_NAME = "app_name";
    public static final String DATE = "date";
    public static final String PLAN_ID = "plan_id";
    public static final String STATUS = "status";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField()
    private String form_no;
    @DatabaseField()
    private String data;
    @DatabaseField()
    private String app_name;
    @DatabaseField()
    private String date;
    @DatabaseField()
    private String status;
    @DatabaseField()
    private String plan_id;

    public String getPlanId() {
        return plan_id;
    }

    public void setPlanId(String planId) {
        this.plan_id = planId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForm_no() {
        return form_no;
    }

    public void setForm_no(String form_no) {
        this.form_no = form_no;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
