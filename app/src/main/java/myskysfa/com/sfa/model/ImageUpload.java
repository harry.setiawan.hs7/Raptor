package myskysfa.com.sfa.model;

public class ImageUpload {
    public boolean send;
    public String filePath;
    public String fileName;
    public String category;

    public ImageUpload(boolean send, String filePath, String fileName, String category) {
        this.send = send;
        this.filePath = filePath;
        this.fileName = fileName;
        this.category = category;
    }
}
