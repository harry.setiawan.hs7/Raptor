package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_hardware_status")
public class TableMHardwareStatus {

    public static final String TABLE_NAME = "m_hardware_status";
    public static final String fROWID = "hw_status_id";
    public static final String fHW_STATUS_NAME = "hw_status_name";
    public static final String fSTATUS = "status";

    @DatabaseField(id = true)
    private String hw_status_id;

    @DatabaseField
    private String hw_status_name, status;

    public void setHw_status_id(String hw_status_id) {
        this.hw_status_id = hw_status_id;
    }

    public String getHw_status_id() {
        return hw_status_id;
    }

    public void setHw_status_name(String hw_status_name) {
        this.hw_status_name = hw_status_name;
    }

    public String getHw_status_name() {
        return hw_status_name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}

