package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 9/28/2016.
 */
@DatabaseTable(tableName = "m_absen")
public class TableAbsensi {

    public static final String ROWID = "id";
    public static final String UID = "uid";
    public static final String IDSTORE = "id_store";
    public static final String ABSMASUK = "absen_masuk";
    public static final String ABSPULANG = "absen_pulang";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String DISTANCE = "distance";
    public static final String FLAG_SEND_MASUK = "flag_send_masuk";
    public static final String FLAG_SEND_KELUAR = "flag_send_keluar";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String uid, id_store, absen_masuk, absen_pulang, latitude, longitude, distance,
            flag_send_masuk,flag_send_keluar;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId_store() {
        return id_store;
    }

    public void setId_store(String id_store) {
        this.id_store = id_store;
    }

    public String getAbsen_masuk() {
        return absen_masuk;
    }

    public void setAbsen_masuk(String absen_masuk) {
        this.absen_masuk = absen_masuk;
    }

    public String getAbsen_pulang() {
        return absen_pulang;
    }

    public void setAbsen_pulang(String absen_pulang) {
        this.absen_pulang = absen_pulang;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFlag_send_masuk() {
        return flag_send_masuk;
    }

    public void setFlag_send_masuk(String flag_send_masuk) {
        this.flag_send_masuk = flag_send_masuk;
    }

    public String getFlag_send_keluar() {
        return flag_send_keluar;
    }

    public void setFlag_send_keluar(String flag_send_keluar) {
        this.flag_send_keluar = flag_send_keluar;
    }

}
