package myskysfa.com.sfa.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.utils.listItemObject.CheckList;
import myskysfa.com.sfa.utils.listItemObject.CheckListPickup;


/**
 * Created by Asus on 11/3/2016.
 */

public class DtdChecklistAdapterPickup extends RecyclerView.Adapter<DtdChecklistAdapterPickup.ViewHolder> {
    private List<CheckListPickup> list;
    private View itemLayoutView;
    //private Context mContext;


    public DtdChecklistAdapterPickup(List<CheckListPickup> list) {
        this.list = list;
        //this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dtd_checklist_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CheckListPickup itemChecklist;
        itemChecklist = list.get(position);
        String labelS = itemChecklist.getCHECKLIST_LABEL();
        String label_editS = itemChecklist.getCHECKLIST_LABEL_EDIT();
        final String statusS = itemChecklist.getCHECKLIST_STATUS();
        String IdS = itemChecklist.getID_CHECKLIST();
        holder.checklistCB.setText(labelS);
        //holder.checklistET.setText(label_editS);
        holder.checklistET.setHint(label_editS);
        //holder.checklistET.setText(label_editS);
        //if true, your checkbox will be selected, else unselected
        if (statusS.equals("1")){
            holder.checklistCB.setChecked(true);
        } else {
            holder.checklistCB.setChecked(false);
        }

        holder.checklistCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    holder.checklistCB.setChecked(true);
                    itemChecklist.setCHECKLIST_STATUS("1");
                }else {
                    holder.checklistET.setText("");
                    itemChecklist.setCHECKLIST_STATUS("0");
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextWatcher {
        private EditText checklistET;
        private CheckBox checklistCB;
        public ViewHolder(View itemView) {
            super(itemView);
            this.checklistET = (EditText) itemLayoutView.findViewById(R.id.checklistET);
            this.checklistCB = (CheckBox) itemLayoutView.findViewById(R.id.checklistCB);
            this.checklistET.addTextChangedListener(this);
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int position = getAdapterPosition();
            if (s.toString().trim().length()<1){
                checklistCB.setChecked(false);
            } else {
                list.get(position).setCHECKLIST_LABEL_EDIT(s.toString());
                checklistCB.setChecked(true);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


}

