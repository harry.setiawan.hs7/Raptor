package myskysfa.com.sfa.adapter.master;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableMasterPromo;

/**
 * Created by admin on 6/20/2016.
 */
public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.ViewHolder> {
    View itemLayoutView;
    ViewGroup mParent;
    private List<TableMasterPromo> itemsData;
    private Context context;

    public PromoAdapter(Context context, List<TableMasterPromo> itemsData) {
        this.context = context;
        this.itemsData = itemsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.promo_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterPromo itemPackage = itemsData.get(position);
        if (itemPackage != null) {
            holder.txtTitle.setText(itemPackage.getPromotion_id());
            holder.txtDate.setText(itemPackage.getEnd_date());
            holder.txtDesc.setText(itemPackage.getPromotion_desc());
            holder.txtIcon.setText(itemPackage.getPromotion_code());
        }

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public TextView txtDate;
        public TextView txtDesc;
        public TextView txtIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemLayoutView.findViewById(R.id.master_promo_item_title);
            txtDate = (TextView) itemLayoutView.findViewById(R.id.master_promo_item_date);
            txtDesc = (TextView) itemLayoutView.findViewById(R.id.master_promo_item_desc);
            txtIcon = (TextView) itemLayoutView.findViewById(R.id.master_promo_item_icon);

        }
    }
}