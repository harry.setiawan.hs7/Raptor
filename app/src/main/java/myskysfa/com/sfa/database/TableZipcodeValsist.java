package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/17/2016.
 */
@DatabaseTable(tableName = "M_ZIPCODE_VALSIST")
public class TableZipcodeValsist {

    public static final String TABLE_NAME = "kodepos_valsist";
    public static final String fROWID = "ID";
    public static final String fVAL_ID = "VAL_ID";
    public static final String fVILLAGE = "VILLAGE";
    public static final String fSMALL_CITY = "SMALL_CITY";
    public static final String fBIG_CITY = "BIG_CITY";
    public static final String fIS_ACTIVE = "IS_ACTIVE";
    public static final String fPROVINCE = "PROVINCE";
    public static final String fPOSTAL_CODE = "POSTAL_CODE";
    public static final String fCOUNTRY = "COUNTRY";

    @DatabaseField(generatedId = true)
    private int ID;

    @DatabaseField
    private String VAL_ID, VILLAGE, SMALL_CITY, BIG_CITY, IS_ACTIVE, PROVINCE, POSTAL_CODE, COUNTRY;

    public TableZipcodeValsist() {}

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getfROWID() {
        return fROWID;
    }

    public static String getfVAL_ID() {
        return fVAL_ID;
    }

    public static String getfVILLAGE() {
        return fVILLAGE;
    }

    public static String getfSMALL_CITY() {
        return fSMALL_CITY;
    }

    public static String getfBIG_CITY() {
        return fBIG_CITY;
    }

    public static String getfIS_ACTIVE() {
        return fIS_ACTIVE;
    }

    public static String getfPROVINCE() {
        return fPROVINCE;
    }

    public static String getfPOSTAL_CODE() {
        return fPOSTAL_CODE;
    }

    public static String getfCOUNTRY() {
        return fCOUNTRY;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getVAL_ID() {
        return VAL_ID;
    }

    public void setVAL_ID(String VAL_ID) {
        this.VAL_ID = VAL_ID;
    }

    public String getVILLAGE() {
        return VILLAGE;
    }

    public void setVILLAGE(String VILLAGE) {
        this.VILLAGE = VILLAGE;
    }

    public String getSMALL_CITY() {
        return SMALL_CITY;
    }

    public void setSMALL_CITY(String SMALL_CITY) {
        this.SMALL_CITY = SMALL_CITY;
    }

    public String getBIG_CITY() {
        return BIG_CITY;
    }

    public void setBIG_CITY(String BIG_CITY) {
        this.BIG_CITY = BIG_CITY;
    }

    public String getIS_ACTIVE() {
        return IS_ACTIVE;
    }

    public void setIS_ACTIVE(String IS_ACTIVE) {
        this.IS_ACTIVE = IS_ACTIVE;
    }

    public String getPROVINCE() {
        return PROVINCE;
    }

    public void setPROVINCE(String PROVINCE) {
        this.PROVINCE = PROVINCE;
    }

    public String getPOSTAL_CODE() {
        return POSTAL_CODE;
    }

    public void setPOSTAL_CODE(String POSTAL_CODE) {
        this.POSTAL_CODE = POSTAL_CODE;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }
}
