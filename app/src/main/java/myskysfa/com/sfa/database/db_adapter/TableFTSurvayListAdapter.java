package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFreeTrialSurvayList;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by admin on 6/14/2016.
 */
public class TableFTSurvayListAdapter {

    static private TableFTSurvayListAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFTSurvayListAdapter(ctx);
        }
    }
    static public TableFTSurvayListAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFTSurvayListAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFreeTrialSurvayList> getAllData() {
        List<TableFreeTrialSurvayList> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFTSurvayDAO().queryBuilder().orderBy(TableFreeTrialSurvayList.KEY_SV_ID
                    , false).query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableFreeTrialSurvayList> getDatabyCondition(String condition, Object param) {
        List<TableFreeTrialSurvayList> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialSurvayList.class);
            QueryBuilder<TableFreeTrialSurvayList, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialSurvayList, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableFreeTrialSurvayList.KEY_SV_ID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFreeTrialSurvayList tbl, String svId, String  prospect_nbr,
                           String sv_name, String sv_visit_date, String sv_status_desc,
                           String sv_status, String sv_value) {
        try {
            tbl.setSv_id(svId);
            tbl.setProspect_nbr(prospect_nbr);
            tbl.setSv_name(sv_name);
            tbl.setSv_visit_date(sv_visit_date);
            tbl.setSv_status_desc(sv_status_desc);
            tbl.setSv_status(sv_status);
            tbl.setSv_value(sv_value);
            getHelper().getTableFTSurvayDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */

    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialSurvayList.class);
            UpdateBuilder<TableFreeTrialSurvayList, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String  prospect_nbr, String sv_name,
                          String sv_visit_date, String sv_status_desc,
                          String sv_status, String sv_value,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialSurvayList.class);
            UpdateBuilder<TableFreeTrialSurvayList, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFreeTrialSurvayList.KEY_PRO_ID, prospect_nbr);
            updateBuilder.updateColumnValue(TableFreeTrialSurvayList.KEY_SV_NAME, sv_name);
            updateBuilder.updateColumnValue(TableFreeTrialSurvayList.KEY_VISIT_DATE, sv_visit_date);
            updateBuilder.updateColumnValue(TableFreeTrialSurvayList.KEY_STATUS_DESC, sv_status_desc);
            updateBuilder.updateColumnValue(TableFreeTrialSurvayList.KEY_STATUS, sv_status);
            updateBuilder.updateColumnValue(TableFreeTrialSurvayList.KEY_VALUE, sv_value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFreeTrialSurvayList.class);
            DeleteBuilder<TableFreeTrialSurvayList, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableFreeTrialSurvayList> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFTSurvayDAO().queryForAll();
            getHelper().getTableFTSurvayDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
