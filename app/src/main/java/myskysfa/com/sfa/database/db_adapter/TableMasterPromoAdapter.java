package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.util.List;

import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 6/20/2016.
 */
public class TableMasterPromoAdapter {
    static private TableMasterPromoAdapter instance;
    private DatabaseManager helper;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMasterPromoAdapter(ctx);
        }
    }

    static public TableMasterPromoAdapter getInstance() {
        return instance;
    }

    public TableMasterPromoAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }


    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMasterPromo> getAllData() {
        List<TableMasterPromo> tblData = null;
        try {
            tblData = getHelper().getTablePromoDAO()
                    .queryBuilder().orderBy(TableMasterPromo.FLD_DESC,
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblData;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMasterPromo tbl, String id, String  code, String desc,
                           String start_date, String end_date, String status,
                           String flag_promo) {
        try {
            tbl.setPromotion_id(id);
            tbl.setPromotion_code(code);
            tbl.setPromotion_desc(desc);
            tbl.setStart_date(start_date);
            tbl.setEnd_date(end_date);
            tbl.setStatus(status);
            tbl.setFlag_promo(flag_promo);
            getHelper().getTablePromoDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            Dao dao = helper.getDao(TableMasterPromo.class);
            DeleteBuilder<TableMasterPromo, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
