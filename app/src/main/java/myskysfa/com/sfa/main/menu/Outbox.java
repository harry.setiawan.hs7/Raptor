package myskysfa.com.sfa.main.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.outbox.Fragment_Absensi;
import myskysfa.com.sfa.main.menu.outbox.Fragment_Sales;

/**
 * Created by Eno on 6/28/2016.
 */
public class Outbox extends Fragment {
    private ViewGroup viewGroup;
    private static TabLayout tabLayout;
    private static ViewPager viewPager;
    public static int int_items = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_main_outbox, container, false);
        tabLayout = (TabLayout) viewGroup.findViewById(R.id.tabs);
        viewPager = (ViewPager) viewGroup.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2); //Start from 1
        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
        return viewGroup;
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Fragment_Sales();
                case 1:
                    return new Fragment_Absensi();
            }
            return null;
        }

        @Override
        public int getCount() {
            return int_items;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Prospect";
                case 1:
                    return "Absensi";
            }
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getActivity().getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
