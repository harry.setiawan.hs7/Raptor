package myskysfa.com.sfa.main.menu;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.ChecklistServiceAdapter;
import myskysfa.com.sfa.adapter.MSAlacarteListAdapter;
import myskysfa.com.sfa.adapter.SpinnerBrandAdapter;
import myskysfa.com.sfa.adapter.SpinnerHWAdapter;
import myskysfa.com.sfa.adapter.SpinnerMaterialAdapter;
import myskysfa.com.sfa.adapter.SpinnerPackAdapter;
import myskysfa.com.sfa.adapter.SpinnerPromoAdapter;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableFreeTrial;
import myskysfa.com.sfa.database.TableFreeTrialCoh;
import myskysfa.com.sfa.database.TableMBrand;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.TablePlan;
import myskysfa.com.sfa.database.TablePromotionFix;
import myskysfa.com.sfa.database.TablePromotionServices;
import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMBrandAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPackageAdapter;
import myskysfa.com.sfa.database.db_adapter.TableMasterPromoAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePromotionFixAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePromotionServiceAdapter;
import myskysfa.com.sfa.main.menu.dtd.DTDStatus;
import myskysfa.com.sfa.main.menu.ms.Ms_Fragment_Status;
import myskysfa.com.sfa.model.ObjEstimasi;
import myskysfa.com.sfa.model.ObjPromo;
import myskysfa.com.sfa.utils.MataUang;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by Eno on 11/4/2016.
 */

public class MSDTDPackage {
    private static Activity activity;
    private Utils utils;
    private PopupWindow mpopup;
    private RecyclerView recyclerView;
    private Button btnSave;
    private TableMasterPackageAdapter mTablePackageProductAdapter;
    private TableMasterPromoAdapter mPromoAdapter;
    private TablePromotionFixAdapter mPromoFixAdapter;
    private TablePromotionServiceAdapter mPromoServiceAdapter;
    private TableMBrandAdapter mBrandAdapter;
    private TableFTSOHAdapter mMaterialAdapter;
    private List<TableMasterPackage> mPackageProductListItem;

    private List<TableFormApp> listFormApp = new ArrayList<>();
    private List<TableFreeTrialCoh> listSOH = new ArrayList<>();
    private List<TableMBrand> mBrandListItem = new ArrayList<>();
    private List<TableMasterPromo> mPromoListItems = new ArrayList<>();
    private List<TablePromotionFix> mPromoFixListItems = new ArrayList<>();
    private List<TablePromotionServices> mPromoServiceListItems = new ArrayList<>();
    private List<TableFreeTrialCoh> mMaterialListItems = new ArrayList<>();
    private ArrayList<String> listProductPackage = new ArrayList<>();
    private ArrayList<ArrayList<String>> selAlacarteSaveAll = new ArrayList<>();
    private List<TablePlan> listPlan = new ArrayList<TablePlan>();
    private List<TableFreeTrial> listFreetrial = new ArrayList<TableFreeTrial>();
    private List<TableMasterPackage> mPackageProductListItemList;
    private ArrayList<String> listPromo, listPromoFix, listPromoService, listBrand, listVc, listDsd,
            listLnb, listDish, listMaterial;
    private ArrayList<String> selAlacarte = new ArrayList<String>();
    private SpinnerPromoAdapter spinPromoAdapter;
    private SpinnerBrandAdapter spinBrandAdapter;
    private SpinnerMaterialAdapter spinnerMaterialAdapter;
    private TableFTSOHAdapter dbCohAdapter;
    private MSAlacarteListAdapter mPackageProductAdapter;
    private ChecklistServiceAdapter adapterService;
    private List<TablePromotionServices> currentPromoServiceSelect = new ArrayList<>();
    private SpinnerHWAdapter spinAdapterVC, spinAdapterLNB, spinAdapterDSD, spinAdapterODU,
            spinAdapterType;
    private int totPakacge = 0, isMulty;
    private LinearLayout mLayoutPackage, mLayoutAdd, mLayoutAdditional, packageBundling, LinearBundling, layout_price_charge, layout_price_hardware, layout_price_bundling, layout_price_transport;
    private int target, index, tempTotalOffer;
    private static int hrgBasic, hrgBasic2, hrgAlacarte, totalOffer, firstBasic;
    private Spinner spinPackage, spinType, spinBrand, spinVc, spinLnb, spinDsd, spinDish,
            materialAdd, bundling, billFrek;
    private SearchableSpinner spinPromo;
    private RecyclerView promoService;

    private TextView pricePackage, priceInstalasi, pricePromo,promoAmountDescryption,promoDiscDescryption, priceAlacarte, txtPack, promoDisc, promoAmount, priceOffer, price_charge, price_hardware, price_bundling;
    public static TextView price_transport;
    private ViewGroup mView;
    private SpinnerPackAdapter spinAdapter;
    private String promoCode, promoPrice, formattednumber, strPaket, strAlacarte, paket, saveBrand, numberForm, appType,
            simcardId = "", routerId = "", priceMaterial = "", product_id = "", basic_id;

    private ArrayList<String> strPromoFix = new ArrayList<>();
    private Double duit;
    private NumberFormat defaultF;
    private TableFormAppAdapter mFormAppAdapter;
    private TablePlanAdapter tablePlanAdapter;
    private TableFreeTrialAdapter dbAdapter;
    //private ArrayAdapter<String> spinnerVc, spinnerDsd, spinnerLnb, spinnerDish;
    private ImageView additional, btnAdd, btnBundling, delete_additional;
    private int count, strBillFrek = 1, calBasicWithBill = 1, biayaInstall, harga_bundling;
    public static int harga_hardware;
    private Button save;
    private Boolean isShowBundling = false, isShowAdditonal = true;
    private EditText countAdd, simcardValue, routerValue;

    private List<ObjPromo> tmpPromoDiscount = new ArrayList<>();
    private List<ObjPromo> tmpPromoAmount = new ArrayList<>();
    private ArrayList<BigInteger> tmpHargaBasic = new ArrayList<>();
    private ArrayList<BigInteger> tmpHargaAlacarte = new ArrayList<>();
    private ArrayList<BigInteger> tmpHargaCharge = new ArrayList<>();
    private ArrayList<BigInteger> tmpHargaHardware = new ArrayList<>();
    private ArrayList<String> tmpHardware = new ArrayList<>();
    LinearLayout mLayoutBackProduct;
    ObjEstimasi tmpEstimation;
    boolean isAllHD = false;
    boolean isAllSD = false;

    String code = "1";

    public MSDTDPackage(Activity activity, ViewGroup mView, LinearLayout mLayoutAdd,
                        LinearLayout mLayoutPackage, Spinner spinType,
                        Spinner spinBrand, SearchableSpinner spinPromo, TextView priceInstalasi, TextView pricePackage,
                        TextView pricePromo, TextView promoDiscDescryption, TextView promoAmountDescryption, String numberForm, String appType, Spinner bundling,
                        TextView priceAlacarte, TextView promoDisc, TextView promoAmount,
                        RecyclerView promoService, TextView promoOffer, Spinner billFrek,
                        LinearLayout layout_price_hardware, LinearLayout layout_price_charge, LinearLayout layout_price_bundling , LinearLayout layout_price_transport,
                        TextView price_charge, TextView price_hardware, TextView price_bundling, TextView price_transport, Button save) {
        this.activity = activity;
        tmpEstimation = new ObjEstimasi();
        if (appType.equals("d")) {
            tmpEstimation.setTotal_transport(new BigInteger("0"));
        }else{
            try {
                tmpEstimation.setTotal_transport(MainFragmentMS.total_transport);
            }catch (Exception e){

            }
        }
        this.mView = mView;
        this.mLayoutPackage = mLayoutPackage;
        this.mLayoutAdd = mLayoutAdd;
        this.spinType = spinType;
        this.spinBrand = spinBrand;
        this.spinPromo = spinPromo;
        this.priceInstalasi = priceInstalasi;
        this.pricePackage = pricePackage;
        this.pricePromo = pricePromo;
        this.numberForm = numberForm;
        this.appType = appType;
        this.save = save;
        this.bundling = bundling;
        this.priceAlacarte = priceAlacarte;
        this.promoDisc = promoDisc;
        this.promoDiscDescryption = promoDiscDescryption;
        this.promoAmountDescryption = promoAmountDescryption;
        this.promoAmount = promoAmount;
        this.promoService = promoService;
        this.priceOffer = promoOffer;
        this.billFrek = billFrek;
        this.layout_price_charge = layout_price_charge;
        this.layout_price_hardware = layout_price_hardware;
        this.layout_price_bundling = layout_price_bundling;
        this.layout_price_transport = layout_price_transport;
        this.price_charge = price_charge;
        this.price_hardware = price_hardware;
        this.price_bundling = price_bundling;
        this.price_transport = price_transport;
    }

    public void InitView() {

        // generate basic list
        utils = new Utils(activity);
        dbCohAdapter = new TableFTSOHAdapter(activity);
        mFormAppAdapter = new TableFormAppAdapter(activity);
        listProductPackage = new ArrayList<String>();
        listPromo = new ArrayList<String>();
        listPromoFix = new ArrayList<String>();
        listPromoService = new ArrayList<String>();
        listBrand = new ArrayList<String>();
        listVc = new ArrayList<String>();
        listDsd = new ArrayList<String>();
        listLnb = new ArrayList<String>();
        listDish = new ArrayList<String>();
        listSOH = new ArrayList<TableFreeTrialCoh>();
        listMaterial = new ArrayList<>();
        listPromoFix = new ArrayList<>();
        listPromoService = new ArrayList<>();

        mTablePackageProductAdapter = new TableMasterPackageAdapter(activity);
        mPromoAdapter = new TableMasterPromoAdapter(activity);
        mPromoFixAdapter = new TablePromotionFixAdapter(activity);
        mPromoServiceAdapter = new TablePromotionServiceAdapter(activity);
        mBrandAdapter = new TableMBrandAdapter(activity);
        mMaterialAdapter = new TableFTSOHAdapter(activity);

        listFormApp = mFormAppAdapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
        mBrandListItem = mBrandAdapter.getAllData();
        mPromoListItems = mPromoAdapter.getAllData();
        mPromoFixListItems = mPromoFixAdapter.getDatabyCondition(TablePromotionFix.ID_PROMO, promoCode);

        mMaterialListItems = mMaterialAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_IS_SERIALIZE, "N");
        listSOH = dbCohAdapter.getDatabyCondition(TableFreeTrialCoh.KEY_HW_STATUS, "ADD");

        for (int i = 0; i < mPromoListItems.size(); i++) {
            TableMasterPromo item = mPromoListItems.get(i);
            listPromo.add(item.getPromotion_code() + " - " + item.getPromotion_desc());
        }

        for (int i = 0; i < mBrandListItem.size(); i++) {
            TableMBrand item = mBrandListItem.get(i);
            listBrand.add(item.getBrand_name());
        }

        for (int i = 0; i < listFormApp.size(); i++) {
            TableFormApp item = listFormApp.get(i);
            if (item != null) {
                paket = item.getVALUES_PACKAGE();
            }
        }

        for (int i = 0; i < mMaterialListItems.size(); i++) {
            TableFreeTrialCoh item = mMaterialListItems.get(i);
            listMaterial.add(item.getName());
        }


        /**
         * Generate Process Package
         */
        listVc.add("");
        listDsd.add("");
        listLnb.add("");
        listDish.add("");
        for (int i = 0; i < listSOH.size(); i++) {
            TableFreeTrialCoh item = listSOH.get(i);
            if (item.getType().equalsIgnoreCase("VC")) {
                listVc.add(item.getSn());
            }
            if (item.getType().equalsIgnoreCase("DEC")) {
                listDsd.add(item.getSn());
            }
            if (item.getType().equalsIgnoreCase("LNB")) {
                listLnb.add(item.getSn());
            }
            if (item.getType().equalsIgnoreCase("ANT")) {
                listDish.add(item.getSn());
            }
        }
        // End Process

        //spinnerPromo = new ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, listPromo);
        //spinnerBrand = new ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, listBrand);

        //spinPromoAdapter = new SpinnerPromoAdapter(activity, R.layout.status_spinner, mPromoListItems);
        //spinPromoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinBrandAdapter = new SpinnerBrandAdapter(activity, R.layout.status_spinner, mBrandListItem);
        spinBrandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinPromoAdapter = new SpinnerPromoAdapter(activity, R.layout.status_spinner, listPromo);
        spinPromoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerMaterialAdapter = new SpinnerMaterialAdapter(activity, R.layout.status_spinner, mMaterialListItems);
        spinnerMaterialAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayList<String> typeList = new ArrayList<String>();
        typeList.add("Single");
        typeList.add("Multi");

        spinAdapterType = new SpinnerHWAdapter(activity, R.layout.status_spinner, typeList);
        spinAdapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //spinPromo = (Spinner) mView.findViewById(R.id.spinner_promo);
        //spinPromo.setAdapter(spinPromoAdapter);
        //spinPromo.setOnItemSelectedListener(spinPromoListener);

        billFrek.setOnItemSelectedListener(spinBillFrek);

        spinType = (Spinner) mView.findViewById(R.id.spinner_type);
        spinType.setAdapter(spinAdapterType);
        spinType.setSelection(0);
        spinType.setOnItemSelectedListener(spinerTypeListener);

        spinBrand = (Spinner) mView.findViewById(R.id.spinner_brand);
        spinBrand.setAdapter(spinBrandAdapter);
        spinBrand.setSelection(2);
        spinBrand.setOnItemSelectedListener(spinBrandListener);

        spinPromo = (SearchableSpinner) mView.findViewById(R.id.spinner_promo);
        spinPromo.setAdapter(spinPromoAdapter);
        spinPromo.setSelection(0);
        spinPromo.setTitle("Select Item");
        spinPromo.setOnItemSelectedListener(spinerPromoListener);


        additional = (ImageView) mView.findViewById(R.id.btn_add_additional);
        delete_additional = (ImageView) mView.findViewById(R.id.btn_min_additional);
        btnAdd = (ImageView) mView.findViewById(R.id.btn_add_package);

        additional.setOnClickListener(addAdditionalItems);
        delete_additional.setOnClickListener(deleteAdditional);
        btnAdd.setOnClickListener(actAddPackage);


        packageBundling = (LinearLayout) mView.findViewById(R.id.bundling);
        btnBundling = (ImageView) mView.findViewById(R.id.btn_add_bundling);
        LinearBundling = (LinearLayout) mView.findViewById(R.id.itemBundling);
        simcardValue = (EditText) mView.findViewById(R.id.sValue);
        routerValue = (EditText) mView.findViewById(R.id.rValue);
        bundling.setOnItemSelectedListener(bundlingListener);
        btnBundling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShowBundling) {
                    packageBundling.setVisibility(View.VISIBLE);
                    isShowBundling = true;
                    //LinearBundling.setVisibility(View.VISIBLE);
                    btnBundling.setImageResource(R.drawable.ic_cancel_bundling);
                    simcardId = "882";
                    routerId = "881";
                    //show bundle price
                    layout_price_bundling.setVisibility(View.VISIBLE);
                    Log.d("isShowBundlingAfter", isShowBundling+"");
                } else {
                    packageBundling.setVisibility(View.GONE);
                    isShowBundling = false;
                    //LinearBundling.setVisibility(View.GONE);
                    btnBundling.setImageResource(R.drawable.ic_plus);
                    simcardId = "";
                    routerId = "";
                    simcardValue.setText("");
                    routerValue.setText("");
                    //show bundle price
                    layout_price_bundling.setVisibility(View.GONE);
                    Log.d("isShowBundlingAfter", isShowBundling+"");
                }
                setEstimasiBiaya();
            }
        });

        if (paket != null) {
            try {
                JSONObject paketObj = new JSONObject(paket);
                String vc = paketObj.getString("VC");
                String odu = paketObj.getString("ODU");
                String lnb = paketObj.getString("LNB");
                String dsd = paketObj.getString("DSD");
                if (vc != null)
                    setSelectHw("VC", vc);
                if (odu != null)
                    setSelectHw("ANT", odu);
                if (lnb != null)
                    setSelectHw("LNB", lnb);
                if (dsd != null)
                    setSelectHw("DEC", dsd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        save.setOnClickListener(actSaveAll);
    }

    private AdapterView.OnItemSelectedListener bundlingListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (bundling.getSelectedItem().toString().equalsIgnoreCase("xl") ||
                    position == 0) {
                simcardId = "882";
                routerId = "881";
            } else if (bundling.getSelectedItem().toString().equalsIgnoreCase("indosat") ||
                    position == 1) {
                simcardId = "888";
                routerId = "887";
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private void setSelectHw(String type, String val) {
        if (type.equalsIgnoreCase("VC")) {
            for (int i = 0; i < listVc.size(); i++) {
                if (listVc.get(i).equalsIgnoreCase(val)) {
                    spinVc.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("DEC")) {
            for (int i = 0; i < listDsd.size(); i++) {
                if (listDsd.get(i).equalsIgnoreCase(val)) {
                    spinDsd.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("ANT")) {
            for (int i = 0; i < listDish.size(); i++) {
                if (listDish.get(i).equalsIgnoreCase(val)) {
                    spinDish.setSelection(i);
                }
            }
        }

        if (type.equalsIgnoreCase("LNB")) {
            for (int i = 0; i < listLnb.size(); i++) {
                if (listLnb.get(i).equalsIgnoreCase(val)) {
                    spinLnb.setSelection(i);
                }
            }
        }
    }

    /**
     * Spin Listener
     */


    AdapterView.OnItemSelectedListener spinBillFrek = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                strBillFrek = 1;
                tmpEstimation.setBF(new BigInteger("1"));
            } else if (position == 1) {
                strBillFrek = 2;
                tmpEstimation.setBF(new BigInteger("3"));
            } else if (position == 2) {
                strBillFrek = 3;
                tmpEstimation.setBF(new BigInteger("6"));
            } else if (position == 3) {
                strBillFrek = 4;
                tmpEstimation.setBF(new BigInteger("12"));

            }
            setEstimasiBiaya();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    AdapterView.OnItemSelectedListener spinerTypeListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0) {
                for (int i = totPakacge; i >= 1; i--) {
                    delPackage(i);
                }
                isMulty = 0;
                addPackage();
                mLayoutAdd.setVisibility(View.GONE);
            } else {
                if (appType.equals("d")) {
                    //Transport
                    getTarget();
                    isMulty = 1; // ? kenapa langsung d set sebelum di cek
                    if (target >= 2) {
                        addPackage();
                        mLayoutAdd.setVisibility(View.VISIBLE);
                    } else {
                        spinType.setSelection(0);
                        Toast.makeText(activity, "Sisa plan anda kurang dari 2", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    isMulty = 1;
                    addPackage();
                    mLayoutAdd.setVisibility(View.VISIBLE);
                }
                //addPackage();
                setEstimasiBiaya();


            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };


    private AdapterView.OnItemSelectedListener spinBrandListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            code = mBrandListItem.get(position).getBrand_code();
            TableMasterPackageAdapter tableMPackageProductAdapter
                    = new TableMasterPackageAdapter(activity);
            List<TableMasterPackage> paketList = new ArrayList<TableMasterPackage>();
            paketList = tableMPackageProductAdapter.fetchByProfile("2", code, product_id);
            if (paketList != null) {
                for (int m = 0; m < paketList.size(); m++) {
                    TableMasterPackage item = paketList.get(m);
                    if (item != null) {
                        item.getPrice();
                        Double install = Double.parseDouble(item.getPrice());
                        NumberFormat defaultF = NumberFormat.getInstance();
                        String formattednumber = defaultF.format(install);
                        String str = formattednumber.replace(",", ".");
                        //instalasi.setText("Rp." + strPaket);
                        //
                    }
                }
            }

            mLayoutBackProduct = (LinearLayout) mView.findViewById(R.id.back_product);

            mPackageProductListItemList = mTablePackageProductAdapter.fetchBasic(code);
            for (int i = 0; i < mPackageProductListItemList.size(); i++) {
                TableMasterPackage item = mPackageProductListItemList.get(i);
                listProductPackage.add(item.getProduct_name());
            }
            //spinnerProductPackage = new ArrayAdapter<String>(activity, R.layout.support_simple_spinner_dropdown_item, listProductPackage);
            spinAdapter = new SpinnerPackAdapter(activity, R.layout.status_spinner, mPackageProductListItemList);
            spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            /*for (int i=0; i<listSOH.size();i++) {
                if(listSOH.get(i).getType().equalsIgnoreCase("VC")) {
                    listVc.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("LNB")) {
                    listLnb.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("ANT")) {
                    listDish.add(listSOH.get(i).getSn());
                } else if (listSOH.get(i).getType().equalsIgnoreCase("DEC")) {
                    listDsd.add(listSOH.get(i).getSn());
                }
            }*/

            spinAdapterVC = new SpinnerHWAdapter(activity, R.layout.status_spinner, listVc);
            spinAdapterVC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterLNB = new SpinnerHWAdapter(activity, R.layout.status_spinner, listLnb);
            spinAdapterLNB.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterODU = new SpinnerHWAdapter(activity, R.layout.status_spinner, listDish);
            spinAdapterODU.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinAdapterDSD = new SpinnerHWAdapter(activity, R.layout.status_spinner, listDsd);
            spinAdapterDSD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            try {
                mLayoutPackage.removeAllViews();
                selAlacarteSaveAll.clear();
                totPakacge = 0;
                addPackage();
            } catch (NullPointerException e) {

            }
            setEstimasiBiaya();

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };


    AdapterView.OnItemSelectedListener spinerPromoListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            strPromoFix.clear();
            tmpPromoDiscount.clear();
            tmpPromoAmount.clear();

            promoCode = mPromoListItems.get(position).getPromotion_code();

            //Handle Promo Fix
            mPromoFixListItems = mPromoFixAdapter.getDatabyCondition(TablePromotionFix.ID_PROMO, promoCode);

            for(int i = 0 ; i < mPromoFixListItems.size() ; i++){
                strPromoFix.add(i, String.valueOf(mPromoFixListItems.get(i).getId_fix()));
                Log.d("strPro", strPromoFix.get(i));
            }

            int amount = 0;
            int disc = 0;
            promoAmount.setText("0");
            promoDisc.setText("0");
            for (int i = 0; i < mPromoFixListItems.size(); i++) {
                TablePromotionFix item = mPromoFixListItems.get(i);
                if (!item.getAmount().equals("0")) {
                    tmpPromoAmount.add(new ObjPromo(Integer.parseInt(item.getAmount()), item.getPromo_target()));
                }
            }

            for (int i = 0; i < mPromoFixListItems.size(); i++) {
                TablePromotionFix item = mPromoFixListItems.get(i);
                if (!item.getDisc().equals("0")) {
                    tmpPromoDiscount.add(new ObjPromo(Integer.parseInt(item.getDisc()), item.getPromo_target()));
                }
            }

            setShownToDisplayPromo();

            mPromoServiceListItems = mPromoServiceAdapter.getDatabyCondition(TablePromotionServices.ID_FIX, promoCode);
            LinearLayoutManager layoutParams = new LinearLayoutManager(activity);
            promoService.setLayoutManager(layoutParams);
            adapterService = new ChecklistServiceAdapter(mPromoServiceListItems, new ChecklistServiceAdapter.OnItemCheckListener() {
                @Override
                public void onItemCheck(TablePromotionServices item) {
                    currentPromoServiceSelect.add(item);
                    Log.d("serpice", item.getId_service());
                }

                @Override
                public void onItemUncheck(TablePromotionServices item) {
                    currentPromoServiceSelect.remove(item);
                    Log.d("serpice", currentPromoServiceSelect.toString());
                }
            });
            promoService.setAdapter(adapterService);

            setEstimasiBiaya();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    private void setShownToDisplayPromo() {
        promoAmount.setText("");
        promoAmountDescryption.setText("");
        promoDisc.setText("");
        promoDiscDescryption.setText("");
        for(int i = 0 ; i < tmpPromoAmount.size() ; i++){
            String strDescription = "";
            Log.i("DATAZ", String.valueOf(tmpPromoAmount.get(i).getJumlah()) + " - " + tmpPromoAmount.get(i).getDescryption());
            promoAmount.setText(promoAmount.getText() + "Rp. " + MataUang.set_digit_rupiah(String.valueOf(tmpPromoAmount.get(i).getJumlah())));

            if(tmpPromoAmount.get(i).getDescryption().equals("B")){
                strDescription = ("[ Basic ]");
            }
            if(tmpPromoAmount.get(i).getDescryption().equals("A")){
                strDescription = ("[ Add On ]");
            }
            if(tmpPromoAmount.get(i).getDescryption().equals("BA")){
                strDescription =("[ Basic & Add On ]");
            }
            if(tmpPromoAmount.get(i).getDescryption().equals("INS")){
                strDescription = ("[ Instalation ]");
            }
            if(tmpPromoAmount.get(i).getDescryption().equals("INT")){
                strDescription = ("[ Internet ]");
            }
            promoAmountDescryption.setText(promoAmountDescryption.getText() + String.valueOf(strDescription));

            if(i < tmpPromoAmount.size()){
                promoAmount.setText(promoAmount.getText() + "\n");
                promoAmountDescryption.setText(promoAmountDescryption.getText() + "\n");
            }
        }
        for(int i = 0 ; i < tmpPromoDiscount.size() ; i++){
            String strDescription = "";
            Log.i("DATAZ", String.valueOf(tmpPromoDiscount.get(i).getJumlah()) + " - " + tmpPromoDiscount.get(i).getDescryption());
            promoDisc.setText(promoDisc.getText() + String.valueOf(tmpPromoDiscount.get(i).getJumlah()) + "%");

            if(tmpPromoDiscount.get(i).getDescryption().equals("B")){
                strDescription = ("[ Basic ]");
            }
            if(tmpPromoDiscount.get(i).getDescryption().equals("A")){
                strDescription = ("[ Add On ]");
            }
            if(tmpPromoDiscount.get(i).getDescryption().equals("BA")){
                strDescription = ("[ Basic & Add On ]");
            }
            if(tmpPromoDiscount.get(i).getDescryption().equals("INT")){
                strDescription = ("[ Internet ]");
            }
            if(tmpPromoDiscount.get(i).getDescryption().equals("INS")){
                strDescription = ("[ Instalation ]");
            }
            if(tmpPromoDiscount.get(i).getDescryption().equals("HD")){
                strDescription = ("[ HW Charge ]");
            }
            promoDiscDescryption.setText(promoDiscDescryption.getText() + String.valueOf(strDescription));

            if(i < tmpPromoDiscount.size()){
                promoDisc.setText(promoDisc.getText() + "\n");
                promoDiscDescryption.setText(promoDiscDescryption.getText() + "\n");
            }
        }

        if(tmpPromoDiscount.size() == 0){
            promoDisc.setText("0%");
            promoDiscDescryption.setText("-");
        }

        if(tmpPromoAmount.size() == 0){
            promoAmount.setText("Rp. 0");
            promoAmountDescryption.setText("-");
        }
    }

    private void delPackage(int tag) {
        try {
            if (mLayoutPackage.findViewWithTag("PACK_" + tag) != null) {
                mLayoutPackage.removeView(mLayoutPackage.findViewWithTag("PACK_" + tag));
                totPakacge = totPakacge - 1;

                hrgBasic = 0;
                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    String product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                    TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                    List<TableMasterPackage> packageProductListItems
                            = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID, product_id);

                    for (int n = 0; n < packageProductListItems.size(); n++) {
                        TableMasterPackage item = packageProductListItems.get(n);
                        hrgBasic = hrgBasic + Integer.parseInt(item.getPrice());
                    }
                }

                Log.d("FTPaket", "Coba= " + hrgBasic);

                for (int j = 0; j < selAlacarteSaveAll.size(); j++) {
                    if (j == (tag - 1)) {
                        selAlacarteSaveAll.remove(j);
                    }
                }

                tmpHargaAlacarte.clear();
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {
                    hrgAlacarte = 0;
                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                            }
                        }
                        if (x == 1) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + (Integer.parseInt(item.getPrice()) / 2);
                            }
                        }
                        if (x == 2) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + (Integer.parseInt(item.getPrice()) / 2);
                            }
                        }
                    }
                    tmpHargaAlacarte.add(x, new BigInteger(String.valueOf(hrgAlacarte)));
                }

                setEstimasiBiaya();

                LinearLayout layoutTitle = (LinearLayout) mLayoutPackage.findViewWithTag("TITLE_" + totPakacge);
                if (layoutTitle.findViewWithTag(totPakacge) == null) {
                    LinearLayout.LayoutParams pDelPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                    pDelPackage.gravity = Gravity.END;
                    pDelPackage.setMargins(0, 0, 5, 0);
                    if (totPakacge > 2) {
                        ImageView delPack = new ImageView(activity);
                        delPack.setImageResource(R.drawable.ic_close_package);
                        delPack.setTag(totPakacge);
                        delPack.setLayoutParams(pDelPackage);
                        delPack.setOnClickListener(deletePackage);
                        layoutTitle.addView(delPack);
                    }
                }
            }

        } catch (Exception e) {
        }
    }

    public void setEstimasiBiaya() {
            setDefaultEstimation();
            Log.d("INSTALL ESTIMATION", "---------------------");

        try {
            try {
                mLayoutBackProduct = (LinearLayout) mView.findViewById(R.id.back_product);
                switch (code) {
                    case "1":
                        mLayoutBackProduct.setBackgroundColor(Color.parseColor("#01579b"));
                        tmpEstimation.setHarga_installation(new BigInteger("200000"));
                        break;
                    case "2":
                        mLayoutBackProduct.setBackgroundColor(Color.parseColor("#43a047"));
                        tmpEstimation.setHarga_installation(new BigInteger("170000"));
                        break;
                    case "3":
                        mLayoutBackProduct.setBackgroundColor(Color.parseColor("#ffd600"));
                        tmpEstimation.setHarga_installation(new BigInteger("150000"));
                        break;
                    default:
                        mLayoutBackProduct.setBackgroundColor(Color.parseColor("#01579b"));
                        tmpEstimation.setHarga_installation(new BigInteger("200000"));
                        break;
                }
            } catch (Exception y) {
            }

            if (tmpHargaAlacarte.size() == 0) {
                tmpHargaAlacarte.add(new BigInteger("0"));
            }

            for (int i = 0; i < tmpHargaBasic.size(); i++) {
                Log.d("BASIC", tmpHargaBasic.get(i).toString());
            }

            tmpEstimation.setHarga_basic(tmpHargaBasic);
            tmpEstimation.setHarga_alacarte(tmpHargaAlacarte);
            tmpEstimation.setHarga_hardware(tmpHargaHardware);
            tmpEstimation.setHarga_charge(tmpHargaCharge);

            //SET BASIC
            for (int i = 0; i < tmpEstimation.getHarga_basic().size(); i++) {
                tmpEstimation.setTotal_harga_basic(tmpEstimation.getTotal_harga_basic().add((tmpEstimation.getHarga_basic().get(i).multiply(tmpEstimation.getBF()))));
            }

            //SET ALACARTE
            for (int i = 0; i < tmpEstimation.getHarga_alacarte().size(); i++) {
                tmpEstimation.setTotal_harga_alacarte(tmpEstimation.getTotal_harga_alacarte().add((tmpEstimation.getHarga_alacarte().get(i).multiply(tmpEstimation.getBF()))));
            }

            //SET BUNDLING
            if (isShowBundling) {
                tmpEstimation.setHarga_bundling(new BigInteger("220000"));
            } else {
                tmpEstimation.setHarga_bundling(new BigInteger("0"));
            }
            tmpEstimation.setTotal_harga_bundling(tmpEstimation.getHarga_bundling().multiply(tmpEstimation.getBF()));

            //SET HARDWARE OR CHARGE

            boolean flag_charge = false;

            if (appType.equals("d")) {
                layout_price_transport.setVisibility(View.VISIBLE);
                if (DTDStatus.HW_STATUS_TAG.equals("3")) {
                    //SET HARDWARE
                    flag_charge = true;
                    layout_price_charge.setVisibility(View.GONE);
                    layout_price_hardware.setVisibility(View.VISIBLE);
                } else {
                    //SET CHARGE
                    layout_price_hardware.setVisibility(View.GONE);
                    layout_price_charge.setVisibility(View.VISIBLE);
                    flag_charge = false;
                }
            } else {
                layout_price_transport.setVisibility(View.VISIBLE);
                if (Ms_Fragment_Status.HW_STATUS_TAG.equals("3")) {
                    //SET HARDWARE
                    flag_charge = true;
                    layout_price_charge.setVisibility(View.GONE);
                    layout_price_hardware.setVisibility(View.VISIBLE);
                } else {
                    //SET CHARGE
                    layout_price_hardware.setVisibility(View.GONE);
                    layout_price_charge.setVisibility(View.VISIBLE);
                    flag_charge = false;
                }
            }

            ArrayList<BigInteger> tmpHargaCharge_new = new ArrayList<>();
            for (int i = 0; i < tmpEstimation.getHarga_charge().size(); i++) {
                if(i == 0) {
                    tmpHargaCharge_new.add(i, tmpEstimation.getHarga_charge().get(i).multiply(tmpEstimation.getBF()));
                }else{
                    tmpHargaCharge_new.add(i, tmpEstimation.getHarga_charge().get(i).multiply(tmpEstimation.getBF()).divide(new BigInteger("2")));
                }
                Log.d("ESTIMASI", "Charge = " + tmpHargaCharge_new.get(i));
            }

            if(tmpPromoDiscount.size() != 0) {
                for (int y = 0; y < tmpPromoDiscount.size(); y++) {
                    if (tmpPromoDiscount.get(y).getDescryption().toUpperCase().equals("HD")) {
                        if (flag_charge) {
                            tmpEstimation.setDiscount_harga_charge((tmpHargaCharge_new.get(0).multiply(new BigInteger(String.valueOf(tmpPromoDiscount.get(y).getJumlah()))).divide(new BigInteger("100"))));
                            Log.d("ESTIMATION", "Total diskon HW Charge : " + tmpEstimation.getDiscount_harga_charge());
                        }
                    }
                }
            }

            tmpEstimation.setHarga_charge(tmpHargaCharge_new);

            for (int i = 0; i < tmpEstimation.getHarga_charge().size(); i++) {
                tmpEstimation.setTotal_harga_charge(tmpEstimation.getTotal_harga_charge().add((tmpEstimation.getHarga_charge().get(i))));
            }

            for (int i = 0; i < tmpEstimation.getHarga_hardware().size(); i++) {
                tmpEstimation.setTotal_harga_hardware(tmpEstimation.getTotal_harga_hardware().add((tmpEstimation.getHarga_hardware().get(i))));
            }

            Log.d("DTZ", "-----");
            Log.d("DTZ", isMulty + "");
            if (isMulty == 1) {
                for (int i = 0; i < tmpHardware.size(); i++) {
                    if (tmpHardware.get(i).equals("2")) {
                        isAllHD = true;
                    } else {
                        isAllHD = false;
                        break;
                    }
                    Log.d("DTZ", tmpHardware.get(i) + "");
                }

                for (int i = 0; i < tmpHardware.size(); i++) {
                    if (tmpHardware.get(i).equals("1")) {
                        isAllSD = true;
                    } else {
                        isAllSD = false;
                        break;
                    }
                    Log.d("DTZ", tmpHardware.get(i) + "");
                }

                if (isAllHD) {
                    for (int i = 0; i < tmpHardware.size(); i++) {
                        tmpHardware.set(i, "309");
                    }
                }
                if (isAllSD) {
                    for (int i = 0; i < tmpHardware.size(); i++) {
                        tmpHardware.set(i, "308");
                    }
                }
            }

            //SET DISCOUNT
            if(tmpPromoDiscount.size() != 0) {
                for (int y = 0; y < tmpPromoDiscount.size(); y++) {
                    if (tmpPromoDiscount.get(y).getDescryption().toUpperCase().equals("B")) {
                        tmpEstimation.setTotal_discount_basic(tmpEstimation.getHarga_basic().get(0).multiply(tmpEstimation.getBF()).multiply(new BigInteger(String.valueOf(tmpPromoDiscount.get(y).getJumlah()))).divide(new BigInteger("100")));
                        Log.d("ESTIMATION", "Total diskon basic : " + tmpEstimation.getTotal_discount_basic() + "");
                    }
                    if (tmpPromoDiscount.get(y).getDescryption().toUpperCase().equals("A")) {
                        tmpEstimation.setTotal_discount_alacarte(tmpEstimation.getHarga_alacarte().get(0).multiply(tmpEstimation.getBF()).multiply(new BigInteger(String.valueOf(tmpPromoDiscount.get(y).getJumlah()))).divide(new BigInteger("100")));
                        Log.d("ESTIMATION", "Total diskon alacarte : " + tmpEstimation.getTotal_discount_alacarte() + "");
                    }
                    if (tmpPromoDiscount.get(y).getDescryption().toUpperCase().equals("BA")) {
                        tmpEstimation.setTotal_discount_basic_alacarte((tmpEstimation.getHarga_basic().get(0).add(tmpEstimation.getHarga_alacarte().get(0))).multiply(tmpEstimation.getBF()).multiply(new BigInteger(String.valueOf(tmpPromoDiscount.get(y).getJumlah()))).divide(new BigInteger("100")));
                        Log.d("ESTIMATION", "Total diskon basic alacarte : " + tmpEstimation.getTotal_discount_basic_alacarte() + "");
                    }

                    if (tmpPromoDiscount.get(y).getDescryption().toUpperCase().equals("INT")) {
                        tmpEstimation.setTotal_discount__bundling(tmpEstimation.getTotal_harga_bundling().multiply(new BigInteger(String.valueOf(tmpPromoDiscount.get(y).getJumlah()))).divide(new BigInteger("100")));
                        Log.d("ESTIMATION", "Total diskon internet : " + tmpEstimation.getTotal_discount__bundling() + "");
                    }
                    if (tmpPromoDiscount.get(y).getDescryption().toUpperCase().equals("INS")) {
                        tmpEstimation.setTotal_discount_installation(tmpEstimation.getHarga_installation().multiply(new BigInteger(String.valueOf(tmpPromoDiscount.get(y).getJumlah()))).divide(new BigInteger("100")));
                        Log.d("ESTIMATION", "Total diskon install : " + tmpEstimation.getTotal_discount_installation() + "");
                    }
                }
            }

            if (tmpPromoAmount.size() != 0) {
                for (int y = 0; y < tmpPromoAmount.size(); y++) {
                    tmpEstimation.setTotal_offer(tmpEstimation.getTotal_offer().add(new BigInteger(String.valueOf(tmpPromoAmount.get(y).getJumlah()))));
                }
            }

            tmpEstimation.setTotal_offer(tmpEstimation.getTotal_offer().add(tmpEstimation.getTotal_discount_installation()).add(tmpEstimation.getTotal_discount_basic_alacarte()).add(tmpEstimation.getTotal_discount_basic()).add(tmpEstimation.getTotal_discount_alacarte()).add(tmpEstimation.getTotal_discount__bundling()).add(tmpEstimation.getDiscount_harga_charge()));


            if (appType.equals("d")) {
                tmpEstimation.setTotal_transport(new BigInteger("0"));
                tmpEstimation.setTotal_bayar((tmpEstimation.getHarga_installation()
                        .add(tmpEstimation.getTotal_harga_basic())
                        .add(tmpEstimation.getTotal_harga_alacarte())
                        .add(tmpEstimation.getTotal_harga_bundling())
                        .add(tmpEstimation.getTotal_harga_hardware())
                        .add(tmpEstimation.getHarga_additional())
                        .add(tmpEstimation.getTotal_harga_charge()))
                        .subtract(tmpEstimation.getTotal_offer()));
            } else {
                tmpEstimation.setTotal_transport(MainFragmentMS.total_transport);
                tmpEstimation.setTotal_bayar((tmpEstimation.getHarga_installation()
                        .add(tmpEstimation.getTotal_harga_basic())
                        .add(tmpEstimation.getTotal_harga_alacarte())
                        .add(tmpEstimation.getTotal_harga_bundling())
                        .add(tmpEstimation.getTotal_harga_hardware())
                        .add(tmpEstimation.getHarga_additional())
                        .add(tmpEstimation.getTotal_harga_charge()))
                        .add(tmpEstimation.getTotal_transport())
                        .subtract(tmpEstimation.getTotal_offer()));
            }

            Log.d("ESTIMATION", "Total Install : " + tmpEstimation.getHarga_installation() + "");
            Log.d("ESTIMATION", "Total Basic : " + tmpEstimation.getTotal_harga_basic() + "");
            Log.d("ESTIMATION", "Total Alacarte : " + tmpEstimation.getTotal_harga_alacarte() + "");
            Log.d("ESTIMATION", "Total Bundling : " + tmpEstimation.getTotal_harga_bundling() + "");
            Log.d("ESTIMATION", "Total Charge : " + tmpEstimation.getTotal_harga_charge() + "");
            Log.d("ESTIMATION", "Total Hardware : " + tmpEstimation.getTotal_harga_hardware() + "");
            Log.d("ESTIMATION", "Total Offer : " + tmpEstimation.getTotal_offer() + "");
            Log.d("ESTIMATION", "Total Bayar : " + tmpEstimation.getTotal_bayar() + "");
        }catch (Exception e){

        }
        price_transport.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_transport())));
        priceInstalasi.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getHarga_installation())));
        pricePackage.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_harga_basic())));
        priceAlacarte.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_harga_alacarte())));
        price_bundling.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_harga_bundling())));
        price_charge.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_harga_charge())));
        price_hardware.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_harga_hardware())));
        priceOffer.setText(MataUang.set_digit_rupiah(String.valueOf(tmpEstimation.getTotal_offer())));

    }

    private void setDefaultEstimation() {
        switch (code) {
            case "1":
                tmpEstimation.setHarga_installation(new BigInteger("200000"));
                break;
            case "2":
                tmpEstimation.setHarga_installation(new BigInteger("170000"));
                break;
            case "3":
                tmpEstimation.setHarga_installation(new BigInteger("150000"));
                break;
            default:
                tmpEstimation.setHarga_installation(new BigInteger("200000"));
                break;
        }
        tmpEstimation.setTotal_discount_installation(new BigInteger("0"));
        tmpEstimation.setTotal_harga_installation_with_discount(new BigInteger("0"));
        tmpEstimation.setTotal_harga_basic(new BigInteger("0"));
        tmpEstimation.setTotal_discount_basic(new BigInteger("0"));
        tmpEstimation.setTotal_harga_basic_with_discount(new BigInteger("0"));
        tmpEstimation.setTotal_harga_alacarte(new BigInteger("0"));
        tmpEstimation.setTotal_discount_alacarte(new BigInteger("0"));
        tmpEstimation.setTotal_discount_basic_alacarte(new BigInteger("0"));
        tmpEstimation.setTotal_harga_alacarte_with_discount(new BigInteger("0"));
        tmpEstimation.setTotal_harga_charge(new BigInteger("0"));
        tmpEstimation.setDiscount_harga_charge(new BigInteger("0"));
        tmpEstimation.setTotal_harga_hardware(new BigInteger("0"));
        tmpEstimation.setHarga_additional(new BigInteger("0"));
        tmpEstimation.setHarga_bundling(new BigInteger("0"));
        tmpEstimation.setTotal_discount__bundling(new BigInteger("0"));
        tmpEstimation.setTotal_harga_bundling(new BigInteger("0"));
        tmpEstimation.setTotal_harga_bundling_with_discount(new BigInteger("0"));
        tmpEstimation.setTotal_offer(new BigInteger("0"));
    }

    private View.OnClickListener addAdditionalItems = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AdditionalPackage();
            delete_additional.setVisibility(View.VISIBLE);
        }
    };


    private View.OnClickListener deleteAdditional = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setEstimasiBiaya();
            mLayoutAdditional.removeAllViews();
            delete_additional.setVisibility(View.GONE);

            /*for (int i = 0; i < mLayoutAdditional.getChildCount(); i++) {
                View childView = mLayoutAdditional.getChildAt(i);
                int b = mLayoutAdditional.indexOfChild(childView);
                materialAdd = (Spinner) childView.findViewWithTag("add_" + (b + 1));
                priceMaterial = mMaterialListItems.get(materialAdd.getSelectedItemPosition()).getRate();
                hargaAdditional = hargaAdditional + Integer.parseInt(priceMaterial);
                defaultF = NumberFormat.getInstance();
                formattednumber = defaultF.format(hrgBasic - hargaAdditional);
                strPaket = formattednumber.replace(",", ".");
                pricePackage.setText(strPaket);
                //mLayoutAdditional.removeView(mLayoutAdditional.findViewWithTag("ADDITIONAL_" + (b)));
            }*/

        }
    };

    private View.OnClickListener actAddPackage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (appType.equals("d")) {
                if (target > 2 && totPakacge == 3) {
                    Toast.makeText(activity, "Paket tidak boleh lebih dari 3", Toast.LENGTH_SHORT).show();
                } else if (target == 2 && totPakacge == 2) {
                    Toast.makeText(activity, "Paket tidak boleh lebih dari 2", Toast.LENGTH_SHORT).show();
                } else {
                    addPackage();
                }
            } else {
                if (totPakacge == 3) {
                    Toast.makeText(activity, "Paket tidak boleh lebih dari 3", Toast.LENGTH_SHORT).show();
                } else {
                    addPackage();
                }
            }
        }
    };


    private void AdditionalPackage() {
        tmpEstimation.setTotal_additional(tmpEstimation.getTotal_additional().add(new BigInteger("1")));
        mLayoutAdditional = (LinearLayout) mView.findViewById(R.id.package_additional);

        final LinearLayout linearPackage = new LinearLayout(activity);
        linearPackage.setBackgroundColor(activity.getResources().getColor(R.color.white));
        linearPackage.setOrientation(LinearLayout.VERTICAL);
        linearPackage.setTag("ADDITIONAL_" + totPakacge);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 10, 0, 0);
        linearPackage.setLayoutParams(pLinearPackage);
        mLayoutAdditional.addView(linearPackage);

        LinearLayout.LayoutParams pLineTitlePack = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lineTitlePack = new LinearLayout(activity);
        lineTitlePack.setBackgroundColor(activity.getResources().getColor(R.color.white));
        lineTitlePack.setTag("TITLE_" + tmpEstimation.getTotal_additional());
        lineTitlePack.setOrientation(LinearLayout.HORIZONTAL);
        lineTitlePack.setLayoutParams(pLineTitlePack);
        linearPackage.addView(lineTitlePack);

        LinearLayout.LayoutParams pTxtPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtPack = new TextView(activity);
        txtPack.setLayoutParams(pTxtPackage);
        txtPack.setText("Additional Item ");
        txtPack.setPadding(5, 0, 0, 0);
        txtPack.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitlePack.addView(txtPack);
        //Line For Change Items

        LinearLayout.LayoutParams pLineItems = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineItems.setMargins(0, 0, 0, 20);
        LinearLayout linearFormVC = new LinearLayout(activity);
        linearFormVC.setLayoutParams(pLineItems);
        linearFormVC.setOrientation(LinearLayout.HORIZONTAL);
        linearFormVC.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormVC);

        LinearLayout.LayoutParams pTxtItems = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtTitleItem = new TextView(activity);
        txtTitleItem.setLayoutParams(pTxtItems);
        txtTitleItem.setText("Items :");
        txtTitleItem.setPadding(5, 0, 0, 0);
        linearFormVC.addView(txtTitleItem);

        LinearLayout.LayoutParams pTxtAdditionalForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner spinnerAdditional = new Spinner(activity);
        spinnerAdditional.setLayoutParams(pTxtAdditionalForm);
        spinnerAdditional.setPadding(0, 5, 0, 5);
        spinnerAdditional.setTag("add_" + tmpEstimation.getTotal_additional());
        //genBasic("VC_" + totPakacge, mLayoutPackage);
        //genVC("item_" + totPakacge, mLayoutPackage);
        genMaterial("add_" + tmpEstimation.getTotal_additional(), mLayoutAdditional);
        linearFormVC.addView(spinnerAdditional);

        //End Line

        LinearLayout.LayoutParams pLineCounter = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineCounter.setMargins(0, 0, 0, 20);
        LinearLayout linearFormDSD = new LinearLayout(activity);
        linearFormDSD.setLayoutParams(pLineCounter);
        linearFormDSD.setOrientation(LinearLayout.HORIZONTAL);
        linearFormDSD.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormDSD);

        LinearLayout.LayoutParams pTxtCounter = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.30f);
        TextView txtDSD = new TextView(activity);
        txtDSD.setLayoutParams(pTxtCounter);
        txtDSD.setText("Total :");
        txtDSD.setPadding(5, 0, 0, 0);
        linearFormDSD.addView(txtDSD);

        LinearLayout.LayoutParams pTxtCounterForm = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.39f);
        EditText counterEdit = new EditText(activity);
        counterEdit.setLayoutParams(pTxtCounterForm);
        counterEdit.setPadding(5, 0, 0, 0);
        counterEdit.setBackgroundResource(R.drawable.border_item_zipcode);
        counterEdit.setInputType(InputType.TYPE_CLASS_DATETIME);
        counterEdit.setGravity(Gravity.CENTER);
        counterEdit.setTag("addCount_" + tmpEstimation.getTotal_additional());
        //genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(counterEdit);

        LinearLayout.LayoutParams pTxtSpinerSatuan = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.2f);
        LinearLayout linearLayout = new LinearLayout(activity);
        linearLayout.setLayoutParams(pTxtSpinerSatuan);
        /*pTxtSpinerSatuan.setMargins(25,0,50,0);
        spinnerSatuan.setPadding(0, 16, 0, 16);*/
        //txtDSDForm.setTag("DSD_" + totPakacge);
        //genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(linearLayout);
    }

    private void genMaterial(final String tagId, final LinearLayout mLayoutAdd) {
        mLayoutAdd.post(new Runnable() {
            @Override
            public void run() {
                materialAdd = (Spinner) mLayoutAdd.findViewWithTag(tagId);
                materialAdd.setAdapter(spinnerMaterialAdapter);
                materialAdd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //priceMaterial = mMaterialListItems.get(position).getRate();
                        tmpEstimation.setHarga_additional(new BigInteger("0"));
                        for (int i = 0; i < Integer.parseInt(String.valueOf(tmpEstimation.getTotal_additional())); i++) {
                            materialAdd = (Spinner) mLayoutAdditional.findViewWithTag("add_" + (i + 1));
                            countAdd = (EditText) mLayoutAdditional.findViewWithTag("addCount_" + (i + 1));
                            priceMaterial = mMaterialListItems.get(materialAdd.getSelectedItemPosition()).getRate();
                            if (!priceMaterial.equalsIgnoreCase("null")) {
                                tmpEstimation.setHarga_additional(tmpEstimation.getHarga_additional().add(new BigInteger(priceMaterial)));
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });
    }

    private void addPackage() {
        totPakacge = totPakacge + 1;
        mLayoutPackage = (LinearLayout) mView.findViewById(R.id.package_list);

        LinearLayout linearPackage = new LinearLayout(activity);
        linearPackage.setBackgroundColor(activity.getResources().getColor(R.color.white));
        linearPackage.setOrientation(LinearLayout.VERTICAL);
        linearPackage.setTag("PACK_" + totPakacge);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 10, 0, 0);
        linearPackage.setLayoutParams(pLinearPackage);
        mLayoutPackage.addView(linearPackage);

        LinearLayout.LayoutParams pLineTitlePack = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lineTitlePack = new LinearLayout(activity);
        lineTitlePack.setBackgroundColor(activity.getResources().getColor(R.color.white));
        lineTitlePack.setTag("TITLE_" + totPakacge);
        lineTitlePack.setOrientation(LinearLayout.HORIZONTAL);
        lineTitlePack.setLayoutParams(pLineTitlePack);
        linearPackage.addView(lineTitlePack);

        //Title Package
        LinearLayout.LayoutParams pTxtPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        txtPack = new TextView(activity);
        txtPack.setLayoutParams(pTxtPackage);
        if(totPakacge == 1){
            txtPack.setText("Package "+totPakacge+" (100% Price)");
        }else{
            txtPack.setText("Package "+totPakacge+" (50% Price)");
        }
        txtPack.setPadding(5, 5, 5, 5);
        txtPack.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitlePack.addView(txtPack);

        LinearLayout.LayoutParams pLineView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 0.5f);
        View lineView = new View(activity);
        lineView.setLayoutParams(pLineView);
        lineView.setBackgroundColor(activity.getResources().getColor(R.color.black_translucent));
        linearPackage.addView(lineView);

        LinearLayout.LayoutParams pLineForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearForm = new LinearLayout(activity);
        linearForm.setLayoutParams(pLineForm);
        linearForm.setOrientation(LinearLayout.HORIZONTAL);
        linearForm.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearForm);

        LinearLayout.LayoutParams pTxtBasic = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtBasic = new TextView(activity);
        txtBasic.setLayoutParams(pTxtBasic);
        txtBasic.setText("Basic");
        txtBasic.setPadding(5, 0, 0, 0);
        linearForm.addView(txtBasic);

        LinearLayout.LayoutParams pTxtBasicForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtBasicForm = new Spinner(activity);
        txtBasicForm.setLayoutParams(pTxtBasicForm);
        txtBasicForm.setPadding(0, 16, 0, 16);
        txtBasicForm.setTag("BASIC_" + totPakacge);
        genBasic("BASIC_" + totPakacge, mLayoutPackage);
        linearForm.addView(txtBasicForm);

        LinearLayout.LayoutParams pLineAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearAlacarte = new LinearLayout(activity);
        linearAlacarte.setLayoutParams(pLineAlacarte);
        linearAlacarte.setOrientation(LinearLayout.HORIZONTAL);
        linearAlacarte.setPadding(16, 10, 16, 10);
        linearPackage.addView(linearAlacarte);

        //Title Alacart
        LinearLayout.LayoutParams pTxtAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtAlacarte = new TextView(activity);
        txtAlacarte.setLayoutParams(pTxtAlacarte);
        txtAlacarte.setText("Alacarte");
        txtAlacarte.setGravity(TextView.TEXT_ALIGNMENT_CENTER);
        linearAlacarte.addView(txtAlacarte);

        LinearLayout.LayoutParams pListAlacarte = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        pListAlacarte.setMargins(0, 10, 0, 0);
        LinearLayout linearListAlacarte = new LinearLayout(activity);
        linearListAlacarte.setLayoutParams(pListAlacarte);
        linearListAlacarte.setOrientation(LinearLayout.VERTICAL);
        linearListAlacarte.setTag("LIST_" + totPakacge);
        linearAlacarte.addView(linearListAlacarte);

        // Button Add Alacart
        LinearLayout.LayoutParams pContentAla = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pContentAla.setMargins(5, 5, 0, 0);
        TextView txtAdd = new TextView(activity);
        txtAdd.setText("+ Add Alacarte");
        txtAdd.setTag("ADD_" + totPakacge);
        txtAdd.setPadding(10, 10, 10, 10);
        txtAdd.setClickable(true);
        txtAdd.setLayoutParams(pContentAla);
        txtAdd.setTextColor(activity.getResources().getColor(R.color.white));
        txtAdd.setBackgroundResource(R.color.blue_10);
        txtAdd.setOnClickListener(addAlacarteListener);
        linearListAlacarte.addView(txtAdd);

        addListAlacarte();

        LinearLayout.LayoutParams pLineTitleHW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineTitleHW.setMargins(0, 0, 0, 10);
        LinearLayout lineTitleHW = new LinearLayout(activity);
        lineTitleHW.setBackgroundColor(activity.getResources().getColor(R.color.white));
        lineTitleHW.setTag("TITLE_" + totPakacge);
        lineTitleHW.setOrientation(LinearLayout.HORIZONTAL);
        lineTitleHW.setLayoutParams(pLineTitleHW);
        linearPackage.addView(lineTitleHW);


        //List Hardware
        LinearLayout.LayoutParams pTxtHW = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtHW = new TextView(activity);
        txtHW.setLayoutParams(pTxtHW);
        txtHW.setText("Hardware");
        txtHW.setVisibility(View.GONE);
        txtHW.setPadding(5, 5, 5, 5);
        txtHW.setTypeface(Typeface.DEFAULT_BOLD);
        lineTitleHW.addView(txtHW);

        /**
         * Line For VC
         */
        LinearLayout.LayoutParams pLineFormVC = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormVC.setMargins(0, 0, 0, 20);
        LinearLayout linearFormVC = new LinearLayout(activity);
        linearFormVC.setLayoutParams(pLineFormVC);
        linearFormVC.setOrientation(LinearLayout.HORIZONTAL);
        linearFormVC.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormVC);

        LinearLayout.LayoutParams pTxtVC = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtVC = new TextView(activity);
        txtVC.setLayoutParams(pTxtVC);
        txtVC.setText("VC");
        txtVC.setVisibility(View.GONE);
        txtVC.setPadding(5, 0, 0, 0);
        linearFormVC.addView(txtVC);

        LinearLayout.LayoutParams pTxtVCForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtVCForm = new Spinner(activity);
        txtVCForm.setLayoutParams(pTxtVCForm);
        txtVCForm.setPadding(0, 16, 0, 16);
        txtVCForm.setTag("VC_" + totPakacge);
        txtVCForm.setVisibility(View.GONE);
        genVC("VC_" + totPakacge, mLayoutPackage);
        linearFormVC.addView(txtVCForm);

        /**
         * Line For DSD
         */
        LinearLayout.LayoutParams pLineFormDSD = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormDSD.setMargins(0, 0, 0, 20);
        LinearLayout linearFormDSD = new LinearLayout(activity);
        linearFormDSD.setLayoutParams(pLineFormDSD);
        linearFormDSD.setOrientation(LinearLayout.HORIZONTAL);
        linearFormDSD.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormDSD);

        LinearLayout.LayoutParams pTxtDSD = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtDSD = new TextView(activity);
        txtDSD.setLayoutParams(pTxtDSD);
        txtDSD.setText("DSD");
        txtDSD.setVisibility(View.GONE);
        txtDSD.setPadding(5, 0, 0, 0);
        linearFormDSD.addView(txtDSD);

        LinearLayout.LayoutParams pTxtDSDForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtDSDForm = new Spinner(activity);
        txtDSDForm.setLayoutParams(pTxtDSDForm);
        txtDSDForm.setPadding(0, 16, 0, 16);
        txtDSDForm.setTag("DSD_" + totPakacge);
        txtDSDForm.setVisibility(View.GONE);
        genDSD("DSD_" + totPakacge, mLayoutPackage);
        linearFormDSD.addView(txtDSDForm);

        /**
         * Line For LNB
         */

        LinearLayout.LayoutParams pLineFormLNB = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormLNB.setMargins(0, 0, 0, 20);
        LinearLayout linearFormLNB = new LinearLayout(activity);
        linearFormLNB.setLayoutParams(pLineFormLNB);
        linearFormLNB.setOrientation(LinearLayout.HORIZONTAL);
        linearFormLNB.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormLNB);

        LinearLayout.LayoutParams pTxtLNB = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtLNB = new TextView(activity);
        txtLNB.setLayoutParams(pTxtLNB);
        txtLNB.setText("LNB");
        txtLNB.setVisibility(View.GONE);
        txtLNB.setPadding(5, 0, 0, 0);
        linearFormLNB.addView(txtLNB);

        LinearLayout.LayoutParams pTxtLNBForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtLNBForm = new Spinner(activity);
        txtLNBForm.setLayoutParams(pTxtLNBForm);
        txtLNBForm.setPadding(0, 16, 0, 16);
        txtLNBForm.setTag("LNB_" + totPakacge);
        txtLNBForm.setVisibility(View.GONE);
        genLNB("LNB_" + totPakacge, mLayoutPackage);
        linearFormLNB.addView(txtLNBForm);

        /**
         * Line For ODU
         */

        LinearLayout.LayoutParams pLineFormODU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLineFormODU.setMargins(0, 0, 0, 20);
        LinearLayout linearFormODU = new LinearLayout(activity);
        linearFormODU.setLayoutParams(pLineFormODU);
        linearFormODU.setOrientation(LinearLayout.HORIZONTAL);
        linearFormODU.setPadding(10, 0, 10, 0);
        linearPackage.addView(linearFormODU);

        LinearLayout.LayoutParams pTxtODU = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        TextView txtODU = new TextView(activity);
        txtODU.setLayoutParams(pTxtODU);
        txtODU.setText("ODU");
        txtODU.setPadding(5, 0, 0, 0);
        txtODU.setVisibility(View.GONE);
        linearFormODU.addView(txtODU);

        LinearLayout.LayoutParams pTxtODUForm = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        Spinner txtODUForm = new Spinner(activity);
        txtODUForm.setLayoutParams(pTxtODUForm);
        txtODUForm.setPadding(0, 16, 0, 16);
        txtODUForm.setTag("ODU_" + totPakacge);
        txtODUForm.setVisibility(View.GONE);
        genODU("ODU_" + totPakacge, mLayoutPackage);
        linearFormODU.addView(txtODUForm);


        //Delete
        for (int i = 1; i < totPakacge; i++) {
            spinLnb = (Spinner) mLayoutPackage.findViewWithTag("LNB_" + i);
            spinLnb.setSelection(0);
            spinDish = (Spinner) mLayoutPackage.findViewWithTag("ODU_" + i);
            spinDish.setSelection(0);
        }
    }

    /**
     * Generate Package
     */

    private void genBasic(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                index = mLayoutPackage.indexOfChild(childView);
                spinPackage = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinPackage.setAdapter(spinAdapter);
                spinPackage.setOnItemSelectedListener(spinPackageListener);
            }
        });
    }

    private void ControlSpinner(String tag, String preMessaage) {
        count = mLayoutPackage.getChildCount();
        JSONObject obj;
        JSONArray result = new JSONArray();
        Spinner packageSpinner = null;
        int b = 0;

        for (int i = 0; i < count; i++) {
            obj = new JSONObject();
            View childView = mLayoutPackage.getChildAt(i);
            packageSpinner = (Spinner) childView.findViewWithTag(tag + (i + 1));
            Log.d("tagku", tag + (i + 1));
            b = mLayoutPackage.indexOfChild(childView);
            if (!packageSpinner.getSelectedItem().toString().equals("")) {
                try {
                    obj.put("id", b);
                    obj.put("value", packageSpinner.getSelectedItem().toString());
                    result.put(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        if (result != null || result.length() > 0) {
            int m;
            for (m = 0; m < result.length(); m++) {
                try {
                    JSONObject objres = result.getJSONObject(m);
                    int id = objres.getInt("id");
                    String value = objres.getString("value");
                    executeValidation(id, value, preMessaage, tag);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void executeValidation(int id, String value, String preMessaage, String tag) {
        Spinner spinner = null;
        if (count > 0) {
            for (int i = 1; i < count; i++) {
                View childLnb = mLayoutPackage.getChildAt(i - 1); // Start From 0
                spinner = (Spinner) childLnb.findViewWithTag(tag + i); // Start from 1
                int index = mLayoutPackage.indexOfChild(childLnb);
                if (id != index && value.equals(spinner.getSelectedItem().toString())) {
                    Toast.makeText(activity, preMessaage + " ada yang sama, mohon periksa kembali", Toast.LENGTH_SHORT).show();
                    spinner.setSelection(0);
                }

            }
        }
    }

    private void autoTextLNBandOdu(String tag, int position) {
        int count = mLayoutPackage.getChildCount();
        if (count > 1) {
            Spinner spinner = null;
            for (int n = 1; n < count; n++) {
                switch (tag) {
                    case "LNB_":
                        View childLnb = mLayoutPackage.getChildAt(n); // Start From 0
                        spinner = (Spinner) childLnb.findViewWithTag(tag + (n + 1)); // Start from 1
                        spinner.setSelection(position);
                        //spinner.setClickable(false);
                        break;
                    case "ODU_":
                        View childOdu = mLayoutPackage.getChildAt(n); // Start From 0
                        spinner = (Spinner) childOdu.findViewWithTag(tag + (n + 1)); // Start from 1
                        spinner.setSelection(position);
                        //spinner.setClickable(false);
                        break;
                }

            }
        }

    }

    private void genVC(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinVc = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinVc.setAdapter(spinAdapterVC);
                spinVc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        ControlSpinner("VC_", "VC");
                    }

                    @Override

                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genLNB(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinLnb = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                if (totPakacge > 1) {
                    spinLnb.setClickable(false);
                }
                spinLnb.setAdapter(spinAdapterLNB);
                spinLnb.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        //ControlSpinner("LNB_", "LNB", position, index);
                        autoTextLNBandOdu("LNB_", position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private void genODU(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinDish = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                if (totPakacge > 1) {
                    spinDish.setClickable(false);
                }
                spinDish.setAdapter(spinAdapterODU);
                spinDish.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        //ControlSpinner("ODU_", "ODU", position, index);
                        autoTextLNBandOdu("ODU_", position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });
    }

    private void genDSD(final String tagId, final LinearLayout mLayoutPackage) {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                spinDsd = (Spinner) mLayoutPackage.findViewWithTag(tagId);
                spinDsd.setAdapter(spinAdapterDSD);
                spinDsd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        View childView = mLayoutPackage.getChildAt(totPakacge - 1);
                        index = mLayoutPackage.indexOfChild(childView);
                        ControlSpinner("DSD_", "DSD");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
    }

    private View.OnClickListener deletePackage = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("FTPaket", "tag = " + view.getTag().toString());
            //setBasicId();
            delPackage(Integer.parseInt(view.getTag().toString()));
        }
    };

    private int getTarget() {
        tablePlanAdapter = new TablePlanAdapter(activity);
        dbAdapter = new TableFreeTrialAdapter(activity);
        String date = utils.getCurrentDate();
        listPlan = tablePlanAdapter.getDatabySfl(activity, TablePlan.KEY_PLAN_DATE, date);

        if (listPlan != null && listPlan.size() > 0) {
            TablePlan item = listPlan.get(0);
            if (item.getTarget() == null || item.getTarget().equalsIgnoreCase("")) {
                target = 0;
            } else {
                target = Integer.parseInt(item.getTarget());
            }
            Log.d("FTTASKLIST:", target +"");

            listFreetrial = dbAdapter.getDatabyCondition2(TableFreeTrial.KEY_PLAN_ID, item.getPlan_id());
            for (int a = 0; a < listFreetrial.size(); a++) {
                if (!listFreetrial.get(a).getValue().equalsIgnoreCase("TITLE")) {
                    Log.d("TASKLIST", listFreetrial.get(a).getFt_id());
                    Log.d("TASKLIST", listFreetrial.get(a).getValue());
                    target --;
                }
            }
        }
        return target;
    }

    private View.OnClickListener addAlacarteListener = new View.OnClickListener() {
        private ImageView closeAla;

        @Override
        public void onClick(View view) {
            View popUpView = activity.getLayoutInflater().inflate(R.layout.ms_package_alacarte_list, null);
            mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, true);
            mpopup.setAnimationStyle(android.R.style.Animation_Dialog);
            mpopup.showAtLocation(popUpView, Gravity.START, 0, 0);
            closeAla = (ImageView) popUpView.findViewById(R.id.close_alacarte);
            closeAla.setClickable(true);
            closeAla.setOnClickListener(closeAlaListener);
            loadAlacarte(popUpView, view.getTag().toString());
        }
    };

    private View.OnClickListener closeAlaListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            mpopup.dismiss();
        }
    };

    private void loadAlacarte(View popUpView, String tagId) {
        String[] tag = tagId.split("_");

        Spinner spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (tag[1]));
        String productId = mPackageProductListItemList
                .get(spinPackage.getSelectedItemPosition())
                .getProduct_id();

        basic_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getBasic_id();

        TableMasterPackageAdapter db = new TableMasterPackageAdapter(this.activity);
        recyclerView = (RecyclerView) popUpView.findViewById(R.id.package_alacarte_list);
        btnSave = (Button) popUpView.findViewById(R.id.btn_save_alacarte);
        btnSave.setTag("SAVE_" + tag[1]);
        btnSave.setOnClickListener(btnSaveListener);
        final LinearLayoutManager layoutParams = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutParams);
        mPackageProductListItem = db.fetchByProfile("1",
                mBrandListItem.get(spinBrand.getSelectedItemPosition()).getBrand_code(), productId);//IVDGT
        mPackageProductAdapter = new MSAlacarteListAdapter(mPackageProductListItem);
        recyclerView.setAdapter(mPackageProductAdapter);
    }

    private AdapterView.OnItemSelectedListener spinPackageListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

            set_Basic();

            defaultF = NumberFormat.getInstance();
            formattednumber = defaultF.format(totalOffer);
            strPaket = formattednumber.replace(",", ".");
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapter) {
        }
    };

    public void set_Basic() {
        TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
        List<TableMasterPackage> packageProductListItems;

        tmpHargaBasic.clear();
        tmpHargaCharge.clear();
        tmpHargaHardware.clear();
        tmpHardware.clear();

        hrgBasic = 0;
        for (int i = 0; i < totPakacge; i++) {
            spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
            product_id = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
            mPackageProductAdapter = new TableMasterPackageAdapter(activity);
            packageProductListItems = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID, product_id);
            for (int n = 0; n < packageProductListItems.size(); n++) {
                TableMasterPackage item = packageProductListItems.get(n);
                if (i == 0) {
                    hrgBasic =((Integer.parseInt(item.getPrice())));
                } else {
                    hrgBasic = ((Integer.parseInt(item.getPrice()) / 2));
                }
                tmpHargaBasic.add(i, new BigInteger(String.valueOf(hrgBasic)));

                String HW_S = "";


                if (appType.equals("d")) {
                    if (DTDStatus.HW_STATUS_TAG.equals("1")) {
                        HW_S = "1";
                    } else {
                        HW_S = "3";
                    }
                }else{
                    if (Ms_Fragment_Status.HW_STATUS_TAG.equals("1")) {
                        HW_S = "1";
                    } else {
                        HW_S = "3";
                    }
                }
                if(item.getIs_hd().equals("1")) {
                    tmpHardware.add(i, (String.valueOf("2")));
                }else{
                    tmpHardware.add(i, (String.valueOf("1")));
                }
                if(HW_S.equals("1")){
                    if(item.getIs_hd().equals("1")) {
                        if(isMulty == 1){
                            tmpHargaHardware.add(i, new BigInteger(String.valueOf("1700000")));
                        }else {
                            tmpHargaHardware.add(i, new BigInteger(String.valueOf("1500000")));
                        }
                    }else{
                        if(isMulty == 1){
                            tmpHargaHardware.add(i, new BigInteger(String.valueOf("1500000")));
                        }else {
                            tmpHargaHardware.add(i, new BigInteger(String.valueOf("1200000")));
                        }
                    }
                    Log.e("ZzzID", item.getProduct_id());
                    if(item.getProduct_id().equals("18") || item.getProduct_name().toLowerCase().equals("jual putus channels for life")){ // Free Hardware basic channel for life
                        tmpHargaHardware.set(i, new BigInteger(String.valueOf("0")));
                    }
                    tmpHargaCharge.add(i, new BigInteger(String.valueOf("0")));
                }else{
                    if(item.getIs_hd().equals("1")) {
                        tmpHargaCharge.add(i, new BigInteger(String.valueOf("20000")));
                    }else{
                        tmpHargaCharge.add(i, new BigInteger(String.valueOf("0")));
                    }

                    tmpHargaHardware.add(i, new BigInteger(String.valueOf("0")));
                }
            }
        }
        for(int i = 0 ; i < tmpHargaBasic.size() ; i++){
            Log.d("BASIC", tmpHargaBasic.get(i).toString());
        }

        Log.d("BASIC", tmpHargaBasic.size()+"");
        setEstimasiBiaya();
    }

    private View.OnClickListener btnSaveListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                String[] tag = view.getTag().toString().split("_");
                int objArray = Integer.parseInt(tag[1]) - 1;
                int dataAll = selAlacarteSaveAll.size();
                if (objArray + 1 > dataAll) {
                    selAlacarteSaveAll.add(objArray, mPackageProductAdapter.itemAlacarte.getList());
                } else {
                    selAlacarteSaveAll.set(objArray, mPackageProductAdapter.itemAlacarte.getList());
                }
                addListAlacarte();
            } catch (IndexOutOfBoundsException e) {

            }
            mpopup.dismiss();
        }
    };

    private void addListAlacarte() {
        mLayoutPackage.post(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < totPakacge; i++) {
                    LinearLayout mLayoutList = (LinearLayout) mLayoutPackage.findViewWithTag("LIST_" + (i + 1));
                    LinearLayout.LayoutParams params;
                    params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(5, 5, 0, 0);
                    selAlacarte.clear();
                    if (selAlacarteSaveAll.size() > 0) {
                        try {
                            for (int j = 0; j < selAlacarteSaveAll.get(i).size(); j++) {
                                selAlacarte.add(selAlacarteSaveAll.get(i).get(j).toString());
                            }
                        } catch (IndexOutOfBoundsException e) {
                        }
                    }

                    try {
                        mLayoutList.removeAllViews();

                        if (mLayoutList.findViewWithTag("ADD_" + (i + 1)) == null) {
                            TextView txtAdd = new TextView(activity);
                            txtAdd.setText("+ Add Alacarte");
                            txtAdd.setTag("ADD_" + (i + 1));
                            txtAdd.setPadding(10, 10, 10, 10);
                            txtAdd.setClickable(true);
                            txtAdd.setLayoutParams(params);
                            txtAdd.setTextColor(activity.getResources().getColor(R.color.white));
                            txtAdd.setBackgroundResource(R.color.blue_10);
                            txtAdd.setOnClickListener(addAlacarteListener);
                            mLayoutList.addView(txtAdd);
                        }

                        if (selAlacarte.size() > 0) {
                            for (int k = 0; k < selAlacarte.size(); k++) {
                                String[] alaData = selAlacarte.get(k).toString().split("#");
                                String alaName = alaData[1].toString();
                                String alaId = alaData[0].toString();
                                TableMasterPackageAdapter mPackageProductAdapter
                                        = new TableMasterPackageAdapter(activity);
                                List<TableMasterPackage> packageProductListItems
                                        = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fROWID,
                                        alaId);/*
                                for (int n=0;n<packageProductListItems.size();n++){
                                    PackageProductListItem item = packageProductListItems.get(n);
                                    try {
                                        String price = item.getPrice();
                                        if (item.getPrice()==""||item.getPrice()==null) {
                                            price = "0";
                                        }

                                        hrgAlacarte = hrgAlacarte+Integer.parseInt(price);
                                        duit = Double.parseDouble(String.valueOf(hrgBasic + hrgAlacarte));
                                        defaultF = NumberFormat.getInstance();
                                        formattednumber = defaultF.format(duit);
                                        strPaket = formattednumber.replace(",", ".");
                                        pricePackage.setText("Rp."+strPaket);
                                        //pricePackage.setText(String.valueOf(hrgBasic+hrgAlacarte));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Log.d("FTPaket", "Harga basic= "+ hrgBasic);
                                }*/
                                LinearLayout LL = new LinearLayout(activity);
                                LL.setOrientation(LinearLayout.HORIZONTAL);
                                LL.setGravity(Gravity.LEFT | Gravity.START);
                                LL.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                if (LL.findViewWithTag(alaId) == null) {
                                    TextView textView = new TextView(activity);
                                    textView.setText(alaName);
                                    textView.setTag(alaId);
                                    textView.setPadding(10, 10, 10, 10);
                                    textView.setLayoutParams(params);
                                    textView.setTextColor(activity.getResources().getColor(R.color.white));
                                    textView.setBackgroundResource(R.color.blue_grey_11);
                                    LL.addView(textView);

                                    ImageView imgDel = new ImageView(activity);
                                    imgDel.setImageResource(R.drawable.ic_close);
                                    imgDel.setTag(i + "_" + alaId);
                                    LinearLayout.LayoutParams imgParam = new LinearLayout.LayoutParams(60, 60);
                                    imgParam.setMargins(5, 5, 0, 0);
                                    imgDel.setLayoutParams(imgParam);
                                    imgDel.setBackgroundColor(activity.getResources().getColor(R.color.blue_grey_11));
                                    imgDel.setOnClickListener(delAlacarteListener);
                                    LL.addView(imgDel);

                                    mLayoutList.addView(LL);
                                }
                            }
                        }
                        int seq = i + 1;
                        LinearLayout layoutTitle = (LinearLayout) mLayoutPackage.findViewWithTag("TITLE_" + seq);
                        if (seq == totPakacge) {
                            if (layoutTitle.findViewWithTag(seq) == null) {
                                LinearLayout.LayoutParams pDelPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
                                pDelPackage.gravity = Gravity.END;
                                pDelPackage.setMargins(0, 0, 5, 0);
                                ImageView delPack = new ImageView(activity);
                                if (totPakacge > 2) {
                                    delPack.setImageResource(R.drawable.ic_close_package);
                                }

                                delPack.setTag(totPakacge);
                                delPack.setLayoutParams(pDelPackage);
                                delPack.setOnClickListener(deletePackage);
                                layoutTitle.addView(delPack);
                            }
                        } else {
                            layoutTitle.removeView(layoutTitle.findViewWithTag(seq));
                        }
                    } catch (NullPointerException e) {
                    }
                }

                tmpHargaAlacarte.clear();
                Log.e("selAlacarteSaveAll Size", selAlacarteSaveAll.size()+"");
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {
                    hrgAlacarte = 0;
                    Log.e("selAlacarteSaveAll X"+x, selAlacarteSaveAll.get(x).toString());
                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        Log.e("selAlacarteSaveAll Y"+y, selAlacarteSaveAll.get(x).get(y).toString());
                        if (x == 0) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + Integer.parseInt(item.getPrice());
                                Log.e("alacarte x0 - n"+n, item.getProduct_name());
                            }
                            //alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            //alaBuild1.append(",");
                        }
                        if (x == 1) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + (Integer.parseInt(item.getPrice()) / 2);
                                Log.e("alacarte x1 - n"+n, item.getProduct_name());
                            }
                        }
                        if (x == 2) {
                            String[] alaData = selAlacarteSaveAll.get(x).get(y).toString().split("#");
                            String alaName = alaData[1].toString();
                            String alaId = basic_id + alaData[0].toString();
                            TableMasterPackageAdapter mPackageProductAdapter
                                    = new TableMasterPackageAdapter(activity);
                            List<TableMasterPackage> packageProductListItems
                                    = mPackageProductAdapter.getDatabyCondition(TableMasterPackage.fKEYID, alaId);
                            for (int n = 0; n < packageProductListItems.size(); n++) {
                                TableMasterPackage item = packageProductListItems.get(n);
                                hrgAlacarte = hrgAlacarte + (Integer.parseInt(item.getPrice()) / 2);
                                Log.e("alacarte x2 - n"+n, item.getProduct_name());
                            }
                        }
                    }
                    tmpHargaAlacarte.add(x, new BigInteger(String.valueOf(hrgAlacarte)));
                }
                setEstimasiBiaya();

            }
        });
    }

    private View.OnClickListener delAlacarteListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String[] dataTag = view.getTag().toString().split("_");
            ArrayList<String> dataObject = selAlacarteSaveAll.get(Integer.parseInt(dataTag[0]));
            for (int i = 0; i < dataObject.size(); i++) {
                String[] ala = dataObject.get(i).toString().split("#");
                String idChannel = ala[0];
                if (idChannel.toString().equals(dataTag[1])) {
                    dataObject.remove(i);
                    addListAlacarte();
                }
            }
        }
    };


    private View.OnClickListener actSaveAll = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            try {
                JSONObject objAdd = null;
                String _vc, _lnb, _dsd, _odu, vcList = "", lnbList = "", dsdList = "", oduList = "";
                /*_vc     = vc.getText().toString();
                _lnb    = lnb.getText().toString();
                _dsd    = dsd.getText().toString();
                _odu    = dish.getText().toString();*/
                _vc = spinVc.getSelectedItem().toString();
                _lnb = spinLnb.getSelectedItem().toString();
                _dsd = spinDsd.getSelectedItem().toString();
                _odu = spinDish.getSelectedItem().toString();

                /*if (_vc == null || _vc.equalsIgnoreCase("") || _vc.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no VC.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_dsd == null || _dsd.equalsIgnoreCase("") || _dsd.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no DSD.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_lnb == null || _lnb.equalsIgnoreCase("") || _lnb.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no LNB.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (_odu == null || _odu.equalsIgnoreCase("") || _odu.length() <= 0) {
                    Toast.makeText(activity, "Silahkan masukan no ODU.", Toast.LENGTH_SHORT).show();
                    return;
                }*/


                for (int i = 0; i < listSOH.size(); i++) {
                    if (listSOH.get(i).getType().equalsIgnoreCase("VC")
                            && _vc.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        vcList = _vc;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("LNB")
                            && _lnb.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        lnbList = _lnb;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("ANT")
                            && _odu.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        oduList = _odu;
                    }

                    if (listSOH.get(i).getType().equalsIgnoreCase("DEC")
                            && _dsd.equalsIgnoreCase(listSOH.get(i).getSn())) {
                        dsdList = _dsd;
                    }
                }

                //MS DTD tidak input Hardware

                /*if (vcList == null || vcList.equalsIgnoreCase("")|| vcList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no VC.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (dsdList == null || dsdList.equalsIgnoreCase("") || dsdList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no DSD.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (lnbList == null || lnbList.equalsIgnoreCase("") || lnbList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no LNB.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (oduList == null || oduList.equalsIgnoreCase("") || oduList.length()<=0) {
                    Toast.makeText(activity, "Silahkan cek kembali no ODU.", Toast.LENGTH_SHORT).show();
                    return;
                }*/

                spinBrand = (Spinner) mView.findViewById(R.id.spinner_brand);
                //spinPromo = (Spinner) root.findViewById(R.id.spinner_promo);
                EditText biayaLain = (EditText) mView.findViewById(R.id.biaya_lain);
                EditText ketLain = (EditText) mView.findViewById(R.id.ket_lain);

                /*int selectedId = radioStatus.getCheckedRadioButtonId();
                selRadio = (RadioButton) mView.findViewById(selectedId);*/

                //alacarte
                ArrayList alacarte = new ArrayList();
                alacarte.add(selAlacarteSaveAll);

                ArrayList basic = new ArrayList();
                ArrayList arrayVc = new ArrayList();
                ArrayList arrayLNB = new ArrayList();
                ArrayList arrayODU = new ArrayList();
                ArrayList arrayDSD = new ArrayList();

                arrayVc.clear();
                arrayLNB.clear();
                arrayODU.clear();
                arrayDSD.clear();
                basic.clear();

                for (int i = 0; i < totPakacge; i++) {
                    spinPackage = (Spinner) mLayoutPackage.findViewWithTag("BASIC_" + (i + 1));
                    spinVc = (Spinner) mLayoutPackage.findViewWithTag("VC_" + (i + 1));
                    spinLnb = (Spinner) mLayoutPackage.findViewWithTag("LNB_" + (i + 1));
                    spinDish = (Spinner) mLayoutPackage.findViewWithTag("ODU_" + (i + 1));
                    spinDsd = (Spinner) mLayoutPackage.findViewWithTag("DSD_" + (i + 1));

                    String vcId = listVc.get(spinVc.getSelectedItemPosition());
                    arrayVc.add(vcId);

                    String lnbId = listLnb.get(spinLnb.getSelectedItemPosition());
                    arrayLNB.add(lnbId);

                    String oduId = listDish.get(spinDish.getSelectedItemPosition());
                    arrayODU.add(oduId);

                    String dsdId = listDsd.get(spinDsd.getSelectedItemPosition());
                    arrayDSD.add(dsdId);

                    String basicId = mPackageProductListItemList.get(spinPackage.getSelectedItemPosition()).getProduct_id();
                    basic.add(basicId);
                }

                JSONArray arrayAdd = new JSONArray();
                JSONObject addResult = new JSONObject();
                for (int i = 0; i < Integer.parseInt(String.valueOf(tmpEstimation.getTotal_additional())); i++) {
                    materialAdd = (Spinner) mLayoutAdditional.findViewWithTag("add_" + (i + 1));
                    countAdd = (EditText) mLayoutAdditional.findViewWithTag("addCount_" + (i + 1));

                    objAdd = new JSONObject();
                    objAdd.put("additional_id", mMaterialListItems.get
                            (materialAdd.getSelectedItemPosition()).getHw_id());
                    objAdd.put("additional_total", countAdd.getText().toString());
                    arrayAdd.put(objAdd);
                    addResult.put("data", arrayAdd);
                }


                String basic1 = "";
                String basic2 = "";
                String basic3 = "";
                String alacarte1 = "";
                String alacarte2 = "";
                String alacarte3 = "";
                String vc1 = "";
                String vc2 = "";
                String vc3 = "";
                String lnb1 = "";
                String lnb2 = "";
                String lnb3 = "";
                String odu1 = "";
                String odu2 = "";
                String odu3 = "";
                String dsd1 = "";
                String dsd2 = "";
                String dsd3 = "";

                StringBuilder alaBuild1 = new StringBuilder();
                StringBuilder alaBuild2 = new StringBuilder();
                StringBuilder alaBuild3 = new StringBuilder();
                for (int x = 0; x < selAlacarteSaveAll.size(); x++) {

                    for (int y = 0; y < selAlacarteSaveAll.get(x).size(); y++) {
                        if (x == 0) {
                            alaBuild1.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild1.append(",");
                        }
                        if (x == 1) {
                            alaBuild2.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild2.append(",");
                        }
                        if (x == 2) {
                            alaBuild3.append(selAlacarteSaveAll.get(x).get(y).toString());
                            alaBuild3.append(",");
                        }
                    }

                    if (x == 0) {
                        alacarte1 = alaBuild1.toString().substring(0, alaBuild1.length() - 1);
                    }
                    if (x == 1) {
                        alacarte2 = alaBuild2.toString().substring(0, alaBuild2.length() - 1);
                    }
                    if (x == 2) {
                        alacarte3 = alaBuild3.toString().substring(0, alaBuild3.length() - 1);
                    }
                }

                for (int x = 0; x < basic.size(); x++) {
                    if (x == 0) {
                        basic1 = basic.get(x).toString();
                    }

                    if (x == 1) {
                        basic2 = basic.get(x).toString();
                    }

                    if (x == 2) {
                        basic3 = basic.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayVc.size(); x++) {
                    if (x == 0) {
                        vc1 = arrayVc.get(x).toString();
                    }

                    if (x == 1) {
                        vc2 = arrayVc.get(x).toString();
                    }

                    if (x == 2) {
                        vc3 = arrayVc.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayLNB.size(); x++) {
                    if (x == 0) {
                        lnb1 = arrayLNB.get(x).toString();
                    }
                    if (x == 1) {
                        lnb2 = arrayLNB.get(x).toString();
                    }
                    if (x == 2) {
                        lnb3 = arrayLNB.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayODU.size(); x++) {
                    if (x == 0) {
                        odu1 = arrayODU.get(x).toString();
                    }
                    if (x == 1) {
                        odu2 = arrayODU.get(x).toString();
                    }
                    if (x == 2) {
                        odu3 = arrayODU.get(x).toString();
                    }
                }

                for (int x = 0; x < arrayDSD.size(); x++) {
                    if (x == 0) {
                        dsd1 = arrayDSD.get(x).toString();
                    }
                    if (x == 1) {
                        dsd2 = arrayDSD.get(x).toString();
                    }
                    if (x == 2) {
                        dsd3 = arrayDSD.get(x).toString();
                    }
                }
                /*try {
                    if (selRadio.getText().toString().equalsIgnoreCase("") || selRadio.getText().toString().length() < 0) {
                        Toast.makeText(activity, "Cek Hw Status", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (NullPointerException x) {
                    Toast.makeText(activity, "Cek Hw Status", Toast.LENGTH_SHORT).show();
                    return;
                }*/


                /*saveStatus = selRadio.getText().toString();
                if (saveStatus.equalsIgnoreCase("Pinjam")) {
                    saveStatus = "1";
                } else {
                    saveStatus = "2";
                }*/

                saveBrand = mBrandListItem.get(spinBrand.getSelectedItemPosition()).getBrand_id();
                //savePromo = mPromoListItems.get(spinPromo.getSelectedItemPosition()).getId();
                //saveBiayaLain = biayaLain.getText().toString();
                //saveKetLain = ketLain.getText().toString();

                JSONObject basicJson = new JSONObject();
                JSONObject alaJson = new JSONObject();
                JSONObject packageDet = new JSONObject();
                JSONObject vcJson = new JSONObject();
                JSONObject lnbJson = new JSONObject();
                JSONObject oduJson = new JSONObject();
                JSONObject dsdJson = new JSONObject();
                JSONObject objBundling = new JSONObject();


                JSONArray resultService = new JSONArray();
                JSONArray resultPromoFix = new JSONArray();

                try {
                    basicJson.put("1", basic1);
                    basicJson.put("2", basic2);
                    basicJson.put("3", basic3);

                    alaJson.put("1", alacarte1);
                    alaJson.put("2", alacarte2);
                    alaJson.put("3", alacarte3);

                    vcJson.put("1", vc1);
                    vcJson.put("2", vc2);
                    vcJson.put("3", vc3);

                    lnbJson.put("1", lnb1);
                    lnbJson.put("2", lnb2);
                    lnbJson.put("3", lnb3);

                    oduJson.put("1", odu1);
                    oduJson.put("2", odu2);
                    oduJson.put("3", odu3);

                    dsdJson.put("1", dsd1);
                    dsdJson.put("2", dsd2);
                    dsdJson.put("3", dsd3);
                    packageDet.put("BRAND", saveBrand);

                    objBundling.put("sim_id", simcardId);
                    objBundling.put("sim_value", simcardValue.getText().toString());
                    objBundling.put("router_id", routerId);
                    objBundling.put("router_value", routerValue.getText().toString());


                    /*if (numberForm.substring(0, 1).equalsIgnoreCase("p")) {
                        saveStatus = "2";
                    } else {
                        saveStatus = "1";
                    }
                    packageDet.put("HW_STATUS", saveStatus);*/

                    packageDet.put("BASIC", basicJson);
                    packageDet.put("ALACARTE", alaJson);
                    packageDet.put("VC", vcJson);
                    packageDet.put("LNB", lnbJson);
                    packageDet.put("ODU", oduJson);
                    packageDet.put("DSD", dsdJson);
                    packageDet.put("isMulty", isMulty);
                    packageDet.put("additional", addResult);
                    packageDet.put("bundling", objBundling);
                    //packageDet.put("VC", _vc);
                    // packageDet.put("LNB", _lnb);
                    //packageDet.put("DSD", _dsd);
                    //packageDet.put("ODU", _odu);
                    packageDet.put("periode", strBillFrek);
                    packageDet.put("promoCode", promoCode);


                    for (int i =0; i < strPromoFix.size(); i++){
                        JSONObject proFix = new JSONObject();
                        proFix.put("id_service", strPromoFix.get(i));
                        resultPromoFix.put(proFix);
                    }

                    packageDet.put("promoFix", resultPromoFix);

                    String HWData = "";
                    for(int f = 0; f < tmpHardware.size() ; f++){
                        HWData = HWData + tmpHardware.get(f);
                        if(f < tmpHardware.size()-1){
                            HWData += ",";
                        }
                    }
                    packageDet.put("HWData", HWData);

                    packageDet.put("totalOffer", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_offer())));
                    packageDet.put("totalBasic", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_harga_basic())));
                    packageDet.put("totalAlacarte", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_harga_alacarte())));
                    packageDet.put("INSTALL_BILL",Integer.parseInt(String.valueOf(tmpEstimation.getHarga_installation())));
                    packageDet.put("MONTHLY_BILL", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_harga_basic())));
                    packageDet.put("priceHardware", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_harga_hardware())));
                    packageDet.put("priceBundling", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_harga_bundling())));
                    packageDet.put("priceDecHD", Integer.parseInt(String.valueOf(tmpEstimation.getTotal_harga_charge())));
                    packageDet.put("amount", String.valueOf(tmpEstimation.getTotal_bayar()));

                    for (int i = 0; i < currentPromoServiceSelect.size(); i++) {
                        JSONObject proService = new JSONObject();
                        proService.put("id_service", currentPromoServiceSelect.get(i).getId_service());
                        resultService.put(proService);

                    }
                    packageDet.put("promoService", resultService);

                    //packageDet.put("PROMO", savePromo);
                    //packageDet.put("BIAYA_LAIN", saveBiayaLain);
                    //packageDet.put("KET_LAIN", saveKetLain);

                    Log.d("jsonku", HWData + "--" +packageDet.toString());
                    TableFormAppAdapter db = new TableFormAppAdapter(activity);
                    List<TableFormApp> sel = db.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
                    if (sel.size() > 0) {
                        db.updatePartial(activity, TableFormApp.fVALUES_PACKAGE, packageDet.toString(),
                                TableFormApp.fFORM_NO, numberForm);
                        Toast.makeText(activity, "Package Saved", Toast.LENGTH_SHORT).show();
                        /*if (!isShowBundling) {
                            if (simcardValue.getText().toString().trim().length() < 1) {
                                Toast.makeText(activity, "Sim Card tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else if (routerValue.getText().toString().trim().length() < 1) {
                                Toast.makeText(activity, "Sim Card tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            } else if (!simcardValue.getText().toString().startsWith("62")) {
                                Toast.makeText(activity, "Sim Card harus di mulai dari 62", Toast.LENGTH_SHORT).show();
                            } else {
                                db.updatePartial(activity, TableFormApp.fVALUES_PACKAGE, packageDet.toString(),
                                        TableFormApp.fFORM_NO, numberForm);
                                Toast.makeText(activity, "Package Saved", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            db.updatePartial(activity, TableFormApp.fVALUES_PACKAGE, packageDet.toString(),
                                    TableFormApp.fFORM_NO, numberForm);
                            Toast.makeText(activity, "Package Saved", Toast.LENGTH_SHORT).show();
                        }*/
                    } else {
                        Toast.makeText(activity, "harap isi profile", Toast.LENGTH_SHORT).show();
                    }
                    Log.d("jsonku", packageDet.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("errorku", e.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("errorku", e.toString());
            }
        }
    };


}
