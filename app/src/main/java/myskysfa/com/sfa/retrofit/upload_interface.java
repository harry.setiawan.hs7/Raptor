package myskysfa.com.sfa.retrofit;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by harrysetiawan on 14/09/17.
 */

public interface upload_interface {
    @Multipart
    @POST("{form_no}")
    Call<response_upload> ploadMulFile(@Path("form_no") String form_no, @Part List<MultipartBody.Part> file);

    @POST("{postal_code}")
    Call<response_transport> getDataTransport(@Path("postal_code") String postal_code);
}
