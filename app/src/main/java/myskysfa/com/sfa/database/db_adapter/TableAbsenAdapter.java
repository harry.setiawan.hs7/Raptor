package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableAbsensi;
import myskysfa.com.sfa.database.TableMasterPackage;
import myskysfa.com.sfa.utils.DatabaseManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by admin on 9/28/2016.
 */

public class TableAbsenAdapter {

    static private TableAbsenAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableAbsenAdapter(ctx);
        }
    }

    static public TableAbsenAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableAbsenAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableAbsensi> getAllData() {
        List<TableAbsensi> tblsatu = null;
        try {
            tblsatu = getHelper().getTableAbsensiDAODAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableAbsensi tbl, String uid, String id_store, String absen_masuk,
                           String absen_pulang, String latitude, String longitude, String distance) {
        try {
            tbl.setUid(uid);
            tbl.setId_store(id_store);
            tbl.setAbsen_masuk(absen_masuk);
            tbl.setAbsen_pulang(absen_pulang);
            tbl.setLatitude(latitude);
            tbl.setLongitude(longitude);
            tbl.setDistance(distance);
            getHelper().getTableAbsensiDAODAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Update by Condition
    */
    public void updatePartial(Context context, String column, Object value, String
            column2, Object value2) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableAbsensi.class);
            UpdateBuilder<TableAbsensi, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value).updateColumnValue(column2, value2);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            Dao dao = helper.getDao(TableAbsensi.class);
            DeleteBuilder<TableAbsensi, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method for count data by condition
     */
    public Long getDataCount(String condition, String param) {
        Long usuarios = 0L;
        try {
            dao = helper.getDao(TableAbsensi.class);
            QueryBuilder queryBuilder = dao.queryBuilder();
            queryBuilder.setCountOf(true);
            queryBuilder.setWhere(queryBuilder.where().eq(condition, param));
            usuarios = dao.countOf(queryBuilder.prepare());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarios;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableAbsensi> getDataByCondition(String condition, Object param) {
        List<TableAbsensi> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableAbsensi.class);
            QueryBuilder<TableAbsensi, Integer> queryBuilder = dao.queryBuilder();
            Where<TableAbsensi, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }
}
