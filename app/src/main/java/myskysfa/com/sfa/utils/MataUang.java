package myskysfa.com.sfa.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by harry_setiawan on 01/10/17.
 */

public class MataUang {
    public static String curs_indonesia(String digit){
        Double digitnya = Double.parseDouble(digit);
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);
        String hasil = String.valueOf(kursIndonesia.format(digitnya));
        return hasil.substring(0, hasil.length()-2) +"-";
    }

    public static String set_digit_rupiah(String hasil){
        float jumlah = Float.parseFloat(String.valueOf(hasil));
        DecimalFormat angka = new DecimalFormat("###,###");

        String ret = angka.format(jumlah).toString().replaceAll(",",".");

        return ret;
    }

}
