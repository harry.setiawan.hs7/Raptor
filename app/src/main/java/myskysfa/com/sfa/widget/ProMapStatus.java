package myskysfa.com.sfa.widget;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.main.menu.dtdproject.FormProDTD;
import myskysfa.com.sfa.main.menu.dtdproject.ProStatus;
import myskysfa.com.sfa.utils.LocationService;

import static com.google.android.gms.wearable.DataMap.TAG;

public class ProMapStatus extends Fragment{

    private MapView map;
    private ImageView imagetrans;
    private MapController mapController;
    protected FolderOverlay prospectOverlay;
    ArrayList<OverlayItem> overlayItemArray;
    ViewGroup v ;
    public LocationService locationService;
    public boolean isMarkerAdded = false;
    public boolean isDrag = false;
    public final MyMarkerDragListener myMarkerDragListener = new MyMarkerDragListener();
    private ImageView my_loc;
    public Location location;
    FormProDTD activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = (ViewGroup) inflater.inflate(R.layout.map_ms_status, container, false);
        activity = (FormProDTD) getActivity();
        
        map = (MapView) v.findViewById(R.id.openmapview);
        my_loc = (ImageView) v.findViewById(R.id.my_loc);
        imagetrans = (ImageView) v.findViewById(R.id.imagetrans);
        set_osm();
        return v;
    }

    private void set_osm() {
        map.setBuiltInZoomControls(false);
        map.setMultiTouchControls(true);
        mapController = (MapController) map.getController();
        mapController.setZoom(20);

        //Create Overlay
        overlayItemArray = new ArrayList<>();

        //DefaultResourceProxyImpl defaultResourceProxyImpl = new DefaultResourceProxyImpl(getActivity());
        MyItemizedIconOverlay myItemizedIconOverlay = new MyItemizedIconOverlay(activity.proStatus.getContext(), overlayItemArray, null);
        map.getOverlays().add(myItemizedIconOverlay);

        //Add Scale Bar
        ScaleBarOverlay myScaleBarOverlay = new ScaleBarOverlay(map);
        map.getOverlays().add(myScaleBarOverlay);

        prospectOverlay = new FolderOverlay();
        map.getOverlays().add(prospectOverlay);

        locationService = new LocationService(getActivity());
        locationService.setListener(new LocationService.Listener() {
            @Override
            public void locationChange(Location location) {
                updateLocation(location);
            }
        });

        imagetrans.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        activity.proStatus.nestedscroll.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;
                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        activity.proStatus.nestedscroll.requestDisallowInterceptTouchEvent(true);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        activity.proStatus.nestedscroll.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });
        my_loc.setClickable(true);
        my_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(location != null){
                    mapController.setCenter(new GeoPoint(location.getLatitude(), location.getLongitude()));
                    setOverlayLocation(location);
                    map.invalidate();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        locationService.stopLocationUpdates();
    }

    private void updateLocation(Location location){
        this.location = location;
        if (!isDrag){
            Log.d("location", "set "+location.getLatitude()+" & "+location.getLongitude());
            activity.proStatus.latitude = location.getLatitude();
            activity.proStatus.longitude = location.getLongitude();
            getAddress(activity.proStatus.getContext(), activity.proStatus.latitude, activity.proStatus.longitude, location);
            show_address(location);
            mapController.setCenter(new GeoPoint(activity.proStatus.latitude, activity.proStatus.longitude));
            setOverlayLocation(location);
            map.invalidate();
        } else {
            Log.d("location", "update "+location.getLatitude()+" & "+location.getLongitude());
            mapController.setCenter(new GeoPoint(activity.proStatus.latitude, activity.proStatus.longitude));
            setOverlayLocation(location);
            map.invalidate();
        }
        if (!isMarkerAdded){
            createMarker(new GeoPoint(activity.proStatus.latitude, activity.proStatus.longitude), "Lokasi Prospek", R.drawable.marker_destination);
            isMarkerAdded = true;
        }
    }

    private void updateDragLocation(Location location){
        Log.d("location", "drag "+location.getLatitude()+" & "+location.getLongitude());
        activity.proStatus.latitude = location.getLatitude();
        activity.proStatus.longitude = location.getLongitude();
        getAddress(activity.proStatus.getContext(), activity.proStatus.latitude, activity.proStatus.longitude, location);
        show_address(location);
    }

    private void show_address(Location location){
        activity.proStatus.list.clear();
        activity.proStatus.list.add(String.valueOf(location.getLongitude()));
        activity.proStatus.list.add(String.valueOf(location.getLatitude()));
        activity.proStatus.list.add(String.valueOf(activity.proStatus.addressLine));
        activity.proStatus.changeStatus();

    }
    public void getAddress(Context context, double LATITUDE, double LONGITUDE, Location location) {

        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                Log.d(TAG, "getAddress:  address" + address);
                activity.proStatus.addressLine = address;
                show_address(location);
            }
        } catch (IOException e) {
            activity.proStatus.addressLine = " - ";
            show_address(location);
        }
        return;
    }

    private void setOverlayLocation(Location overlayLocation){
        GeoPoint geoPoint = new GeoPoint(overlayLocation);
        overlayItemArray.clear();
        OverlayItem newMyLocationItem = new OverlayItem("My Location", "My Location", geoPoint);
        overlayItemArray.add(newMyLocationItem);
    }

    public Marker createMarker(GeoPoint p, String title, int markerResId) {
        Marker marker = new Marker(map);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setDraggable(true);
        marker.setOnMarkerDragListener(myMarkerDragListener);
        prospectOverlay.add(marker);
        marker.setTitle(title);
        marker.setPosition(p);
        marker.setIcon(ResourcesCompat.getDrawable(getResources(), markerResId, null));
        map.invalidate();
        return marker;
    }

    private class MyMarkerDragListener implements Marker.OnMarkerDragListener {
        @Override
        public void onMarkerDrag(Marker marker) {

        }

        @Override
        public void onMarkerDragEnd(Marker marker) {
            isDrag = true;
            GeoPoint prospekPoint = marker.getPosition();
            Location prospekLocation = new Location("lokasi prospek");
            prospekLocation.setLatitude(prospekPoint.getLatitude());
            prospekLocation.setLongitude(prospekPoint.getLongitude());
            updateDragLocation(prospekLocation);
        }

        @Override
        public void onMarkerDragStart(Marker marker) {

        }
    }

    private class MyItemizedIconOverlay extends ItemizedIconOverlay<OverlayItem> {

        MyItemizedIconOverlay(
                Context context,
                List<OverlayItem> pList,
                org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener<OverlayItem> pOnItemGestureListener) {
            super(context, pList, pOnItemGestureListener);
        }

        @Override
        public void draw(Canvas canvas, MapView mapview, boolean arg2) {
            super.draw(canvas, mapview, arg2);
            if(!overlayItemArray.isEmpty()){
                GeoPoint in = (GeoPoint) overlayItemArray.get(0).getPoint();
                Point out = new Point();
                mapview.getProjection().toPixels(in, out);
                Bitmap bm = BitmapFactory.decodeResource(getResources(),
                        R.drawable.marker_poi_default);
                canvas.drawBitmap(bm,
                        out.x - bm.getWidth()/2, 	//shift the bitmap center
                        out.y - bm.getHeight()/2, 	//shift the bitmap center
                        null);
            }
        }

        @Override
        public boolean onSingleTapUp(MotionEvent event, MapView mapView) {
            return true;
        }
    }
}