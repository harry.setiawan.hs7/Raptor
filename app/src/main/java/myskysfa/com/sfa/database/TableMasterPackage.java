package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_package")
public class TableMasterPackage {


    public static final String TABLE_NAME = "m_package_product";
    public static final String fID = "id";
    public static final String fROWID = "product_id";
    public static final String fPRODUCT_TYPE = "product_type";
    public static final String fPRODUCT_NAME = "product_name";
    public static final String fPRICE = "price";
    public static final String fALACARTE = "alacarte";
    public static final String fFLAG_PRODUCT = "flag_product";
    public static final String fBRAND = "brand";
    public static final String fKEYID = "key_id";
    public static final String fBASIKID = "basic_id";
    public static final String fISHD = "is_hd";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String product_id;
    @DatabaseField
    private String product_type;
    @DatabaseField
    private String product_name;
    @DatabaseField
    private String price;
    @DatabaseField
    private String alacarte;
    @DatabaseField
    private String flag_product;
    @DatabaseField
    private String brand;
    @DatabaseField
    private String key_id;
    @DatabaseField
    private String basic_id;
    @DatabaseField
    private String is_hd;

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getfID() {
        return fID;
    }

    public static String getfROWID() {
        return fROWID;
    }

    public static String getfPRODUCT_TYPE() {
        return fPRODUCT_TYPE;
    }

    public static String getfPRODUCT_NAME() {
        return fPRODUCT_NAME;
    }

    public static String getfPRICE() {
        return fPRICE;
    }

    public static String getfALACARTE() {
        return fALACARTE;
    }

    public static String getfFLAG_PRODUCT() {
        return fFLAG_PRODUCT;
    }

    public static String getfBRAND() {
        return fBRAND;
    }

    public static String getfKEYID() {
        return fKEYID;
    }

    public static String getfBASIKID() {
        return fBASIKID;
    }

    public static String getfISHD() {
        return fISHD;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAlacarte() {
        return alacarte;
    }

    public void setAlacarte(String alacarte) {
        this.alacarte = alacarte;
    }

    public String getFlag_product() {
        return flag_product;
    }

    public void setFlag_product(String flag_product) {
        this.flag_product = flag_product;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getKey_id() {
        return key_id;
    }

    public void setKey_id(String key_id) {
        this.key_id = key_id;
    }

    public String getBasic_id() {
        return basic_id;
    }

    public void setBasic_id(String basic_id) {
        this.basic_id = basic_id;
    }

    public String getIs_hd() {
        return is_hd;
    }

    public void setIs_hd(String is_hd) {
        this.is_hd = is_hd;
    }
}
