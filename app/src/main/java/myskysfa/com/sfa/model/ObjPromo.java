package myskysfa.com.sfa.model;

/**
 * Created by harry_setiawan on 29/09/17.
 */

public class ObjPromo {
    private int jumlah;
    private String descryption;

    public ObjPromo(int jumlah, String descryption) {
        this.jumlah = jumlah;
        this.descryption = descryption;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getDescryption() {
        return descryption;
    }

    public void setDescryption(String descryption) {
        this.descryption = descryption;
    }
}
