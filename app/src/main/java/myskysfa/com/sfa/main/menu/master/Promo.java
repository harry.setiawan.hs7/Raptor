package myskysfa.com.sfa.main.menu.master;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.master.PromoAdapter;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.TablePromotionFix;
import myskysfa.com.sfa.database.TablePromotionServices;
import myskysfa.com.sfa.database.db_adapter.TableMasterPromoAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePromotionFixAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePromotionServiceAdapter;
import myskysfa.com.sfa.main.menu.DataMaster;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionDetector;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;

/**
 * Created by admin on 6/20/2016.
 */
public class Promo extends Fragment {
    private List<TableMasterPromo> listPromo;
    private TableMasterPromoAdapter tblAdapter;
    private RecyclerView recyclerView;
    private PromoAdapter adapter;
    private String status;
    private Utils utils;
    private Handler handler = new Handler();
    private SwipeRefreshLayout swipeRefreshLayout;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    public static final String PREF_PROMO = "RAPTOR_PROMO";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.fragment_promo, container, false);
        recyclerView = (RecyclerView) viewGroup.findViewById(R.id.list_promo);
        swipeRefreshLayout = (SwipeRefreshLayout) viewGroup.findViewById(R.id.swipe_container);
        tblAdapter = new TableMasterPromoAdapter(getActivity());
        utils = new Utils(getActivity());
        showList();
        swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cd = new ConnectionDetector(getContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new getDataPromo().execute();
                } else {
                    utils.showAlertDialog(getContext(), getResources().getString(R.string.no_internet1),getResources().getString(R.string.no_internet2), false);
                }
            }
        });
        return viewGroup;
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutParams);
        listPromo = tblAdapter.getAllData();
        adapter = new PromoAdapter(getActivity(), listPromo);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Get Data From Server
     */
    protected class getDataPromo extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url = ConnectionManager.CM_URL_MASTER_PROMOTION;
                String response = ConnectionManager.requestMasterPackage(url,Config.version, getContext());
                System.out.println(response.toString());
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject != null) {
                    status = jsonObject.getString(Config.KEY_STATUS).toString();
                    if(status.equals("true")){
                        //JSONArray jsonData = new JSONArray (jsonObject.getString(Config.KEY_DATA).toString());
                        JSONObject data = new JSONObject(jsonObject.getString(Config.KEY_DATA));

                        JSONArray jsonPromotion = new JSONArray(data.getString("promotion").toString());
                        JSONArray jsonPromotionFix = new JSONArray(data.getString("promotion_fixed").toString());
                        JSONArray jsonpromotionServices = new JSONArray(data.getString("promotion_services").toString());

                        System.out.println("jsonData: "+jsonPromotion);
                        if(jsonPromotion != null) {
                            TableMasterPromoAdapter db = new TableMasterPromoAdapter(getContext());
                            TablePromotionFixAdapter db2 = new TablePromotionFixAdapter(getContext());
                            TablePromotionServiceAdapter db3 = new TablePromotionServiceAdapter(getContext());
                            db.delete(); // delete all master
                            db2.delete();
                            db3.delete();
                            System.out.println("delete all");

                            for (int i=0;i<jsonPromotion.length();i++) {
                                String promo_id = jsonPromotion.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_ID);
                                String promo_code = jsonPromotion.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_CODE);
                                String promo_desc = jsonPromotion.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_DESC);
                                String promo_start = jsonPromotion.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_START);
                                String promo_end = jsonPromotion.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_END);

                                db.insertData(new TableMasterPromo(),promo_id,promo_code,promo_desc,promo_start,promo_end,"A","1");
                            }

                            for (int i=0;i<jsonPromotionFix.length();i++) {
                                String promo_idpromo = jsonPromotionFix.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_IDPROMO);
                                String promo_idfix = jsonPromotionFix.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_IDFIX);
                                String promo_disc = jsonPromotionFix.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_DISC);
                                String promo_amount = jsonPromotionFix.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_AMOUNT);
                                String promo_target = jsonPromotionFix.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_TARGET);
                                String promo_id_edit = jsonPromotionFix.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_EDIT);

                                db2.insertData(new TablePromotionFix(),promo_idpromo,promo_idfix,promo_disc,promo_amount, promo_id_edit, promo_target);
                            }

                            for (int i=0;i<jsonpromotionServices.length();i++) {
                                String promo_idfix = jsonpromotionServices.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_IDFIX);
                                String promo_idservice = jsonpromotionServices.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_IDSERVICE);
                                String promo_service = jsonpromotionServices.getJSONObject(i).getString(Config.KEY_MASTER_PROMO_SERVICE);

                                db3.insertData(new TablePromotionServices(),promo_idfix,promo_idservice,promo_service);
                            }


                            SharedPreferences settings = getActivity().getSharedPreferences(DataMaster.PREF_DATA_MASTER, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putBoolean(PREF_PROMO, true);
                            editor.apply();
                        }


                    }


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*if (message != null) {
                return message;
            }*/
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (status != null && status.equals("true")) {
                swipeRefreshLayout.setRefreshing(false);
                showList();
            } else {
                swipeRefreshLayout.setRefreshing(false);
                if (isAdded()){
                    utils.showErrorDlg(handler, getResources().getString(R.string.network_error), getContext());
                }
            }
        }
    }
}
