package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableDataMSDTDFailed;
import myskysfa.com.sfa.database.TableMasterPromo;
import myskysfa.com.sfa.database.TablePromotionFix;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 11/1/2016.
 */

public class TablePromotionFixAdapter {
    static private TablePromotionFixAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TablePromotionFixAdapter(ctx);
        }
    }

    static public TablePromotionFixAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TablePromotionFixAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TablePromotionFix> getAllData() {
        List<TablePromotionFix> tblsatu = null;
        try {
            tblsatu = getHelper().getPromotionFixDao().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TablePromotionFix> getDatabyCondition(String condition, Object param) {
        List<TablePromotionFix> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TablePromotionFix.class);
            QueryBuilder<TablePromotionFix, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePromotionFix, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TablePromotionFix tbl, String id_promo, String id_fix,
                           String disc, String amount, String status, String promo_target) {
        try {
            tbl.setId_promo(id_promo);
            tbl.setId_fix(id_fix);
            tbl.setDisc(disc);
            tbl.setAmount(amount);
            tbl.setStatus(status);
            tbl.setPromo_target(promo_target);
            getHelper().getPromotionFixDao().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePromotionFix.class);
            DeleteBuilder<TablePromotionFix, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            Dao dao = helper.getDao(TablePromotionFix.class);
            DeleteBuilder<TablePromotionFix, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
