package myskysfa.com.sfa.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import myskysfa.com.sfa.R;


/**
 * Created by Eno on 6/14/2016.
 */
public class StatusMSAdapter extends RecyclerView.Adapter<StatusMSAdapter.ViewHolder> {
    private static final int TYPE_CATEGORY = 1;
    private static final int TYPE_CONTENT = 2;
    private ArrayList<String> list;

    public StatusMSAdapter(ArrayList<String> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CATEGORY:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.status_tagging, parent, false));
            case TYPE_CONTENT:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_status, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vStatus;
        public TextView vKeterangan;
        public ImageView vFlag;
        public TextView vTitle;
        public LinearLayout vBorder;

        public ViewHolder(View itemView) {
            super(itemView);
            vStatus = (TextView) itemView.findViewById(R.id.txtStatus);
            vKeterangan = (TextView) itemView.findViewById(R.id.vKeterangan);
            vFlag = (ImageView) itemView.findViewById(R.id.imageFlag);
            vTitle = (TextView) itemView.findViewById(R.id.ststitle);
            vBorder = (LinearLayout) itemView.findViewById(R.id.border);

        }
    }
}
