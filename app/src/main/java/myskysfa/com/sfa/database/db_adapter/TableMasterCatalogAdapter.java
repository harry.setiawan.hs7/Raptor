package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableFreeTrialSurvayList;
import myskysfa.com.sfa.database.TableMasterCatalog;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 6/20/2016.
 */
public class TableMasterCatalogAdapter {
    static private TableMasterCatalogAdapter instance;
    private DatabaseManager helper;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMasterCatalogAdapter(ctx);
        }
    }

    static public TableMasterCatalogAdapter getInstance() {
        return instance;
    }

    public TableMasterCatalogAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }


    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMasterCatalog> getAllData() {
        List<TableMasterCatalog> tblData = null;
        try {
            tblData = getHelper().getTableCatalogDAO()
                    .queryBuilder().orderBy(TableMasterCatalog.fROWID,
                            false)
                    .orderBy(TableMasterCatalog.fROWID, false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblData;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMasterCatalog tbl, String id, String  category, String code,
                           String description) {
        try {
            tbl.setId(id);
            tbl.setCategory(category);
            tbl.setCode(code);
            tbl.setDescription(description);
            getHelper().getTableCatalogDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableMasterCatalog> tbl = null;
        try {
            tbl = getHelper().getTableCatalogDAO().queryForAll();
            getHelper().getTableCatalogDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMasterCatalog> getDatabyCondition(String condition, Object param) {
        List<TableMasterCatalog> tbl = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = helper.getDao(TableFreeTrialSurvayList.class);
            QueryBuilder<TableMasterCatalog, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterCatalog, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableMasterCatalog.fROWID, false);
            count++;

            if (count > 0) {
                tbl = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tbl;
    }
}
