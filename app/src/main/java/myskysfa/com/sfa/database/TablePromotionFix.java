package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Eno on 11/1/2016.
 */

@DatabaseTable(tableName = "promotionfix")
public class TablePromotionFix {

    public static final String ID = "id";
    public static final String ID_PROMO = "id_promo";
    public static final String ID_FIX = "id_fix";
    public static final String DISC = "disc";
    public static final String AMOUNT = "amount";
    public static final String STATUS = "status";
    public static final String PROMO_TARGET = "promo_target";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField()
    private String id_promo;
    @DatabaseField()
    private String id_fix;
    @DatabaseField()
    private String disc;
    @DatabaseField()
    private String amount;
    @DatabaseField()
    private String status;
    @DatabaseField()
    private String promo_target;

    public String getId_promo() {
        return id_promo;
    }

    public void setId_promo(String id_promo) {
        this.id_promo = id_promo;
    }

    public String getId_fix() {
        return id_fix;
    }

    public void setId_fix(String id_fix) {
        this.id_fix = id_fix;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromo_target() {
        return promo_target;
    }

    public void setPromo_target(String promo_target) {
        this.promo_target = promo_target;
    }
}
