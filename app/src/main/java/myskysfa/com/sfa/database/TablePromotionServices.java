package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Eno on 11/1/2016.
 */

@DatabaseTable(tableName = "promo_service")
public class TablePromotionServices {

    public static final String ID = "id";
    public static final String ID_FIX = "id_fix";
    public static final String ID_SERVICE = "id_service";
    public static final String SERVICE = "service";


    @DatabaseField(id = true)
    private String id;

    @DatabaseField()
    private String id_fix;
    @DatabaseField()
    private String id_service;
    @DatabaseField()
    private String service;

    public String getId_fix() {
        return id_fix;
    }

    public void setId_fix(String id_fix) {
        this.id_fix = id_fix;
    }

    public String getId_service() {
        return id_service;
    }

    public void setId_service(String id_service) {
        this.id_service = id_service;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

}
