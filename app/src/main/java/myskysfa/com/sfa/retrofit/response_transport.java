package myskysfa.com.sfa.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by harrysetiawan on 14/09/17.
 */

public class response_transport {
    @SerializedName("status")
    boolean status;
    @SerializedName("data")
    transport data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public transport getData() {
        return data;
    }

    public void setData(transport data) {
        this.data = data;
    }
}