package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/17/2016.
 */
@DatabaseTable(tableName = "M_ZIPCODE")
public class TableZipcode {

    public static final String TABLE_NAME = "kodepos";
    public static final String fROWID = "ID";
    public static final String fZIPCODE = "ZIPCODE";
    public static final String fSTREET_CODE = "STREET_CODE";
    public static final String fSTREET_NAME = "STREET_NAME";
    public static final String fAREA_CODE = "AREA_CODE";
    public static final String fAREA_NAME = "AREA_NAME";
    public static final String fCITY_CODE = "CITY_CODE";
    public static final String fCITY_NAME = "CITY_NAME";
    public static final String fDISTRICT_CODE = "DISTRICT_CODE";
    public static final String fDISTRICT_NAME = "DISTRICT_NAME";
    public static final String fSTATE_CODE = "STATE_CODE";
    public static final String fSTATE_NAME = "STATE_NAME";

    @DatabaseField(generatedId = true)
    private int ID;

    @DatabaseField
    private String ZIPCODE, STREET_CODE, STREET_NAME, AREA_CODE, AREA_NAME, CITY_CODE, CITY_NAME,
            DISTRICT_CODE, DISTRICT_NAME, STATE_CODE, STATE_NAME;

    public TableZipcode() {}

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setAREA_CODE(String AREA_CODE) {
        this.AREA_CODE = AREA_CODE;
    }

    public String getAREA_CODE() {
        return AREA_CODE;
    }

    public void setAREA_NAME(String AREA_NAME) {
        this.AREA_NAME = AREA_NAME;
    }

    public String getAREA_NAME() {
        return AREA_NAME;
    }

    public void setCITY_CODE(String CITY_CODE) {
        this.CITY_CODE = CITY_CODE;
    }

    public String getCITY_CODE() {
        return CITY_CODE;
    }

    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }

    public String getCITY_NAME() {
        return CITY_NAME;
    }

    public void setDISTRICT_CODE(String DISTRICT_CODE) {
        this.DISTRICT_CODE = DISTRICT_CODE;
    }

    public String getDISTRICT_CODE() {
        return DISTRICT_CODE;
    }

    public void setDISTRICT_NAME(String DISTRICT_NAME) {
        this.DISTRICT_NAME = DISTRICT_NAME;
    }

    public String getDISTRICT_NAME() {
        return DISTRICT_NAME;
    }

    public void setSTATE_CODE(String STATE_CODE) {
        this.STATE_CODE = STATE_CODE;
    }

    public String getSTATE_CODE() {
        return STATE_CODE;
    }

    public void setSTATE_NAME(String STATE_NAME) {
        this.STATE_NAME = STATE_NAME;
    }

    public String getSTATE_NAME() {
        return STATE_NAME;
    }

    public void setSTREET_CODE(String STREET_CODE) {
        this.STREET_CODE = STREET_CODE;
    }

    public String getSTREET_CODE() {
        return STREET_CODE;
    }

    public void setSTREET_NAME(String STREET_NAME) {
        this.STREET_NAME = STREET_NAME;
    }

    public String getSTREET_NAME() {
        return STREET_NAME;
    }

    public void setZIPCODE(String ZIPCODE) {
        this.ZIPCODE = ZIPCODE;
    }

    public String getZIPCODE() {
        return ZIPCODE;
    }

}
