package myskysfa.com.sfa.adapter.master;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableMasterCatalog;

/**
 * Created by Eno on 6/20/2016.
 */
public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.MainViewHolder> {
    View itemLayoutView;
    ViewGroup mParent;
    private List<TableMasterCatalog> itemsData;


    public CatalogAdapter(List<TableMasterCatalog> itemsData) {
        this.itemsData = itemsData;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.master_item_catalog_content, parent, false);
        MainViewHolder viewHolder = new MainViewHolder(itemLayoutView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        TableMasterCatalog cn = itemsData.get(position);

        holder.txtCategory.setText(cn.getCategory());
        holder.txtIcon.setText(cn.getDescription().substring(0, 1));
        holder.txtTitle.setText(cn.getDescription());
    }


    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {
        public TextView txtCategory;
        public TextView txtIcon;
        public TextView txtTitle;

        public MainViewHolder(View itemView) {
            super(itemView);
            txtCategory = (TextView) itemView.findViewById(R.id.master_catalog_item_cat);
            txtIcon = (TextView) itemView.findViewById(R.id.master_catalog_item_icon);
            txtTitle = (TextView) itemView.findViewById(R.id.master_catalog_item_title);
        }
    }

    /*public class CategoryViewHolder extends MainViewHolder{
        TextView txtCategory,txtIcon,txtTitle;

        public CategoryViewHolder(View v){
            super(v);
            this.txtCategory = (TextView) v.findViewById(R.id.master_catalog_item_category);
            this.txtIcon = (TextView) v.findViewById(R.id.master_catalog_item_icon);
            this.txtTitle = (TextView) v.findViewById(R.id.master_catalog_item_title);
        }
    }*/

}
