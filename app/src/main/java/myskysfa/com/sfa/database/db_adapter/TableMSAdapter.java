package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TableMasterMS;
import myskysfa.com.sfa.database.TableMasterMaterial;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 6/20/2016.
 */
public class TableMSAdapter {
    static private TableMSAdapter instance;
    private Dao dao;
    private DatabaseManager helper;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMSAdapter(ctx);
        }
    }

    static public TableMSAdapter getInstance() {
        return instance;
    }

    public TableMSAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }


    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMasterMS> getAllData() {
        List<TableMasterMS> tblData = null;
        try {
            tblData = getHelper().getTableMSDAO()
                    .queryBuilder()
                    .orderBy(TableMasterMS.FLD_NAME, true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblData;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableMasterMS> getDatabyCondition(String condition, Object param) {
        List<TableMasterMS> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableMasterMS.class);
            QueryBuilder<TableMasterMS, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterMS, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy(TableMasterMS.FLD_ROWID, false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMasterMS tbl, String id, String  name, String address,
                           String post_code, String latitude, String longitude,
                           String date_start, String date_end, String id_pos, String ms_code,
                           String entity_id) {
        try {
            tbl.setId(id);
            tbl.setName(name);
            tbl.setAddress(address);
            tbl.setPost_code(post_code);
            tbl.setLatitude(latitude);
            tbl.setLongitude(longitude);
            tbl.setDate_start(date_start);
            tbl.setDate_end(date_end);
            tbl.setId_pos(id_pos);
            tbl.setMs_code(ms_code);
            tbl.setEntity(entity_id);
            getHelper().getTableMSDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            Dao dao = helper.getDao(TableMasterMS.class);
            DeleteBuilder<TableMasterMS, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
