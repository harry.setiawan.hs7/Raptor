package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/13/2016.
 */
@DatabaseTable(tableName = "plan_task")
public class TablePlan {

    public static final String TABLE_NAME = "plan_task";
    public static final String KEY_PLAN_ID = "plan_id";
    public static final String KEY_PLAN_DATE = "plan_date";
    public static final String KEY_ZIPCODE = "zip_code";
    public static final String KEY_AREA = "area";
    public static final String KEY_TS_CODE = "ts_code";
    public static final String KEY_TARGET = "target";
    public static final String KEY_SFL_CODE = "sfl_code";
    public static final String KEY_TS_NAME = "ts_name";
    public static final String KEY_RESULT = "result";
    public static final String KEY_VALUE = "value";
    public static final String KEY_STATUS = "status";

    @DatabaseField(id = true)
    private String plan_id;

    @DatabaseField
    private String plan_date;
    @DatabaseField
    private String zip_code;
    @DatabaseField
    private String area;
    @DatabaseField
    private String ts_code;
    @DatabaseField
    private String ts_name;
    @DatabaseField
    private String target;
    @DatabaseField
    private String result;
    @DatabaseField
    private String sfl_code;
    @DatabaseField
    private String value;
    @DatabaseField
    private String status;


    public TablePlan() {
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSfl_code(String sfl_code) {
        this.sfl_code = sfl_code;
    }

    public String getSfl_code() {
        return sfl_code;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setPlan_date(String plan_date) {
        this.plan_date = plan_date;
    }

    public String getPlan_date() {
        return plan_date;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTarget() {
        return target;
    }

    public void setTs_code(String ts_code) {
        this.ts_code = ts_code;
    }

    public String getTs_code() {
        return ts_code;
    }

    public void setTs_name(String ts_name) {
        this.ts_name = ts_name;
    }

    public String getTs_name() {
        return ts_name;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getZip_code() {
        return zip_code;
    }
}
