package myskysfa.com.sfa.main.menu;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.adapter.DtdChecklistAdapter;
import myskysfa.com.sfa.adapter.DtdChecklistAdapterPickup;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.Utils;
import myskysfa.com.sfa.utils.listItemObject.CheckList;
import myskysfa.com.sfa.utils.listItemObject.CheckListPickup;


public class MSDTD_Checklist extends Fragment {
    private static Context _contex;
    private ViewGroup root;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView, recyclerViewPickup;
    private DtdChecklistAdapter adaper;
    private DtdChecklistAdapterPickup adaperPickup;
    private ArrayList<CheckList> checkList;
    private ArrayList<CheckListPickup> checkListPickups;
    private String username, token, sflCode, url, imei, ftId;
    private int offset = 0;
    private boolean isTaskRunning = false;
    private Handler handler;
    private boolean status = false;
    JSONArray jsonArray;
    Boolean isInternetPresent = false;
    private TableFormAppAdapter mFormAppAdapter;
    //    private List<TableChecklist> listChecklist;
    //    private TableChecklistAdapter mChecklistAdapter;
    private Button saveBTN;
    private Utils utils;
    private String uId, numberForm;
    private String _formNo;
    private TextView checklistLabel;
    private int signal;
    private SharedPreferences sessionPref;
    private String response;
    private Long count;

    public static MSDTD_Checklist newInstance(Context contex) {
        _contex = contex;
        MSDTD_Checklist fragment = new MSDTD_Checklist();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_dtd__checklist, container, false);
        _contex = getActivity();
        ConstructorInit();
        initView(root);
        setFormNumber();
        mFormAppAdapter = new TableFormAppAdapter(getActivity());
        sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        Refresh();
        recyclerView.setNestedScrollingEnabled(false);
        recyclerViewPickup.setNestedScrollingEnabled(false);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new LoadDataDTDCCHECKLIST().execute();
            }
        });

        saveBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jResult = new JSONObject();
                JSONArray jArray = new JSONArray();

                //Create Json Checklist
                for (int i = 0; i < checkList.size(); i++) {
                    JSONObject jGroup = new JSONObject();
                    try {
                        jGroup.put("c_id", checkList.get(i).getID_CHECKLIST());
                        jGroup.put("c_value", checkList.get(i).getCHECKLIST_LABEL_EDIT());
                        if (checkList.get(i).getCHECKLIST_STATUS().equalsIgnoreCase("1")) {
                            jArray.put(jGroup);
                            jResult.put("data", jArray);
                        }
                        Log.d("checklistku", jResult.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //Create Json Pickup

                for (int i = 0; i < checkListPickups.size(); i++) {
                    JSONObject jGroup = new JSONObject();
                    try {
                        jGroup.put("c_id", checkListPickups.get(i).getID_CHECKLIST());
                        jGroup.put("c_value", checkListPickups.get(i).getCHECKLIST_LABEL_EDIT());
                        if (checkListPickups.get(i).getCHECKLIST_STATUS().equalsIgnoreCase("1")) {
                            jArray.put(jGroup);
                            jResult.put("data", jArray);
                        }
                        Log.d("checklistku", jResult.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                count = mFormAppAdapter.getDataCount(TableFormApp.fFORM_NO, numberForm);
                if (count == 0) {
                    Toast.makeText(getActivity(), "Harap isi profile", Toast.LENGTH_SHORT).show();
                } else {
                    mFormAppAdapter.updatePartial(getActivity(), TableFormApp.fVALUES_CHECKLIST,
                            jResult.toString(), TableFormApp.fFORM_NO, numberForm);

                    Toast.makeText(getActivity(), "Checklist Saved", Toast.LENGTH_SHORT).show();
                }


            }
        });
        return root;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    private void ConstructorInit() {
        utils = new Utils(getActivity());
        handler = new Handler();

    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), root);
    }

    private void initView(ViewGroup root) {
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.swipe_container);
        checklistLabel = (TextView) root.findViewById(R.id.checklistLabel);
        recyclerView = (RecyclerView) root.findViewById(R.id.list);
        recyclerViewPickup = (RecyclerView) root.findViewById(R.id.list2);
        saveBTN = (Button) root.findViewById(R.id.save);
        saveBTN.setVisibility(View.GONE);
    }

    private void Refresh() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                new LoadDataDTDCCHECKLIST().execute();
            }
        });
    }

    public void setFormNumber() {
        Bundle bundle = getArguments();
        String catApp = bundle.getString(getString(R.string.category_app));
        Log.d("catApp", catApp);

        assert catApp != null;
        if (catApp.equalsIgnoreCase("dtd")) {
            SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_dtd),
                    Context.MODE_PRIVATE);
            numberForm = sm.getString("fn", null);
        } else {
            SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.fn_ms),
                    Context.MODE_PRIVATE);
            numberForm = sm.getString("fn", null);
        }

    }


    private class LoadDataDTDCCHECKLIST extends AsyncTask<String, String, ArrayList<CheckList>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isTaskRunning) {
                this.cancel(true);
                return;
            }
            signal = utils.checkSignal(getActivity());

        }

        @Override
        protected ArrayList<CheckList> doInBackground(String... params) {
            try {
                isTaskRunning = true;
                checkList = new ArrayList<CheckList>();
                checkListPickups = new ArrayList<CheckListPickup>();
                if (sessionPref == null) {
                } else {
                    sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                    username = sessionPref.getString(TableLogLogin.C_USER_NAME, "");
                    token = sessionPref.getString(TableLogLogin.C_TOKEN, "");
                    offset = 0;
                    imei = utils.getIMEI();
                    checkList.clear();
                    checkListPickups.clear();
                    url = ConnectionManager.CM_URL_LIST_CHECKLIST;
                    response = ConnectionManager.requestListChecklist(url, getActivity());
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject != null) {
                        jsonArray = jsonObject.getJSONArray(Config.KEY_DATA);
//                        if (status) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            String id_checklist = obj.getString("ms_checklist_id");
                            String label_checklist = obj.getString("name");
                            String edit_checklist = obj.getString("description");
                            String status_checklist = obj.getString("is_active");
                            String flag = obj.getString("flag");
                            if (flag.equalsIgnoreCase("C")) {
                                CheckList item = new CheckList(_contex, id_checklist, label_checklist,
                                        edit_checklist, status_checklist);
                                checkList.add(item);
                            } else {
                                CheckListPickup pickupItem = new CheckListPickup(_contex, id_checklist, label_checklist,
                                        edit_checklist, status_checklist, flag);
                                checkListPickups.add(pickupItem);
                            }
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error2), getActivity());
                    }
                });
            }
            return checkList;
        }

        @Override
        protected void onPostExecute(ArrayList<CheckList> checkLists) {
            super.onPostExecute(checkLists);
            try {
                if (checkLists != null) {
                    ShowList();
                    if (checkListPickups != null){
                        ShowListPicup();
                    }
                    isTaskRunning = false;
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error2), getActivity());
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void ShowList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutParams);
        adaper = new DtdChecklistAdapter(checkList);
        recyclerView.setAdapter(adaper);
        saveBTN.setVisibility(View.VISIBLE);
    }

    private void ShowListPicup() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        recyclerViewPickup.setLayoutManager(layoutParams);
        adaperPickup = new DtdChecklistAdapterPickup(checkListPickups);
        recyclerViewPickup.setAdapter(adaperPickup);
        saveBTN.setVisibility(View.VISIBLE);
    }

}
