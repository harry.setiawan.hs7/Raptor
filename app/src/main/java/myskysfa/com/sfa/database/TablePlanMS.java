package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/13/2016.
 */
@DatabaseTable(tableName = "plan_ms")
public class TablePlanMS {

    public static final String TABLE_NAME = "plan_ms";
    public static final String KEY_ASSIGNMENT_ID = "assignment_id";
    public static final String KEY_PLAN_DATE = "assignment_date";
    public static final String KEY_SFL_CODE = "sfl_code";
    public static final String KEY_TS_NAME = "ts_name";
    public static final String KEY_ENTITY_ID = "entity_id";
    public static final String KEY_VALUE = "value";
    public static final String KEY_STATUS = "status";

    @DatabaseField(id = true)
    private String assignment_id;
    @DatabaseField
    private String assignment_date;
    @DatabaseField
    private String ts_name;
    @DatabaseField
    private String sfl_code;
    @DatabaseField
    private String value;
    @DatabaseField
    private String status;
    @DatabaseField
    private String entity_id;

    public String getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }


    public String getAssignment_id() {
        return assignment_id;
    }

    public void setAssignment_id(String assignment_id) {
        this.assignment_id = assignment_id;
    }

    public String getAssignment_date() {
        return assignment_date;
    }

    public void setAssignment_date(String assignment_date) {
        this.assignment_date = assignment_date;
    }

    public String getTs_name() {
        return ts_name;
    }

    public void setTs_name(String ts_name) {
        this.ts_name = ts_name;
    }

    public String getSfl_code() {
        return sfl_code;
    }

    public void setSfl_code(String sfl_code) {
        this.sfl_code = sfl_code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public TablePlanMS() {
    }

}
