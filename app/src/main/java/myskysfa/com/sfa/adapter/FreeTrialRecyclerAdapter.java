package myskysfa.com.sfa.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFreeTrial;

/**
 * Created by admin on 6/13/2016.
 */
public class FreeTrialRecyclerAdapter extends RecyclerView.Adapter<FreeTrialRecyclerAdapter.ViewHolder>{

    View itemLayoutView;
    private List<TableFreeTrial> itemsData;
    Activity activity;

    public FreeTrialRecyclerAdapter(Activity activity, List<TableFreeTrial> itemsData) {
        this.itemsData = itemsData;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.free_trial_list_item, parent,false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final TableFreeTrial item = itemsData.get(position);
        if (item!=null) {
            holder.ftId.setText(item.getFt_id());
            if (item.getStatus().equalsIgnoreCase("1")) {
                holder.statusDesc.setText("New");
            } else if (item.getStatus().equalsIgnoreCase("2")) {
                holder.statusDesc.setText("FT");
            } else {
                holder.statusDesc.setText("");
            }
            holder.productName.setText(item.getProduct_name());
            holder.area.setText(item.getAddress());
            holder.planDate.setText(item.getCreated_date());
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView ftId;
        public TextView planDate;
        public TextView statusDesc;
        public TextView productName;
        public TextView area;
        public ViewHolder(View itemView) {
            super(itemView);
            ftId = (TextView) itemView.findViewById(R.id.ft_id);
            planDate = (TextView) itemView.findViewById(R.id.ft_date);
            statusDesc = (TextView) itemView.findViewById(R.id.ft_status);
            productName = (TextView) itemView.findViewById(R.id.ft_product);
            area = (TextView) itemView.findViewById(R.id.ft_address);
        }
    }

    @Override
    public int getItemCount() {
        if (itemsData==null) {
            return 0;
        } else {
            return itemsData.size();
        }
    }

}
