package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_material")
public class TableMasterMaterial {
    public static final String KEY_ID = "id";
    public static final String KEY_CODE = "code";
    public static final String KEY_DESC = "description";
    public static final String KEY_RATE = "rate";
    public static final String KEY_CC = "class_code";
    public static final String KEY_IP = "is_package";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String code, description, rate, class_code, is_package;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getClass_code() {
        return class_code;
    }

    public void setClass_code(String class_code) {
        this.class_code = class_code;
    }

    public String getIs_package() {
        return is_package;
    }

    public void setIs_package(String is_package) {
        this.is_package = is_package;
    }


}
