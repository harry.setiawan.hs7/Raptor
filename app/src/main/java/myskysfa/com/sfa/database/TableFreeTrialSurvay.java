package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/14/2016.
 */
@DatabaseTable(tableName = "free_trial_survay")
public class TableFreeTrialSurvay {

    public static final String TABLE_NAME       = "free_trial_survay";
    public static final String KEY_SV_ID        = "sv_id";
    public static final String KEY_PROSPECT_ID  = "prospect_id";
    public static final String KEY_SV_PROFILE   = "sv_profile";
    public static final String KEY_SV_ALAMAT    = "sv_address";
    public static final String KEY_SV_TLP       = "sv_tlp";
    public static final String KEY_SV_PHOTO     = "sv_photo";
    public static final String KEY_STATUS       = "sv_status";
    public static final String KEY_LONGITUDE    = "sv_longitude";
    public static final String KEY_LANGITUDE    = "sv_latitude";
    public static final String KEY_VALUE        = "sv_value";
    public static final String KEY_VISIT_DATE   = "sv_visit_date";
    public static final String KEY_SUBMIT_DATE  = "sv_submit_date";
    public static final String KEY_INTERESTED   = "sv_interested";
    public static final String KEY_MEETING_TIME = "sv_meet_time";
    public static final String KEY_MEET         = "sv_meet";
    public static final String KEY_NOTE         = "sv_note";

    @DatabaseField(id = true)
    private String sv_id;

    @DatabaseField
    private String prospect_id, sv_profile, sv_address, sv_tlp, sv_longitude, sv_latitude, sv_interested, sv_meet,
            sv_visit_date, sv_submit_date, sv_note, sv_photo, sv_status, sv_value;

    public TableFreeTrialSurvay() {}

    public void setSv_value(String sv_value) {
        this.sv_value = sv_value;
    }

    public String getSv_visit_date() {
        return sv_visit_date;
    }

    public void setProspect_id(String prospect_id) {
        this.prospect_id = prospect_id;
    }

    public String getProspect_id() {
        return prospect_id;
    }

    public void setSv_address(String sv_address) {
        this.sv_address = sv_address;
    }

    public String getSv_address() {
        return sv_address;
    }

    public void setSv_id(String sv_id) {
        this.sv_id = sv_id;
    }

    public String getSv_id() {
        return sv_id;
    }

    public void setSv_interested(String sv_interested) {
        this.sv_interested = sv_interested;
    }

    public String getSv_interested() {
        return sv_interested;
    }

    public void setSv_latitude(String sv_latitude) {
        this.sv_latitude = sv_latitude;
    }

    public String getSv_latitude() {
        return sv_latitude;
    }

    public void setSv_longitude(String sv_longitude) {
        this.sv_longitude = sv_longitude;
    }

    public String getSv_longitude() {
        return sv_longitude;
    }

    public void setSv_meet(String sv_meet) {
        this.sv_meet = sv_meet;
    }

    public String getSv_meet() {
        return sv_meet;
    }

    public void setSv_note(String sv_note) {
        this.sv_note = sv_note;
    }

    public String getSv_note() {
        return sv_note;
    }

    public void setSv_photo(String sv_photo) {
        this.sv_photo = sv_photo;
    }

    public String getSv_photo() {
        return sv_photo;
    }

    public void setSv_profile(String sv_profile) {
        this.sv_profile = sv_profile;
    }

    public String getSv_profile() {
        return sv_profile;
    }

    public void setSv_status(String sv_status) {
        this.sv_status = sv_status;
    }

    public String getSv_status() {
        return sv_status;
    }

    public void setSv_submit_date(String sv_submit_date) {
        this.sv_submit_date = sv_submit_date;
    }

    public String getSv_submit_date() {
        return sv_submit_date;
    }

    public void setSv_tlp(String sv_tlp) {
        this.sv_tlp = sv_tlp;
    }

    public String getSv_tlp() {
        return sv_tlp;
    }

    public void setSv_visit_date(String sv_visit_date) {
        this.sv_visit_date = sv_visit_date;
    }

    public String getSv_value() {
        return sv_value;
    }
}
