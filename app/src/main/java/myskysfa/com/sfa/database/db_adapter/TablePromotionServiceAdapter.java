package myskysfa.com.sfa.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.sfa.database.TablePromotionFix;
import myskysfa.com.sfa.database.TablePromotionServices;
import myskysfa.com.sfa.utils.DatabaseManager;

/**
 * Created by Eno on 11/1/2016.
 */

public class TablePromotionServiceAdapter {
    static private TablePromotionServiceAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TablePromotionServiceAdapter(ctx);
        }
    }

    static public TablePromotionServiceAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TablePromotionServiceAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TablePromotionServices> getAllData() {
        List<TablePromotionServices> tblsatu = null;
        try {
            tblsatu = getHelper().getPromotionServiceDao().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TablePromotionServices> getDatabyCondition(String condition, Object param) {
        List<TablePromotionServices> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TablePromotionServices.class);
            QueryBuilder<TablePromotionServices, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePromotionServices, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TablePromotionServices tbl, String id_fix, String id_service,
                           String service) {
        try {
            tbl.setId_fix(id_fix);
            tbl.setId_service(id_service);
            tbl.setService(service);
            getHelper().getPromotionServiceDao().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePromotionServices.class);
            DeleteBuilder<TablePromotionServices, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        try {
            Dao dao = helper.getDao(TablePromotionServices.class);
            DeleteBuilder<TablePromotionServices, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
