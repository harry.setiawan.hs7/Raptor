package myskysfa.com.sfa.main.menu;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFormApp;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.sfa.main.menu.dtd.DTD_Signature;
import myskysfa.com.sfa.main.menu.dtdproject.ProSign;
import myskysfa.com.sfa.main.menu.ms.MS_Signature;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.widget.SignatureView;

/**
 * Created by Hari Hendryan on 11/2/2015.
 */
public class AgreementActivity extends AppCompatActivity {
    private Context context;
    private WebView webView;
    private String type, numberForm, uId, appType;
    private static String imagePathSign;
    private String url, cust;
    private static Bitmap bitmap;
    private JavaScriptClickSignatureCustomer signatureCustomer;
    private SignatureView signatureView;
    private Button ok, clear;
    private ImageView closer;
    private DTD_Signature dtd_signature;
    private MS_Signature ms_signature;
    private ProSign sign_bawa_barang;

    private List<TableFormApp> listFormApp;
    private TableFormAppAdapter adapter;

    @SuppressLint("AddJavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        setContentView(R.layout.ms_agreement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        type = getIntent().getStringExtra(getResources().getString(R.string.type));
        appType = getIntent().getStringExtra("appType");
        numberForm = getIntent().getStringExtra("numberForm");

        signatureCustomer = new JavaScriptClickSignatureCustomer(this);
        webView = (WebView) findViewById(R.id.wv_agreement);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
                new AlertDialog.Builder(AgreementActivity.this)
                        .setTitle("Warning!")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok,
                                new AlertDialog.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        result.confirm();
                                    }
                                })
                        .setCancelable(false)
                        .create()
                        .show();

                return true;
            }

            ;
        });

        webView.requestFocus(View.FOCUS_DOWN);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSettings.setAllowUniversalAccessFromFileURLs(true);
            webSettings.setAllowFileAccessFromFileURLs(true);
        }
        if (type.equalsIgnoreCase(getResources().getString(R.string.brand_type_indovision))) {
            url = "html/aggrement.html";
            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_agreement) +
                    " " + getResources().getString(R.string.brand_type_indovision));
        } else if (type.equalsIgnoreCase(getResources().getString(R.string.brand_type_okevision))) {
            url = "html/aggrement_OkeVision.html";

            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_agreement) +
                    " " + getResources().getString(R.string.brand_type_okevision));
        } else if (type.equalsIgnoreCase(getResources().getString(R.string.brand_type_top_tv))) {
            url = "html/aggrement_TOPTV.html";

            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_agreement) +
                    " " + getResources().getString(R.string.brand_type_top_tv));
        } else {
            url = "html/aggrement.html";

            getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_agreement));
        }

        webView.loadUrl("file:///android_asset/" + url);
        webView.setWebViewClient(new MyWebViewClient());

        webView.addJavascriptInterface(signatureCustomer, "signatureCustomer");
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            try {
                getDataSales();
                if (type.equalsIgnoreCase(getResources().getString(R.string.brand_type_indovision))) {
                    webView.loadUrl("javascript:OnAttachInfo(\"" + cust + "\")");
                } else if (type.equalsIgnoreCase(getResources().getString(R.string.brand_type_okevision))) {
                    webView.loadUrl("javascript:OnAttachInfo(\"" + cust + "\")");
                } else if (type.equalsIgnoreCase(getResources().getString(R.string.brand_type_top_tv))) {
                    webView.loadUrl("javascript:OnAttachInfo(\"" + cust + "\")");
                } else {
                    webView.loadUrl("javascript:OnAttachInfo(\"" + cust + "\")");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            Log.e("Error VAGARO", error.toString());
            handler.proceed();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("http")) {
                view.loadUrl(url);
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        if (appType != null){
            switch (appType) {
                case "ms":
                    ms_signature = new MS_Signature();
                    ms_signature.decodeFile(imagePathSign, "aggrement");
                    break;
                case "dtd":
                    dtd_signature = new DTD_Signature();
                    dtd_signature.decodeFile(imagePathSign, "aggrement");
                    break;
                case "dtdbb":
                    sign_bawa_barang = new ProSign();
                    sign_bawa_barang.decodeFile(imagePathSign, "aggrement");
                    break;
            }
        }
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }*/


    public void decodeFile(final String filePath) {
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        bitmap = BitmapFactory.decodeFile(filePath, o2);

    }

    /* Ag*/

    public class JavaScriptClickSignatureCustomer {
        Context context;

        public JavaScriptClickSignatureCustomer(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void performClickCustomer(String webMessage) {
            SignatureClick("aggrement");
        }


        private void SignatureClick(final String name) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_sign);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            closer = (ImageView) dialog.findViewById(R.id.imageView_close);
            clear = (Button) dialog.findViewById(R.id.clear);
            ok = (Button) dialog.findViewById(R.id.ok);
            signatureView = (SignatureView) dialog.findViewById(R.id.signature);
            closer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (signatureView.isClear()) {
                        Toast.makeText(context, "Silahkan tanda tangan!", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        StringBuilder fileName = new StringBuilder();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date now = new Date();
                        String strDate = sdf.format(now);
                        String yy = strDate.substring(2, 4);
                        String mm = strDate.substring(5, 7);
                        String dd = strDate.substring(8, 10);
                        if (!Environment.getExternalStorageState().equals(
                                Environment.MEDIA_MOUNTED)) {
                            Toast.makeText(context, "Error! No SDCARD Found!", Toast.LENGTH_SHORT)
                                    .show();
                        } else {
                            if (uId == null) {
                                uId = "";
                            }
                            fileName.append(numberForm + "_");
                            fileName.append(name);
                            fileName.append("_");
                            fileName.append("SignatureAgree_");
                            fileName.append(yy);
                            fileName.append(mm);
                            fileName.append(dd);
                            fileName.append(".jpg");
                            String _fileNameSign = fileName.toString();
                            String path = Environment.getExternalStorageDirectory() + File.separator + Config.path;
                            signatureView.exportFile(path, _fileNameSign);
                            dialog.dismiss();
                            //Toast.makeText(context, "Gambar tersimpan", Toast.LENGTH_SHORT).show();
                            imagePathSign = path + File.separator + fileName.toString();
                            decodeFile(imagePathSign);
                            bitmap = BitmapFactory.decodeFile(imagePathSign);
                            InputStream is = null;
                            try {
                                Log.e("url", url);
                                is = getAssets().open(url);
                                final Document doc = Jsoup.parse(is, "UTF-8", url);
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                if (bitmap != null) {
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                                    String imgageBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                                    String image = "data:image/png;base64," + imgageBase64;
                                    doc.getElementsByClass("ttdPelanggan").attr("src", image);
                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        webView.loadDataWithBaseURL(null, doc.toString(), "text/html", "utf-8", null);
                                    }
                                });

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            });


            clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signatureView.clear();
                }
            });
            dialog.show();
        }

    }

    private void getDataSales() throws JSONException {
        listFormApp = new ArrayList<>();
        adapter = new TableFormAppAdapter(AgreementActivity.this);
        listFormApp = adapter.getDatabyCondition(TableFormApp.fFORM_NO, numberForm);
        cust = listFormApp.get(0).getFIRST_NAME();
    }


}
