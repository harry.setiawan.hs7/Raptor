package myskysfa.com.sfa.utils.listItemObject;

import android.content.Context;

/**
 * Created by Hari Hendryan on 11/9/2015.
 */
public class EmergencyListItem {
    private Context context;
    private String transNo;
    private String formNo;
    private String firstName;
    private String middleName;
    private String lastName;
    private String relationship;
    private String telephone;
    private String address;

    public EmergencyListItem(Context context) {
        this.context = context;
    }

    public void setTransNo(String transNo) {
        this.transNo = transNo;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setFormNo(String formNo) {
        this.formNo = formNo;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getTransNo() {
        return transNo;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFormNo() {
        return formNo;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getRelationship() {
        return relationship;
    }
}
