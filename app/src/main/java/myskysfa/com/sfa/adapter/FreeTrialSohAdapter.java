package myskysfa.com.sfa.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableFreeTrialCoh;

/**
 * Created by admin on 6/14/2016.
 */
public class FreeTrialSohAdapter extends RecyclerView.Adapter<FreeTrialSohAdapter.ViewHolder> {

    private List<TableFreeTrialCoh> itemsData;
    private ViewGroup mViewGroup;

    public FreeTrialSohAdapter(List<TableFreeTrialCoh> itemsData) {
        this.itemsData = itemsData;
    }

    @Override
    public FreeTrialSohAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ft_master_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        mViewGroup = parent;
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TableFreeTrialCoh cn = itemsData.get(position);

        if (cn.getSerialized().equalsIgnoreCase("Y")) {
            viewHolder.txtTitle.setText(cn.getSn());
        } else {
            viewHolder.txtTitle.setText(cn.getName());
        }
        viewHolder.txtDate.setText(cn.getStock_date());
        viewHolder.txtIcon.setText(cn.getType().substring(0, 1));
        viewHolder.txtPack.setText(cn.getPackage_id());
        if (cn.getHw_id().equalsIgnoreCase("881")) {
            viewHolder.txtType.setText("Router XL");
        } else if (cn.getHw_id().equalsIgnoreCase("882")) {
            viewHolder.txtType.setText("SimCard XL");
        } else if (cn.getHw_id().equalsIgnoreCase("887")) {
            viewHolder.txtType.setText("Router Indosat");
        } else if (cn.getHw_id().equalsIgnoreCase("888")) {
            viewHolder.txtType.setText("SimCard Indosat");
        } else {
            viewHolder.txtType.setText("");
        }

        switch (cn.getType()) {
            case "VC":
                viewHolder.txtIcon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.green_10));
                break;
            case "LNB":
                viewHolder.txtIcon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.blue_10));
                break;
            case "ODU":
                viewHolder.txtIcon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.yellow_10));
                break;
            case "DSD":
                viewHolder.txtIcon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.red_10));
                break;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTitle;
        public TextView txtDate;
        public TextView txtIcon;
        public TextView txtPack;
        private TextView txtType;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtTitle = (TextView) itemLayoutView.findViewById(R.id.ft_master_item_title);
            txtDate = (TextView) itemLayoutView.findViewById(R.id.ft_master_item_date);
            txtIcon = (TextView) itemLayoutView.findViewById(R.id.ft_master_item_icon);
            txtPack = (TextView) itemLayoutView.findViewById(R.id.ft_master_item_package);
            txtType = (TextView) itemLayoutView.findViewById(R.id.ft_master_item_type);
        }
    }

    @Override
    public int getItemCount() {
        if (itemsData == null) {
            return 0;
        } else {
            return itemsData.size();
        }
    }
}
