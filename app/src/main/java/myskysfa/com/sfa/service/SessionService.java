package myskysfa.com.sfa.service;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import myskysfa.com.sfa.database.db_adapter.TableFTSOHAdapter;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.database.db_adapter.TablePlanAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;
import myskysfa.com.sfa.utils.SessionManager;
import myskysfa.com.sfa.utils.Utils;

public class SessionService extends BroadcastReceiver {
    Context ctx;
    public SessionManager SM;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            this.ctx = context;
            this.SM = new SessionManager(context);
            resuest();
        }catch (Exception e){}
    }

    private void resuest() {
        if(SM.isLoggedIn()) {
            new LogoutTask().execute();
            Log.i("Session Service", "Execute");
        }else{
            Log.i("Session Service", "Lose");
        }
    }

    private class LogoutTask extends AsyncTask<String, String, String> {

        private Utils utils = new Utils(ctx);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String url = ConnectionManager.CM_URL_LOGOUT;
                String username = null, token = null;

                System.out.println("url: " + url);
                String response = ConnectionManager.requestLogout(url, utils.getIMEI(), token, username, Config.version, ctx);
                System.out.println("response: " + response);
                Boolean status = false;
                if (response.length() > 0) {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status = jsonObject.getBoolean(Config.KEY_STATUS);
                        String message = jsonObject.getString(Config.KEY_MESSAGE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            DeleteLogSession();
            DeleteLogLogin();
            clearSharedPreference();
            deleteSOHandPlan();
        }
    }

    private void clearSharedPreference() {
        TableLogLoginAdapter db = new TableLogLoginAdapter(ctx);
        db.deleteAll();
        System.exit(0);
    }

    private void deleteSOHandPlan() {
        TableFTSOHAdapter soh = new TableFTSOHAdapter(ctx);
        soh.delete(ctx);
        TablePlanAdapter plan = new TablePlanAdapter(ctx);
        plan.delete(ctx);
    }

    private void DeleteLogLogin() {
        TableLogLoginAdapter mLogLoginAdapter = new TableLogLoginAdapter(ctx);
        mLogLoginAdapter.deleteAll();
    }

    private void DeleteLogSession(){
        SM.logoutUser();
    }
}