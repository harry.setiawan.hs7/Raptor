package myskysfa.com.sfa.adapter.master;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableMasterMaterial;

/**
 * Created by Eno on 6/20/2016.
 */
public class MaterialAdapter extends RecyclerView.Adapter<MaterialAdapter.ViewHolder> {
    View itemLayoutView;
    ViewGroup mParent;
    private List<TableMasterMaterial> itemsData;
    private Context context;

    public MaterialAdapter(Context context, List<TableMasterMaterial> itemsData) {
        this.context = context;
        this.itemsData = itemsData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.material_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterMaterial itemMaterial = itemsData.get(position);
        if (itemMaterial != null) {
            String desc;
            if (itemMaterial.getDescription().length() > 50) {
                desc = itemMaterial.getDescription().substring(0, 49) + "...";
            } else {
                desc = itemMaterial.getDescription();
            }


            holder.item_code.setText(itemMaterial.getId());
            holder.txtTitle.setText(desc);
            holder.txtRate.setText("price: " + itemMaterial.getRate());
            holder.txtIcon.setText(itemMaterial.getDescription().substring(0, 1));
        }

    }

    @Override
    public int getItemCount() {
        return itemsData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public TextView txtRate;
        public TextView txtIcon;
        public TextView item_code;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemLayoutView.findViewById(R.id.master_material_item_title);
            txtRate = (TextView) itemLayoutView.findViewById(R.id.master_material_item_price);
            txtIcon = (TextView) itemLayoutView.findViewById(R.id.master_material_item_icon);
            item_code = (TextView) itemLayoutView.findViewById(R.id.item_code);

        }
    }
}
