package myskysfa.com.sfa.retrofit;

import com.google.gson.annotations.SerializedName;

public class transport{
    @SerializedName("post_code")
    String post_code;
    @SerializedName("transportation_fee")
    String transportation_fee;

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getTransportation_fee() {
        return transportation_fee;
    }

    public void setTransportation_fee(String transportation_fee) {
        this.transportation_fee = transportation_fee;
    }
}