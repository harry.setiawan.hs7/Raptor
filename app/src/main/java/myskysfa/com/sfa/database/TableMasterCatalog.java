package myskysfa.com.sfa.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 6/20/2016.
 */
@DatabaseTable(tableName = "m_catalog")
public class TableMasterCatalog {

    public static final String TABLE_NAME = "m_catalog";
    public static final String fROWID = "id";
    public static final String fCATEGORY = "category";
    public static final String fCODE = "code";
    public static final String fDESC = "description";

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String category, code, description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
