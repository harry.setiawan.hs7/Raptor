package myskysfa.com.sfa.retrofit;

import com.google.gson.annotations.SerializedName;

/**
 * Created by harrysetiawan on 14/09/17.
 */

public class data_upload {
    @SerializedName("status")
    boolean status;
    @SerializedName("data")
    String data;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
}
