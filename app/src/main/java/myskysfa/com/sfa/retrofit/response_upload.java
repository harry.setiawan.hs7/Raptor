package myskysfa.com.sfa.retrofit;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by harrysetiawan on 14/09/17.
 */

public class response_upload implements Serializable{
    @SerializedName("upload")
    List<data_upload> upload;

    public List<data_upload> getUpload() {
        return upload;
    }

    public void setUpload(List<data_upload> upload) {
        this.upload = upload;
    }
}

