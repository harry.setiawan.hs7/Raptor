package myskysfa.com.sfa.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import myskysfa.com.sfa.R;
import myskysfa.com.sfa.database.TableLogLogin;
import myskysfa.com.sfa.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.sfa.utils.Config;
import myskysfa.com.sfa.utils.ConnectionManager;

public class Home extends Fragment {
    private ViewGroup root;
    private Menu menu;
    private MenuItem alert;
    int dtd_bb_plan = 0, dtd_bb_jp_new = 0, dtd_bb_jp_pa = 0, dtd_bb_pp_new = 0, dtd_bb_pp_pa = 0, dtd_plan = 0, dtd_jp_pe = 0, dtd_pp_pe = 0, ms_plan = 0, ms_jp_pe = 0, ms_pp_pe = 0;
    String jualputus_pinjampakai[] = {"Jual Putus", "Pinjam Pakai"};
    private TableLogLoginAdapter loginAdapter;
    SwipeRefreshLayout swipe;
    HorizontalBarChart dtdbbChart, dtdChart, msChart;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.home, container, false);
        _init();
        _iset_chart();
        new GetDashboardData().execute();

        return root;
    }

    private void _init() {
        dtdbbChart = (HorizontalBarChart) root.findViewById(R.id.dtdbbChart);
        dtdChart = (HorizontalBarChart) root.findViewById(R.id.dtdChart);
        msChart = (HorizontalBarChart) root.findViewById(R.id.msChart);
        swipe = (SwipeRefreshLayout) root.findViewById(R.id.swipe_dash);

        swipe.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe.setRefreshing(true);
                new GetDashboardData().execute();
            }
        });
    }

    private void _iset_chart() {
        // DTD Bawa Barang
        BarData dataDTDBB = new BarData(getXAxisDTDBBValues(), getDataDTDBB());
        dtdbbChart.setData(dataDTDBB);
        dtdbbChart.setDescription("");
        dtdbbChart.animateXY(2000, 2000);
        dtdbbChart.invalidate();

        // Door To Door
        BarData dataDTD = new BarData(getXAxisDTDMSValues(), getDataDTD());
        dtdChart.setData(dataDTD);
        dtdChart.setDescription("");
        dtdChart.animateXY(2000, 2000);
        dtdChart.invalidate();

        // Modern Store
        BarData dataMS = new BarData(getXAxisDTDMSValues(), getDataMS());
        msChart.setData(dataMS);
        msChart.setDescription("");
        msChart.animateXY(2000, 2000);
        msChart.invalidate();

    }

    private List<String> getXAxisDTDBBValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Plan");
        xAxis.add("New");
        xAxis.add("PA");
        return xAxis;
    }

    private List<String> getXAxisDTDMSValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Plan");
        xAxis.add("EP");
        return xAxis;
    }

    private List<IBarDataSet> getDataDTDBB() {
        List<IBarDataSet> dataSet = new ArrayList<>();

        List<BarEntry> bar_plan = new ArrayList<>();
        bar_plan.add(new BarEntry(dtd_bb_plan, 0));
        BarDataSet status_plan = new BarDataSet(bar_plan, "Plan");
        status_plan.setValueFormatter(new MyValueFormatter());
        status_plan.setValueTextSize(8);
        status_plan.setColor(Color.rgb(51, 153, 255));

        List<BarEntry> bar_new_pa = new ArrayList<>();
        bar_new_pa.add(new BarEntry(new float[]{dtd_bb_jp_new, dtd_bb_pp_new}, 1));
        bar_new_pa.add(new BarEntry(new float[]{dtd_bb_jp_pa, dtd_bb_pp_pa}, 2));
        BarDataSet status_new_pa = new BarDataSet(bar_new_pa, "");
        status_new_pa.setStackLabels(jualputus_pinjampakai);
        status_new_pa.setValueFormatter(new MyValueFormatter());
        status_new_pa.setValueTextSize(8);
        status_new_pa.setColors(new int[]{Color.rgb(255, 204, 0), Color.rgb(46, 204, 58)});

        dataSet.addAll(Arrays.asList(status_plan, status_new_pa/*, status_pa*/));
        return dataSet;
    }

    private List<IBarDataSet> getDataDTD() {
        List<IBarDataSet> dataSet = new ArrayList<>();

        List<BarEntry> bar_plan = new ArrayList<>();
        bar_plan.add(new BarEntry(dtd_plan, 0));
        BarDataSet status_plan = new BarDataSet(bar_plan, "Plan");
        status_plan.setValueFormatter(new MyValueFormatter());
        status_plan.setValueTextSize(8);
        status_plan.setColor(Color.rgb(51, 153, 255));

        List<BarEntry> bar_new = new ArrayList<>();
        bar_new.add(new BarEntry(new float[]{dtd_jp_pe, dtd_pp_pe}, 1));
        BarDataSet status_ep = new BarDataSet(bar_new, "");
        status_ep.setStackLabels(jualputus_pinjampakai);
        status_ep.setValueFormatter(new MyValueFormatter());
        status_ep.setValueTextSize(8);
        status_ep.setColors(new int[]{Color.rgb(255, 204, 0), Color.rgb(46, 204, 58)});

        dataSet.addAll(Arrays.asList(status_plan, status_ep));
        return dataSet;
    }

    private List<IBarDataSet> getDataMS() {
        List<IBarDataSet> dataSet = new ArrayList<>();

        List<BarEntry> bar_plan = new ArrayList<>();
        bar_plan.add(new BarEntry(ms_plan, 0));
        BarDataSet status_plan = new BarDataSet(bar_plan, "Plan");
        status_plan.setValueFormatter(new MyValueFormatter());
        status_plan.setValueTextSize(8);
        status_plan.setColor(Color.rgb(51, 153, 255));

        List<BarEntry> bar_new = new ArrayList<>();
        bar_new.add(new BarEntry(new float[]{ms_jp_pe, ms_pp_pe}, 1));
        BarDataSet status_ep = new BarDataSet(bar_new, "");
        status_ep.setStackLabels(jualputus_pinjampakai);
        status_ep.setValueFormatter(new MyValueFormatter());
        status_ep.setValueTextSize(8);
        status_ep.setColors(new int[]{Color.rgb(255, 204, 0), Color.rgb(46, 204, 58)});

        dataSet.addAll(Arrays.asList(status_plan, status_ep));
        return dataSet;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.info_menu, menu);
        this.menu = menu;
        alert = this.menu.findItem(R.id.count);
        alert.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private class MyValueFormatter implements ValueFormatter {
        private DecimalFormat mFormat;
        MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##");
        }
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value);
        }
    }

    private class GetDashboardData extends AsyncTask<URL, Integer, Long>{
        private ProgressDialog pDialog = new ProgressDialog(getActivity());
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!swipe.isRefreshing()) {
                pDialog.setMessage("Mohon tunggu...");
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        @Override
        protected Long doInBackground(URL... params) {
            try {
                /*loginAdapter = new TableLogLoginAdapter(getContext());
                List<TableLogLogin> listLogin = loginAdapter.getAllData();
                String sflCode = listLogin.get(listLogin.size() - 1).getcSflCode();*/
                SharedPreferences sessionPref = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);
                String sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
                String url = ConnectionManager.CM_URL_DASHBOARD + sflCode;
                String response = ConnectionManager.requestDashboard(url, Config.version, getContext());
                if(response!=null){
                    JSONObject jo_response = new JSONObject(response);
                    JSONArray ja_response = jo_response.getJSONArray("data");
                    if(ja_response.length() != 0) {
                        for (int i = 0; i < ja_response.length(); i += 1) {
                            JSONObject data_chart = ja_response.getJSONObject(i);
                            dtd_bb_jp_new = Integer.parseInt(data_chart.getString("dtd_jp_new"));
                            dtd_bb_pp_new = Integer.parseInt(data_chart.getString("dtd_pp_new"));
                            dtd_bb_jp_pa = Integer.parseInt(data_chart.getString("dtd_jp_pa"));
                            dtd_bb_pp_pa = Integer.parseInt(data_chart.getString("dtd_pp_pa"));
                            dtd_jp_pe = Integer.parseInt(data_chart.getString("dtd_reg_jp"));
                            dtd_pp_pe = Integer.parseInt(data_chart.getString("dtd_reg_pp"));
                            ms_jp_pe = Integer.parseInt(data_chart.getString("ms_jp"));
                            ms_pp_pe = Integer.parseInt(data_chart.getString("ms_pp"));
                            dtd_plan = Integer.parseInt(data_chart.getString("plan_reg"));
                            dtd_bb_plan = Integer.parseInt(data_chart.getString("plan_dtd"));
                            ms_plan = Integer.parseInt(data_chart.getString("plan_ms"));
                        }
                    }
                }
                Log.i("RESP GR", response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            swipe.setRefreshing(false);
            _iset_chart();
        }
    }

}
